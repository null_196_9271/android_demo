package com.uzhu.project.widget.pulltorefresh.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;

import com.mvvm.framework.util.LogUtil;


public class IExpandListView extends ExpandableListView implements OnGroupClickListener {
    public IExpandListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        registerListener();
    }

    public IExpandListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        registerListener();
    }

    public IExpandListView(Context context) {
        super(context);
        registerListener();
    }


    public ExpandScrollListener expandScrollListener = new ExpandScrollListener();

    /**
     * Adapter 接口 . 列表必须实现此接口 .
     */
    public interface IExpandHeaderAdapter {
        public static final int PINNED_HEADER_GONE = 0;
        public static final int PINNED_HEADER_VISIBLE = 1;
        public static final int PINNED_HEADER_PUSHED_UP = 2;

        /**
         * 获取 Header 的状态
         *
         * @param groupPosition
         * @param childPosition
         * @return PINNED_HEADER_GONE, PINNED_HEADER_VISIBLE, PINNED_HEADER_PUSHED_UP 其中之一
         */
        int getIExpandHeaderState(int groupPosition, int childPosition);

        /**
         * 配置 IExpandHeader, 让 IExpandHeader 知道显示的内容
         *
         * @param header
         * @param groupPosition
         * @param childPosition
         * @param alpha
         */
        void configureIExpandHeader(View header, int groupPosition, int childPosition, int alpha);

        /**
         * 设置组按下的状态
         *
         * @param groupPosition
         * @param status
         */
        void setGroupClickStatus(int groupPosition, int status);

        /**
         * 获取组按下的状态
         *
         * @param groupPosition
         * @return
         */
        int getGroupClickStatus(int groupPosition);

    }

    private static final int MAX_ALPHA = 255;

    private IExpandHeaderAdapter mIExpandHeaderAdapter;

    /**
     * 用于在列表头显示的 View,mHeaderViewVisible 为 true 才可见
     */
    private View mHeaderView;

    /**
     * 列表头是否可见
     */
    private boolean mHeaderViewVisible;

    private int mHeaderViewWidth;

    private int mHeaderViewHeight;

    public void setHeaderView(View view) {
        mHeaderView = view;
        LayoutParams lp = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mHeaderView.setLayoutParams(lp);

        if (mHeaderView != null) {
            setFadingEdgeLength(0);
        }

        requestLayout();
    }

    private void registerListener() {
        setOnScrollListener(expandScrollListener);
        setOnGroupClickListener(this);
//		setOnChildClickListener(this);
    }


    /**
     * 点击 HeaderView 触发的事件
     */
    private void headerViewClick() {
        long packedPosition = getExpandableListPosition(this.getFirstVisiblePosition());

        int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);

        if (mIExpandHeaderAdapter.getGroupClickStatus(groupPosition) == 1) {
            this.collapseGroup(groupPosition);
            mIExpandHeaderAdapter.setGroupClickStatus(groupPosition, 0);
        } else {
            this.expandGroup(groupPosition);
            mIExpandHeaderAdapter.setGroupClickStatus(groupPosition, 1);
        }

        LogUtil.d(LogUtil.MYTAG, "header-setSelectedGroup");
        this.setSelectedGroup(groupPosition);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //拦截事件判断是否点击在headerview区域。
        if (mHeaderViewVisible) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mDownX = ev.getX();
                    mDownY = ev.getY();
                    //点击范围是否HeaderView内
                    if (mDownX <= mHeaderViewWidth && mDownY <= mHeaderViewHeight) {
                        return true;
                    }
                    break;
            }
        }
        return super.onInterceptTouchEvent(ev);
    }

    private float mDownX;
    private float mDownY;
    private boolean isTouchDownHeaderView = false;


    /**
     * 如果 HeaderView 是可见的 , 此函数用于判断是否点击了 HeaderView, 并对做相应的处理 ,
     * 因为 HeaderView 是画上去的 , 所以设置事件监听是无效的 , 只有自行控制 .
     */
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......1");
        if (mHeaderViewVisible) {
            LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......2");
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......3:DOWN");
                    mDownX = ev.getX();
                    mDownY = ev.getY();
                    //点击范围是否HeaderView内
                    if (mDownX <= mHeaderViewWidth && mDownY <= mHeaderViewHeight) {
                        LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......3.1");
                        isTouchDownHeaderView = true;
                        return true;
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......5:MOVE");
                    break;
                case MotionEvent.ACTION_UP:
                    LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......4:UP");
                    float x = ev.getX();
                    float y = ev.getY();
                    float offsetX = Math.abs(x - mDownX);
                    float offsetY = Math.abs(y - mDownY);
                    // 如果 HeaderView 是可见的 , 点击在 HeaderView 内 , 那么触发 headerClick()
                    if (isTouchDownHeaderView && x <= mHeaderViewWidth && y <= mHeaderViewHeight
                            && offsetX <= mHeaderViewWidth && offsetY <= mHeaderViewHeight) {
                        LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......4.1");
                        if (mHeaderView != null) {
                            headerViewClick();
                        }
                        //重置
                        isTouchDownHeaderView = false;
                        return true;
                    }
                    break;
                default:
                    break;
            }
        }
        LogUtil.d(LogUtil.MYTAG, "HeaderView Touch......6");
        return super.onTouchEvent(ev);

    }

    @Override
    public void setAdapter(ExpandableListAdapter adapter) {
        super.setAdapter(adapter);
        mIExpandHeaderAdapter = (IExpandHeaderAdapter) adapter;
    }

    /**
     * 点击了 Group 触发的事件 , 要根据根据当前点击 Group 的状态来
     */
    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if (mIExpandHeaderAdapter.getGroupClickStatus(groupPosition) == 0) {
            mIExpandHeaderAdapter.setGroupClickStatus(groupPosition, 1);
            parent.expandGroup(groupPosition);
            LogUtil.d(LogUtil.MYTAG, "onGroupClick-setSelectedGroup");
            //定位到当前展开的groupPosition
//			parent.setSelectedGroup(groupPosition);

        } else if (mIExpandHeaderAdapter.getGroupClickStatus(groupPosition) == 1) {
            mIExpandHeaderAdapter.setGroupClickStatus(groupPosition, 0);
            parent.collapseGroup(groupPosition);
        }

        // 返回 true 才可以弹回第一行 , 不知道为什么
        return true;

    }

//	@Override
//	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//
//
//		return false;
//	}

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mHeaderView != null) {
            measureChild(mHeaderView, widthMeasureSpec, heightMeasureSpec);
            mHeaderViewWidth = mHeaderView.getMeasuredWidth();
            mHeaderViewHeight = mHeaderView.getMeasuredHeight();
            LogUtil.d(LogUtil.MYTAG, "mHeaderViewWidth=" + mHeaderViewWidth + ";mHeaderViewHeight=" + mHeaderViewHeight);
        }
    }

    private int mOldState = -1;

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        final long flatPostion = getExpandableListPosition(getFirstVisiblePosition());
        final int groupPos = ExpandableListView.getPackedPositionGroup(flatPostion);
        final int childPos = ExpandableListView.getPackedPositionChild(flatPostion);
        int state = mIExpandHeaderAdapter.getIExpandHeaderState(groupPos, childPos);
        if (mHeaderView != null && mIExpandHeaderAdapter != null && state != mOldState) {
            mOldState = state;
            mHeaderView.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight);
        }

        configureHeaderView(groupPos, childPos);
    }

    public void configureHeaderView(int groupPosition, int childPosition) {
        if (mHeaderView == null || mIExpandHeaderAdapter == null
                || ((ExpandableListAdapter) mIExpandHeaderAdapter).getGroupCount() == 0) {
            return;
        }

        int state = mIExpandHeaderAdapter.getIExpandHeaderState(groupPosition, childPosition);

        switch (state) {
            case IExpandHeaderAdapter.PINNED_HEADER_GONE: {
                mHeaderViewVisible = false;
                break;
            }

            case IExpandHeaderAdapter.PINNED_HEADER_VISIBLE: {
                mIExpandHeaderAdapter.configureIExpandHeader(mHeaderView, groupPosition, childPosition, MAX_ALPHA);

                if (mHeaderView.getTop() != 0) {
                    mHeaderView.layout(0, 0, mHeaderViewWidth, mHeaderViewHeight);
                }

                mHeaderViewVisible = true;

                break;
            }

            case IExpandHeaderAdapter.PINNED_HEADER_PUSHED_UP: {
                View firstView = getChildAt(1);
//				int bottom = firstView.getBottom();
                int bottom = firstView.getTop();
//				bottom=bottom-1;
                // intitemHeight = firstView.getHeight();
                int headerHeight = mHeaderView.getHeight();

                int y;

                int alpha;

                if (bottom < headerHeight) {
                    y = (bottom - headerHeight);
                    alpha = MAX_ALPHA * (headerHeight + y) / headerHeight;
                } else {
                    y = 0;
                    alpha = MAX_ALPHA;
                }

                mIExpandHeaderAdapter.configureIExpandHeader(mHeaderView, groupPosition, childPosition, alpha);

                if (mHeaderView.getTop() != y) {
                    mHeaderView.layout(0, y, mHeaderViewWidth, mHeaderViewHeight + y);
                }

                mHeaderViewVisible = true;
                break;
            }
        }
    }

    @Override
    /**
     * 列表界面更新时调用该方法(如滚动时)
     */
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (mHeaderViewVisible) {
            //分组栏是直接绘制到界面中，而不是加入到ViewGroup中
            drawChild(canvas, mHeaderView, getDrawingTime());
        }
    }

//	@Override
//	public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {
//		final long flatPos = getExpandableListPosition(firstVisibleItem);
//		int groupPosition = ExpandableListView.getPackedPositionGroup(flatPos);
//		int childPosition = ExpandableListView.getPackedPositionChild(flatPos);
//		LogUtil.d(LogUtil.MYTAG,"I:groupPosition="+groupPosition+";childPosition="+childPosition);
//		configureHeaderView(groupPosition, childPosition);
//	}
//
//	@Override
//	public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//	}


    public class ExpandScrollListener implements OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            final long flatPos = getExpandableListPosition(firstVisibleItem);
            int groupPosition = ExpandableListView.getPackedPositionGroup(flatPos);
            int childPosition = ExpandableListView.getPackedPositionChild(flatPos);
            LogUtil.d(LogUtil.MYTAG, "I:groupPosition=" + groupPosition + ";childPosition=" + childPosition);
            configureHeaderView(groupPosition, childPosition);
        }
    }
}
