package com.uzhu.project.widget.viewpager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * 封装view，使viewpager根据内容自适应高度
 *
 * @author 军
 */
public class AutoHeightViewPager extends ViewPager {

    public AutoHeightViewPager(Context context) {
        super(context);
    }

    public AutoHeightViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if (h > height) height = h;
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        boolean interceptTouchEvent = super.onInterceptTouchEvent(event);
        double preX = 0;
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            preX = event.getX();

        } else {
            if( Math.abs(event.getX() - preX)>4) {
                return true;
            } else {
                preX = event.getX();
            }
        }
        return interceptTouchEvent;
    }
}