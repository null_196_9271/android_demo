/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: LogManager.java
 * Author:   jeremy
 * Date:     2015-10-13 19:57:57
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-13 19:57:57> <version>   <desc>
 */
package com.uzhu.project.widget.photoview.log;

import android.util.Log;

/**
 * class that holds the {@link Logger} for this library, defaults to {@link LoggerDefault} to send logs to android {@link Log}
 */
public final class LogManager {

    private static Logger logger = new LoggerDefault();

    public static void setLogger(Logger newLogger) {
        logger = newLogger;
    }

    public static Logger getLogger() {
        return logger;
    }

}
