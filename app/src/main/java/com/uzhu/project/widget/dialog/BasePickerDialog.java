package com.uzhu.project.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uzhu.project.R;


public class BasePickerDialog extends Dialog {

	private TextView commonCancelTv, commonConfirmTv, timePickerTitle;
	private PickerView firstPickerView;
	private PickerView pickerView2, pickerView3;
	private LinearLayout mPickerLayout;
	private View mRootView;
	private TextView mTitle;

	private Animation mShowAnim;
	private Animation mDismissAnim;
	private boolean isDismissing;

	public BasePickerDialog(Context context) {
		super(context, R.style.ActionSheetDialog);
		getWindow().setGravity(Gravity.BOTTOM);
		initDatePickerDialogViews(context);
		initAnim(context);
//		initData();
		setCancelable(false);
	}

	public void setPicker(int count1, PickerView.OnPickerViewAdapter adapter1, int count2, PickerView.OnPickerViewAdapter adapter2, int count3, PickerView.OnPickerViewAdapter adapter3) {
		if (count1 <= 0) {
			return;
		}
		firstPickerView.setData(count1, adapter1);
		if (count2 > 0) {
			pickerView2 = getPickerView();
			pickerView2.setData(count2, adapter2);
			mPickerLayout.addView(pickerView2);
		}
		if (count3 > 0) {
			pickerView3 = getPickerView();
			pickerView3.setData(count3, adapter3);
			mPickerLayout.addView(pickerView3);
		}
	}

	public void setOnSelectListener(PickerView.onSelectListener inf1, PickerView.onSelectListener inf2, PickerView.onSelectListener inf3) {
		firstPickerView.setOnSelectListener(inf1);
		if (pickerView2 != null) {
			pickerView2.setOnSelectListener(inf2);
		}
		if (pickerView3 != null) {
			pickerView3.setOnSelectListener(inf3);
		}
	}

	public void setSelectedIndex(int... index) {
		if (index == null || index.length == 0) {
			return;
		}
		firstPickerView.setCurrentIndex(index[0]);
		if (index.length > 1) {
			if (pickerView2 != null) {
				pickerView2.setCurrentIndex(index[1]);
			}
		}
		if (index.length > 2) {
			if (pickerView3 != null) {
				pickerView3.setCurrentIndex(index[2]);
			}
		}
	}

	public int[] getSelectedIndex() {
		int[] selectedIndex;
		if (pickerView3 != null) {
			selectedIndex = new int[3];
			selectedIndex[0] = firstPickerView.getCurrentIndex();
			selectedIndex[1] = pickerView2.getCurrentIndex();
			selectedIndex[2] = pickerView3.getCurrentIndex();
		} else if (pickerView2 != null) {
			selectedIndex = new int[2];
			selectedIndex[0] = firstPickerView.getCurrentIndex();
			selectedIndex[1] = pickerView2.getCurrentIndex();
		} else {
			selectedIndex = new int[1];
			selectedIndex[0] = firstPickerView.getCurrentIndex();
		}
		return selectedIndex;
	}

	private PickerView getPickerView() {
		PickerView pickerView = new PickerView(getContext());
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f);
		pickerView.setLayoutParams(layoutParams);
		return pickerView;
	}

	private void initDatePickerDialogViews(Context context) {
		mRootView = View.inflate(context, R.layout.picker_dialog, null);
		mTitle = (TextView) mRootView.findViewById(R.id.tv_title);
		setContentView(mRootView);
		commonCancelTv = (TextView) findViewById(R.id.dialog_cancel_tv);
		commonConfirmTv = (TextView) findViewById(R.id.dialog_confirm_tv);

//		timePickerTitle = (TextView) findViewById(R.id.dialog_time_picker_label_tv);
		firstPickerView = (PickerView) findViewById(R.id.first_check);
		mPickerLayout = (LinearLayout) findViewById(R.id.picker_layout);

		commonCancelTv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
				if (mOnViewClickListener != null) {
					mOnViewClickListener.onCancelClick();
				}
			}
		});
		commonConfirmTv
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
//						dismiss();
						if (mOnViewClickListener != null) {
							if (pickerView2 == null) {
								mOnViewClickListener.onConfirmClick(new int[]{firstPickerView.getCurrentIndex()});
							} else if (pickerView3 == null) {
								mOnViewClickListener.onConfirmClick(new int[]{firstPickerView.getCurrentIndex(), pickerView2.getCurrentIndex()});
							} else {
								mOnViewClickListener.onConfirmClick(new int[]{firstPickerView.getCurrentIndex(), pickerView2.getCurrentIndex(), pickerView3.getCurrentIndex()});
							}
						}
					}
				});
		//dialog填充屏幕宽度
		WindowManager.LayoutParams wmlp = getWindow().getAttributes();
		wmlp.width = WindowManager.LayoutParams.MATCH_PARENT;
	}

	private void initAnim(Context context) {
		mShowAnim = AnimationUtils.loadAnimation(context, R.anim.push_bottom_in);
		mDismissAnim = AnimationUtils.loadAnimation(context, R.anim.push_bottom_out);
		mDismissAnim.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				dismissMe();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
	}

	public void setActionSheetTitle(String title) {
		mTitle.setText(title);
	}

	@Override
	public void show() {
		super.show();
		mRootView.startAnimation(mShowAnim);
	}

	@Override
	public void dismiss() {
		if (isDismissing) {
			return;
		}
		isDismissing = true;
		mRootView.startAnimation(mDismissAnim);
	}

	private void dismissMe() {
		super.dismiss();
		isDismissing = false;
	}

	public void notifyDataSetChanged(int column, int count) {
		switch (column) {
			case 0:
				firstPickerView.notifyDataSetChanged(count, true);
				break;
			case 1:
				pickerView2.notifyDataSetChanged(count, true);
				break;
			case 2:
				pickerView3.notifyDataSetChanged(count, true);
				break;
		}
	}

	@Override
	public void setTitle(int titleId) {
//		timePickerTitle.setText(titleId);
	}

	private OnViewClickListener mOnViewClickListener;

	public void setOnViewClickListener(OnViewClickListener inf) {
		mOnViewClickListener = inf;
	}

	public interface OnViewClickListener {
		void onCancelClick();

		void onConfirmClick(int... index);
	}
}
