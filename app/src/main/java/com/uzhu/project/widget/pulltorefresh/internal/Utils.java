/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: Utils.java
 * Author:   jeremy
 * Date:     2015-10-08 15:59:02
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-08 15:59:02> <version>   <desc>
 */

package com.uzhu.project.widget.pulltorefresh.internal;

import android.util.Log;

public class Utils {

	static final String LOG_TAG = "PullToRefresh";

	public static void warnDeprecation(String depreacted, String replacement) {
		Log.w(LOG_TAG, "You're using the deprecated " + depreacted + " attr, please switch over to " + replacement);
	}

}
