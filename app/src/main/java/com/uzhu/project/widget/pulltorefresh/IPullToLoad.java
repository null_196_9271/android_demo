/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: IPullToLoad.java
 * Author:   jeremy
 * Date:     2015-10-08 15:58:33
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-08 15:58:33> <version>   <desc>
 */

package com.uzhu.project.widget.pulltorefresh;

import android.view.View;

public interface IPullToLoad<T extends View> {

    public boolean getFilterTouchEvents();


    public ILoadingLayout getLoadingLayoutProxy();


    public ILoadingLayout getLoadingLayoutProxy(boolean includeStart, boolean includeEnd);



    public T getRefreshableView();


    public boolean getShowViewWhileRefreshing();



    public boolean isPullToRefreshEnabled();


    public boolean isPullToRefreshOverScrollEnabled();


    public boolean isRefreshing();


    public void onRefreshComplete();

    public void setOnPullEventListener(PullToRefreshBase.OnPullEventListener<T> listener);


    public void setOnRefreshListener(PullToRefreshBase.OnRefreshListener<T> listener);

    public void setRefreshing();

    public void setRefreshing(boolean doScroll);


}