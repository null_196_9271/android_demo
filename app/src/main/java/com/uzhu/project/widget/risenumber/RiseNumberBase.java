/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: RiseNumberBase.java
 * Author:   jeremy
 * Date:     2015-08-26 14:38:19
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-08-26 14:38:19> <version>   <desc>
 */

package com.uzhu.project.widget.risenumber;

public interface RiseNumberBase {
    public void start();
 
    public RiseNumberTextView withNumber(double number);
 
    public RiseNumberTextView withNumber(double number, boolean flag);
 
    public RiseNumberTextView withNumber(int number);
 
    public RiseNumberTextView setDuration(long duration);
 
    public void setOnEnd(RiseNumberTextView.EndListener callback);
}