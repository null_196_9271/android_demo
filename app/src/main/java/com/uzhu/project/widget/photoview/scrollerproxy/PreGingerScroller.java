/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: PreGingerScroller.java
 * Author:   jeremy
 * Date:     2015-10-13 19:58:17
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-13 19:58:17> <version>   <desc>
 */
package com.uzhu.project.widget.photoview.scrollerproxy;

import android.content.Context;
import android.widget.Scroller;

public class PreGingerScroller extends ScrollerProxy {

    private final Scroller mScroller;

    public PreGingerScroller(Context context) {
        mScroller = new Scroller(context);
    }

    @Override
    public boolean computeScrollOffset() {
        return mScroller.computeScrollOffset();
    }

    @Override
    public void fling(int startX, int startY, int velocityX, int velocityY, int minX, int maxX, int minY, int maxY,
                      int overX, int overY) {
        mScroller.fling(startX, startY, velocityX, velocityY, minX, maxX, minY, maxY);
    }

    @Override
    public void forceFinished(boolean finished) {
        mScroller.forceFinished(finished);
    }

    public boolean isFinished() {
        return mScroller.isFinished();
    }

    @Override
    public int getCurrX() {
        return mScroller.getCurrX();
    }

    @Override
    public int getCurrY() {
        return mScroller.getCurrY();
    }
}