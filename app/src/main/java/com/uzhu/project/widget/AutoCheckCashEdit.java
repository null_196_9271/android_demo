/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: AutoCheckCashEdit.java
 * Author:   jeremy
 * Date:     2015-08-28 14:17:08
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-08-28 14:17:08> <version>   <desc>
 */
package com.uzhu.project.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.uzhu.project.R;
import com.uzhu.project.util.DataUtils;

import java.text.DecimalFormat;


/**
 * 自定义EditText控件
 * 限制支付金额时统一调用的控件
 * 小数点后只能输入两位,最前面只能输入一个0，最前面一位不能为.
 * 不能输入0.00
 * 添加底线
 * @author tong
 */
public class AutoCheckCashEdit extends ClearEditText {
	private View.OnFocusChangeListener mOnFocusChangeListener;
	private TextView.OnEditorActionListener mOnEditorActionListenerBase;
	private int mMaxCount = 10;
	private double mMaxAmt = -1;
	private int mEditStart;
	private int mEditEnd;
	private Paint mPaint;
	private boolean isShowLine = false;

	public AutoCheckCashEdit(Context context, AttributeSet attrs) {
		super(context, attrs);

		mPaint = new Paint();
		mPaint.setStyle(Style.STROKE);
		mPaint.setColor(getResources().getColor(R.color.default_line_color));
		
		setLongClickable(false);
		addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void afterTextChanged(Editable edt) {
				if (edt.length() == 0)
					return;
				String temp = edt.toString();
				if (temp.startsWith(".")) {
					edt.delete(0, 1);
					return;
				}
			/*	if (temp.startsWith("0.00") && edt.length() == 4) {
					edt.delete(3, 4);
					return;
				}*/

				if (edt.length() >= 2 && temp.startsWith("0") && !temp.substring(1,2).equals(".")) {
					edt.delete(0, 1);
					return;
				}

				//处理小数点后只能输入两位的问题
				int pointIndex = temp.indexOf(".");
				if (pointIndex > 0 && temp.length() - pointIndex - 1 > 2) {
					edt.delete(pointIndex + 3, pointIndex + 4);
				}

				//小数点前只能输入mMaxCount位

				pointIndex = temp.indexOf(".");
				mEditStart = getSelectionStart();
				mEditEnd = getSelectionEnd();
				if (pointIndex <= 0) {
					if (temp.length() > mMaxCount) {
						edt.delete(mMaxCount, edt.length());
					}
				} else {
					if (pointIndex > mMaxCount) {
						edt.delete(mEditStart -1, mEditEnd );
					}
				}

				//处理输入最大值
				if(mMaxAmt != -1){
					if(Double.parseDouble(temp) > mMaxAmt){
						edt.delete(mEditStart -1, mEditEnd);
					}
				}
			}
		});
//		addTextChangedListener(new BaseTextWatcher(){
//			public void afterTextChanged(Editable edt) {
//				if (edt.length() == 0)
//					return;
//				String temp = edt.toString();
//				if (temp.startsWith(".")) {
//					edt.delete(0, 1);
//					return;
//				}
//			/*	if (temp.startsWith("0.00") && edt.length() == 4) {
//					edt.delete(3, 4);
//					return;
//				}*/
//
//				if (edt.length() >= 2 && temp.startsWith("0") && !temp.substring(1,2).equals(".")) {
//					edt.delete(0, 1);
//					return;
//				}
//
//				//处理小数点后只能输入两位的问题
//				int pointIndex = temp.indexOf(".");
//				if (pointIndex > 0 && temp.length() - pointIndex - 1 > 2) {
//					edt.delete(pointIndex + 3, pointIndex + 4);
//				}
//
//				//小数点前只能输入mMaxCount位
//
//				pointIndex = temp.indexOf(".");
//				mEditStart = getSelectionStart();
//				mEditEnd = getSelectionEnd();
//				if (pointIndex <= 0) {
//					if (temp.length() > mMaxCount) {
//						edt.delete(mMaxCount, edt.length());
//					}
//				} else {
//					if (pointIndex > mMaxCount) {
//						edt.delete(mEditStart -1, mEditEnd );
//					}
//				}
//
//				//处理输入最大值
//				if(mMaxAmt != -1){
//					if(Double.parseDouble(temp) > mMaxAmt){
//						edt.delete(mEditStart -1, mEditEnd);
//					}
//				}
//			}
//		});
		
		
		super.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					addZero();
				}
				if (mOnFocusChangeListener != null) {
					mOnFocusChangeListener.onFocusChange(v, hasFocus);
				}
			}
		});
		
		super.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
					InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					if(inputMethodManager.isActive()){
						inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
					}
					//移除焦点，添加.00
					clearFocus();
				}
				
				if (mOnEditorActionListenerBase != null) {
					mOnEditorActionListenerBase.onEditorAction(v, actionId, event);
				}
				return false;
			}
		});
	}
	
	/**
	 * 设置输入金额的总长度
	 * @param max
	 */
	public void setMaxCount(int max) {
		 mMaxCount = max;
	};
	
	/**
	 * 显示下划线
	 */
	public void showLine() {
		isShowLine = true;
	}
	
		/**
	 * 设置输入金额的最大值
	 * @param maxAmt
	 */
	public void setMaxAmt(double maxAmt){
		mMaxAmt = maxAmt;
	}
	
	
	@Override
	public void setOnFocusChangeListener(View.OnFocusChangeListener l) {
		mOnFocusChangeListener = l;
	}
	
	public void addZero() {
		if (TextUtils.isEmpty(getText()))
			return;
		DecimalFormat format = new DecimalFormat("0.00");
		double value = DataUtils.parseDouble(getText());
		String result = format.format(value);
		if ("0.0".equals(result)) {
			result = "0.00";
		}
		setText(format.format(value));
		setSelection(getText().length());
	}
	
	public String getMoney() {
		return getText().toString();
	}
	
	@Override
	public void setOnEditorActionListener(TextView.OnEditorActionListener l) {
		mOnEditorActionListenerBase = l;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (isShowLine) {
			// 画底线
			canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1,
					this.getHeight() - 1, mPaint);
		}
	}

}
