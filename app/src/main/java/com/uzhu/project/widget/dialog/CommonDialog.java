package com.uzhu.project.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.uzhu.project.R;


/**
 * Created by chenhainiao on 16/1/17.
 */
public class CommonDialog extends Dialog {
    private TextView messageView, titleView, tipsView;
    private TextView close;
    private TextView commit;

    public CommonDialog(Context context) {
//        super(context);
//        init(context);
        this(context, R.style.Dialog);
    }

    public CommonDialog(Context context, int theme) {
        super(context, theme);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.common_dialog, null);
        setContentView(view);
        messageView = (TextView) view.findViewById(R.id.message);
        titleView = (TextView) view.findViewById(R.id.title);
        close = (TextView) view.findViewById(R.id.close);
        commit = (TextView) view.findViewById(R.id.commit);
        tipsView = (TextView) view.findViewById(R.id.tips);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setCanceledOnTouchOutside(false);
    }

    public void setMessage(CharSequence charSequence) {
        messageView.setText(charSequence);
    }
    public void setMessageAlignment(int textAlignment) {
        messageView.setGravity(textAlignment);
    }
    public void setMessageSize(float size) {
        messageView.setTextSize(size);
    }
    public void setMessageColor(int color) {
        messageView.setTextColor(color);
    }
    public void setTitle(CharSequence charSequence) {
        titleView.setText(charSequence);
    }
    public void setOnCommitListener(CharSequence charSequence, View.OnClickListener listener) {
        commit.setText(charSequence);
        commit.setOnClickListener(listener);
    }
    public void setButtonVisibility(int visibility) {
        commit.setVisibility(visibility);
    }
    public void setTipsVisibility(int visibility) {
        tipsView.setVisibility(visibility);
    }
    public void showClose(boolean showClose) {
        if (showClose) {
            close.setVisibility(View.VISIBLE);
        } else {
            close.setVisibility(View.GONE);
        }
    }
}
