package com.uzhu.project.widget.swipeback.app;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.mvvm.framework.common.NotificationCenter;
import com.mvvm.framework.controller.BaseFragmentActivity;
import com.mvvm.framework.route.Route;
import com.uzhu.project.R;
import com.uzhu.project.base.CommonConst;
import com.uzhu.project.controller.activity.common.LoginActivity;
import com.uzhu.project.util.AppConfig;
import com.uzhu.project.widget.dialog.CustomDialog;
import com.uzhu.project.widget.swipeback.SwipeBackLayout;
import com.uzhu.project.widget.swipeback.SwipeBackUtils;

public abstract class SwipeBackActivity extends BaseFragmentActivity implements
        SwipeBackActivityBase {
    private SwipeBackActivityHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHelper = new SwipeBackActivityHelper(this);
        mHelper.onActivityCreate();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mHelper.onPostCreate();
    }

    @Override
    public View findViewById(int id) {
        View v = super.findViewById(id);
        if (v == null && mHelper != null)
            return mHelper.findViewById(id);
        return v;
    }

    @Override
    public SwipeBackLayout getSwipeBackLayout() {
        return mHelper.getSwipeBackLayout();
    }

    @Override
    public void setSwipeBackEnable(boolean enable) {
        getSwipeBackLayout().setEnableGesture(enable);
    }

    @Override
    public void scrollToFinishActivity() {
        SwipeBackUtils.convertActivityToTranslucent(this);
        getSwipeBackLayout().scrollToFinishActivity();
    }


    Dialog promptDialog;

    @Override
    public void alertMessage(String fieldName, int errorCode, String errorMsg) {
        if (errorCode == CommonConst.ERROR_CODE_TOKEN_INVALID) {
            //用户登录失效后，先清空用户登录信息，弹出dialog提示用户去登录
            AppConfig.clearLoginState();
            NotificationCenter.defaultCenter().postNotification("logout", null);
            CustomDialog.Builder customBuilder = new CustomDialog.Builder(this);
            customBuilder
                    .setTitle("提示")
                    .setMessage(getString(R.string.token_invalid))
                    .setContentView(null)
                    .setNegativeButton("登录",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    Route.nextController(SwipeBackActivity.this, LoginActivity.class, CommonConst.LOGIN);
                                    promptDialog.dismiss();
                                }
                            });
            if (promptDialog != null && promptDialog.isShowing()) {
                promptDialog.dismiss();
            }
            promptDialog = customBuilder.createDialog();
            // 点击周围是否可以取消
            promptDialog.setCancelable(false);
            promptDialog.setCanceledOnTouchOutside(false);
            promptDialog.show();
        } else {
            super.alertMessage(fieldName, errorCode, errorMsg);
        }
    }
}
