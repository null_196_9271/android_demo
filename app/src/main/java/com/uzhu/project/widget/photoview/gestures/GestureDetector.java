/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: GestureDetector.java
 * Author:   jeremy
 * Date:     2015-10-13 19:57:47
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-13 19:57:47> <version>   <desc>
 */
package com.uzhu.project.widget.photoview.gestures;

import android.view.MotionEvent;

public interface GestureDetector {

    public boolean onTouchEvent(MotionEvent ev);

    public boolean isScaling();

    public boolean isDragging();

    public void setOnGestureListener(OnGestureListener listener);

}
