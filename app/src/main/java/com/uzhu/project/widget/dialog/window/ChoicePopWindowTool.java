package com.uzhu.project.widget.dialog.window;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.mvvm.framework.util.CommonUtil;
import com.uzhu.project.R;
import com.uzhu.project.util.StrUtils;

import java.util.ArrayList;

/**
 * @author JeremyAiYt
 */
@SuppressLint("NewApi")
public class ChoicePopWindowTool {
    private ItemChoiceListener mItemChoiceListener;
    // 当前视图
    private int currCheckIndex;
    private Activity activity = null;
    private PopupWindow popupWindow = null;
    private View contentLayout;
    private ImageView closeImage;
    private View outSideTop;
    private View outSideBottom;
    private TextView title;
    private ListView listview;

    public static class ChoiceModel {
        public String content_1;// 内容1
        public String content_2;// 内容2
        public ChoiceModel(){

        }
        public ChoiceModel(String content_1, String content_2){
            this.content_1=content_1;
            this.content_2=content_2;
        }
    }

    public ChoicePopWindowTool(Activity activity) {
        this.activity = activity;
        //取消软键盘
        CommonUtil.keyBoardCancel(activity);
    }

    public interface ItemChoiceListener {
        public void itemChoice(PopupWindow popupWindow, int position);
    }

    public void openChoiceWindow(View parentView, String windowTitl,
                                 ArrayList<ChoiceModel> choiceList, int currCheckIndex, ItemChoiceListener mItemChoiceListener) {
        LayoutInflater mLayoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup popview = (ViewGroup) mLayoutInflater.inflate(
                R.layout.view_window_listview, null, true);
        // 视图初始化
        initView(popview);
        // 数据填充
        setView(windowTitl, choiceList,currCheckIndex, mItemChoiceListener);
        setListener();
        WindowManager wm = (WindowManager) activity
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int mScreenWidth = dm.widthPixels;// 获取屏幕分辨率宽度
        int mScreenHeight = dm.heightPixels;
        //        // int xw = (width * 10) / 10;
//        // int yh = (height * 10) / 10;
        int xw = mScreenWidth;
        int yh = mScreenHeight;
        popupWindow = new PopupWindow(popview, xw, yh);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        // backgroundAlpha(activity, 1f);
        // 设置动画
        popupWindow.setAnimationStyle(R.style.popwindow_anim_from_fade_style);
        popupWindow.isOutsideTouchable();
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAtLocation(parentView, Gravity.NO_GRAVITY, 0, 0);
        // 添加pop窗口关闭事件
        // popupWindow.setOnDismissListener(new poponDismissListener());
        popupWindow.update();

//        Animation animation = AnimationUtils.loadAnimation(activity,
//                R.anim.push_top_in);
//        contentLayout.setAnimation(animation);
    }

    public void initView(View popview) {
        contentLayout = popview.findViewById(R.id.content_layout);
        closeImage = (ImageView) popview.findViewById(R.id.choice_window_close);
        outSideTop = popview.findViewById(R.id.outside_top);
        outSideBottom = popview.findViewById(R.id.outside_bottom);
        title = (TextView) popview.findViewById(R.id.choice_window_title);
        listview = (ListView) popview.findViewById(R.id.choice_window_listview);
    }

    public void setView(String windowTitle,
                        ArrayList<ChoiceModel> choiceModelList, final int currCheckIndex, final ItemChoiceListener listener) {
        title.setText(windowTitle);
        // 填充数据
        choiceListAdapter listAdapter = new choiceListAdapter(activity,
                choiceModelList, currCheckIndex);
        listview.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        if(currCheckIndex>=0){
            //listview滑动到当前index
            listview.post(new Runnable() {

                @Override
                public void run() {
                    listview.setSelection(currCheckIndex);
                }
            });
        }

        listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                if (listener != null) {
                    listener.itemChoice(popupWindow, position);
                }
            }
        });
    }

    public void setListener() {
        closeImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
        outSideTop.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
        outSideBottom.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
    }

    public void dismiss() {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }

    public boolean isshow() {
        boolean isshow = false;
        if (popupWindow != null) {
            isshow = popupWindow.isShowing();
        }
        return isshow;
    }

    public class choiceListAdapter extends BaseAdapter {
        private Context mContext;
        private ArrayList<ChoiceModel> choiceList;
        private int currCheckIndex;
        private LayoutInflater inflater;

        public choiceListAdapter(Context context,
                                 ArrayList<ChoiceModel> choiceList, int currCheckIndex) {
            this.mContext = context;
            this.choiceList = choiceList;
            this.currCheckIndex = currCheckIndex;
            inflater = LayoutInflater.from(this.mContext);
        }

        @Override
        public int getCount() {
            return choiceList.size();
        }

        @Override
        public Object getItem(int arg0) {
            return choiceList.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {
            ViewHolder viewHolder;
            if (arg1 == null) {
                viewHolder = new ViewHolder();
                arg1 = inflater.inflate(R.layout.item_choice_window_list, null);
                viewHolder.content_1 = (TextView) arg1
                        .findViewById(R.id.txt_choice_content1);
                viewHolder.content_2 = (TextView) arg1
                        .findViewById(R.id.txt_choice_content2);
                viewHolder.ischecked = (ImageView) arg1.findViewById(R.id.ischecked);
                arg1.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) arg1.getTag();
            }
            ChoiceModel item = (ChoiceModel) getItem(arg0);
            if (item.content_1 != null
                    && !StrUtils.stringIsEmpty(item.content_1)) {
                viewHolder.content_1.setVisibility(View.VISIBLE);
                viewHolder.content_1.setText(item.content_1);
            } else {
                viewHolder.content_1.setVisibility(View.GONE);
            }
            if (item.content_2 != null
                    && !StrUtils.stringIsEmpty(item.content_2)) {
                viewHolder.content_2.setVisibility(View.VISIBLE);
                viewHolder.content_2.setText(item.content_2);
            } else {
                viewHolder.content_2.setVisibility(View.GONE);
            }

            if (arg0 == currCheckIndex) {
                viewHolder.ischecked.setVisibility(View.VISIBLE);
            } else {

                viewHolder.ischecked.setVisibility(View.INVISIBLE);
            }

            return arg1;
        }

    }

    static class ViewHolder {
        TextView content_1;
        TextView content_2;
        ImageView ischecked;

    }

}
