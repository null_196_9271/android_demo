package com.uzhu.project.widget.pager;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;

import com.mvvm.framework.util.LogUtil;

import java.util.ArrayList;

public abstract class PagerBaseAdapter extends BaseAdapter {

	public Context context;
	public ArrayList<View> pagerArrayList;

	public void snapToPage(int pageIndex) {
		LogUtil.i("<>", "snapToPage" + pageIndex);
	}

	public PagerBaseAdapter(Context context) {
		this.context = context;
		pagerArrayList = new ArrayList<View>();
	}

	public void cycleBitmap(int position) {
		
	}
	
	public void loadImage(int position) {
		
	}

	public void cleanChildrenViews() {
		pagerArrayList.clear();
	}
	

}
