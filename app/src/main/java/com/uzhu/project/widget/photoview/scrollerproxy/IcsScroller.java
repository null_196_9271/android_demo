/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: IcsScroller.java
 * Author:   jeremy
 * Date:     2015-10-13 19:58:17
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-13 19:58:17> <version>   <desc>
 */
package com.uzhu.project.widget.photoview.scrollerproxy;

import android.annotation.TargetApi;
import android.content.Context;

@TargetApi(14)
public class IcsScroller extends GingerScroller {

    public IcsScroller(Context context) {
        super(context);
    }

    @Override
    public boolean computeScrollOffset() {
        return mScroller.computeScrollOffset();
    }

}
