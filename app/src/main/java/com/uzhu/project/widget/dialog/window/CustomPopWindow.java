/*
 * CustomPopWindow.java
 *
 *  Created by: shard on 2015-7-18
 *  Copyright (c) 2015 shard. All rights reserved.
 */
package com.uzhu.project.widget.dialog.window;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.uzhu.project.R;


public class CustomPopWindow implements OnClickListener {
	private ScrollView contentSv;
	private Activity mContext;
	private PopupWindow popupWindow;
	private View contentLayout;
	private View outSideTop;
	private View outSideBottom;
	private TextView titleTxt;
	private ImageView closeIv;

	public CustomPopWindow(Activity context) {
		mContext = context;
	}

	public void openWindow(String windowTitle) {
		View parentView = ((ViewGroup) mContext
				.findViewById(android.R.id.content)).getChildAt(0);
		LayoutInflater mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout popview = (LinearLayout) mLayoutInflater.inflate(
				R.layout.view_window_view, null, true);
		// 视图初始化
		initView(popview);
		titleTxt.setText(windowTitle);
		WindowManager wm = (WindowManager) mContext
				.getSystemService(Context.WINDOW_SERVICE);
		int width = wm.getDefaultDisplay().getWidth();
		int height = wm.getDefaultDisplay().getHeight();
		popupWindow = new PopupWindow(popview, width, height);
		// 设置动画
		popupWindow.setAnimationStyle(R.style.popwindow_anim_from_fade_style);
		popupWindow.isOutsideTouchable();
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(true);
		popupWindow.showAtLocation(parentView, Gravity.NO_GRAVITY, 0, 0);
		popupWindow.update();

		Animation animation = AnimationUtils.loadAnimation(mContext,
				R.anim.push_top_in);
		contentLayout.setAnimation(animation);
	}

	private void initView(View v) {
		contentSv = (ScrollView) v.findViewById(R.id.sv_content);
		contentLayout = v.findViewById(R.id.content_layout);
		outSideTop = v.findViewById(R.id.outside_top);
		outSideBottom = v.findViewById(R.id.outside_bottom);
		titleTxt = (TextView) v.findViewById(R.id.txt_title);
		closeIv = (ImageView) v.findViewById(R.id.iv_close);
		outSideTop.setOnClickListener(this);
		outSideBottom.setOnClickListener(this);
		closeIv.setOnClickListener(this);
	}

	public void setContentView(View v) {
		if (v != null) {
			contentSv.addView(v);
			new Handler().post(new Runnable() {

				@Override
				public void run() {
					contentSv.fullScroll(ScrollView.FOCUS_UP);
				}
			});
		}
	}

	public void dismiss() {
		if (popupWindow != null) {
			popupWindow.dismiss();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.outside_top:
		case R.id.outside_bottom:
		case R.id.iv_close:
			dismiss();
			break;

		default:
			break;
		}
	}
}
