package com.uzhu.project.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.uzhu.project.R;


public class ClearEditText extends EditText implements OnFocusChangeListener,
		TextWatcher {
	private Drawable mClearDrawable;
	private boolean hasFoucs;
	private boolean clearEnable = true;
	private OnTextWatcher mOnTextWatcher;

	public ClearEditText(Context context) {
		this(context, null);
	}

	public ClearEditText(Context context, AttributeSet attrs) {
		this(context, attrs, android.R.attr.editTextStyle);
	}

	public ClearEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public void setOnTextWatcher(OnTextWatcher onTextWatcher) {
		this.mOnTextWatcher = onTextWatcher;
	}

	public void setClearEnable(boolean enable) {
		this.clearEnable = enable;
	}

	private void init() {
		mClearDrawable = getCompoundDrawables()[2];
		if (mClearDrawable == null) {
			mClearDrawable = getResources().getDrawable(
					R.drawable.selector_btn_delete);
		}

		mClearDrawable.setBounds(0, 0, mClearDrawable.getIntrinsicWidth(),
				mClearDrawable.getIntrinsicHeight());
//		setInputType(InputType.TYPE_CLASS_TEXT |
//		 InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS |
//		 InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		setClearIconVisible(false);
		setOnFocusChangeListener(this);
		addTextChangedListener(this);
		this.setSingleLine();
	}

	public boolean isEmpty() {
		String str = getText().toString();
		return (str == null || str.equals(""));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP) {
			if (getCompoundDrawables()[2] != null) {

				boolean touchable = event.getX() > (getWidth() - getTotalPaddingRight())
						&& (event.getX() < ((getWidth() - getPaddingRight())));

				if (touchable) {
					this.setText("");
					// clearFocus();
				}
			}
		}

		return super.onTouchEvent(event);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		this.hasFoucs = hasFocus;
		if (hasFocus) {
			setClearIconVisible(getText().length() > 0);
		} else {
			setClearIconVisible(false);
		}
	}

	protected void setClearIconVisible(boolean visible) {
		if (clearEnable) {
			Drawable right = visible ? mClearDrawable : null;
			setCompoundDrawables(getCompoundDrawables()[0],
					getCompoundDrawables()[1], right, getCompoundDrawables()[3]);
		}
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int count, int after) {
		if (hasFoucs && clearEnable) {
			setClearIconVisible(s.length() > 0);
		}
		if (mOnTextWatcher != null) {
			mOnTextWatcher.onTextChanged(s, start, count, after);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
								  int after) {
		if (mOnTextWatcher != null) {
			mOnTextWatcher.beforeTextChanged(s, start, count, after);
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		if (mOnTextWatcher != null) {
			mOnTextWatcher.afterTextChanged(s);
		}
	}

	public interface OnTextWatcher {
		void beforeTextChanged(CharSequence s, int start, int count,
							   int after);

		void onTextChanged(CharSequence s, int start, int count,
						   int after);

		void afterTextChanged(Editable s);
	}
}
