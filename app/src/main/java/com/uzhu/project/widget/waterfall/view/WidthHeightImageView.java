package com.uzhu.project.widget.waterfall.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * An {@link ImageView} layout that maintains a consistent width
 * to height aspect ratio.
 */
public class WidthHeightImageView extends ImageView {

	public WidthHeightImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public WidthHeightImageView(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		// // set the image views size
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = (int) (width * 0.5);
		setMeasuredDimension(width, height);
	}
}
