package com.uzhu.project.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

import com.uzhu.project.util.UiUtil;


public class ListViewForScrollView extends ListView {
	private Context context;

	public ListViewForScrollView(Context context) {
		super(context);
		this.context = context;
	}

	public ListViewForScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public ListViewForScrollView(Context context, AttributeSet attrs,
								 int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	@Override
	/**
	 * 重写该方法，达到使ListView适应ScrollView的效果
	 */
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(UiUtil.dp2px(context, 300),
				MeasureSpec.AT_MOST);
		super.onMeasure(widthMeasureSpec, expandSpec);
	}
}
