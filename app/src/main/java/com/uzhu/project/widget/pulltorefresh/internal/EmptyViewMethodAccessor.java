/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: EmptyViewMethodAccessor.java
 * Author:   jeremy
 * Date:     2015-10-08 15:59:02
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-08 15:59:02> <version>   <desc>
 */
package com.uzhu.project.widget.pulltorefresh.internal;

import android.view.View;

/**
 * Interface that allows PullToRefreshBase to hijack the call to
 * AdapterView.setEmptyView()
 * 
 * @author chris
 */
public interface EmptyViewMethodAccessor {

	/**
	 * Calls upto AdapterView.setEmptyView()
	 * 
	 * @param emptyView - to set as Empty View
	 */
	public void setEmptyViewInternal(View emptyView);

	/**
	 * Should call PullToRefreshBase.setEmptyView() which will then
	 * automatically call through to setEmptyViewInternal()
	 * 
	 * @param emptyView - to set as Empty View
	 */
	public void setEmptyView(View emptyView);

}
