package com.uzhu.project.widget.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.uzhu.project.R;


/**
 * Create custom Dialog windows for your application Custom dialogs rely on
 * custom layouts wich allow you to create and use your own look & feel.
 * <p>
 * Under GPL v3 : http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @author antoine vianey
 */
public class CustomProgressDialog extends Dialog {

    public static CustomProgressDialogListener customProgressDialogListener;
    public Builder builder;

    public void setCustomProgressDialogListener(
            CustomProgressDialogListener customProgressDialogListener) {
        this.customProgressDialogListener = customProgressDialogListener;

    }

    public CustomProgressDialog(Context context) {
        super(context);

    }

    public CustomProgressDialog(Context context, int dialog) {
        super(context, dialog);
    }

    public Builder getBuilder() {
        return builder;
    }

    /**
     * Helper class for creating a custom dialog
     */
    public static class Builder {
        private Activity activity;
        public ImageView loding_bg;
        public ImageView cancle;
        public TextView titleTv;

        public Builder(Activity activity) {
            this.activity = activity;
        }

        /**
         * Create the custom dialog
         */
        public CustomProgressDialog create(boolean hasCancle) {
            final CustomProgressDialog dialog = new CustomProgressDialog(
                    activity, R.style.Dialog);
            LayoutInflater inflater = LayoutInflater.from(activity);
            View layout = inflater.inflate(
                    R.layout.common_custom_progress_dialog, null);
            ImageView progressBar = ((ImageView) layout
                    .findViewById(R.id.loding_bg));
            cancle = (ImageView) layout.findViewById(R.id.cancle);
            titleTv = (TextView) layout.findViewById(R.id.tv_title);
            if (hasCancle) {
                cancle.setVisibility(View.VISIBLE);
            } else {
                cancle.setVisibility(View.GONE);
            }
            cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    customProgressDialogListener.onCancleClick(dialog);
                }
            });
            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            dialog.builder = this;
            return dialog;
        }

        public CustomProgressDialog createNew() {
            final CustomProgressDialog dialog = new CustomProgressDialog(
                    activity, R.style.Dialog);
            LayoutInflater inflater = LayoutInflater.from(activity);
            View layout = inflater.inflate(
                    R.layout.common_progress_dialog, null);
            ImageView loadingIcon = (ImageView) layout.findViewById(R.id.loading_icon);
            ((AnimationDrawable) loadingIcon.getBackground()).start();
            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            dialog.builder = this;
            return dialog;
        }

        // 旋转动画
        private void startRotateAnimation(final View view) {
            RotateAnimation rotateAnimation = new RotateAnimation(0f, 360f,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);
            rotateAnimation.setDuration(3000);
            rotateAnimation.setRepeatCount(-1);
            LinearInterpolator lin = new LinearInterpolator();
            rotateAnimation.setInterpolator(lin);
            view.startAnimation(rotateAnimation);
        }

        public void setTitle(String title) {
            titleTv.setText(title);
        }
    }

    public interface CustomProgressDialogListener {
        public void onCancleClick(CustomProgressDialog dialog);
    }

}