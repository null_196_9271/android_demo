package com.uzhu.project.widget.waterfall.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * An {@link ImageView} layout that maintains a consistent width
 * to height aspect ratio.
 */
public class DynamicHeightImageView extends ImageView {

	private double mHeightRatio = 0;
	private boolean scaleToWidth = false; // this flag determines if should
											// measure height manually dependent
											// of width

	public DynamicHeightImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public DynamicHeightImageView(Context context) {
		super(context);
	}

	public void setHeightRatio(double ratio) {
		if (ratio != mHeightRatio) {
			mHeightRatio = ratio;
			requestLayout();
		}
	}

	public double getHeightRatio() {
		return mHeightRatio;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mHeightRatio == 0) {
			int widthMode = MeasureSpec.getMode(widthMeasureSpec);
			int heightMode = MeasureSpec.getMode(heightMeasureSpec);
			int width = MeasureSpec.getSize(widthMeasureSpec);
			int height = MeasureSpec.getSize(heightMeasureSpec);

			/**
			 * if both width and height are set scale width first. modify in
			 * future if necessary
			 */

			if (widthMode == MeasureSpec.EXACTLY
					|| widthMode == MeasureSpec.AT_MOST) {
				scaleToWidth = true;
			} else if (heightMode == MeasureSpec.EXACTLY
					|| heightMode == MeasureSpec.AT_MOST) {
				scaleToWidth = false;
			} else
				throw new IllegalStateException(
						"width or height needs to be set to match_parent or a specific dimension");

			if (getDrawable() == null || getDrawable().getIntrinsicWidth() == 0) {
				// nothing to measure
				super.onMeasure(widthMeasureSpec, heightMeasureSpec);
				return;
			} else {
				if (scaleToWidth) {
					int iw = this.getDrawable().getIntrinsicWidth();
					int ih = this.getDrawable().getIntrinsicHeight();
					int heightC = width * ih / iw;
					if (height > 0)
						if (heightC > height) {
							// dont let hegiht be greater then set max
							heightC = height;
							width = heightC * iw / ih;
						}

					this.setScaleType(ScaleType.CENTER_CROP);
					setMeasuredDimension(width, heightC);

				} else {
					// need to scale to height instead
					int marg = 0;
					if (getParent() != null) {
						if (getParent().getParent() != null) {
							marg += ((RelativeLayout) getParent().getParent())
									.getPaddingTop();
							marg += ((RelativeLayout) getParent().getParent())
									.getPaddingBottom();
						}
					}

					int iw = this.getDrawable().getIntrinsicWidth();
					int ih = this.getDrawable().getIntrinsicHeight();

					width = height * iw / ih;
					height -= marg;
					setMeasuredDimension(width, height);
				}

			}
		} else if (mHeightRatio > 0.0) {
			// // set the image views size
			int width = MeasureSpec.getSize(widthMeasureSpec);
			int height = (int) (width * mHeightRatio);
			setMeasuredDimension(width, height);
		} else {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}

	}
}
