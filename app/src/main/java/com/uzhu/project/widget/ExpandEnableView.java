/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: ExpandEnableView.java
 * Author:   jeremy
 * Date:     2015-10-09 17:31:50
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-09 17:31:50> <version>   <desc>
 */

package com.uzhu.project.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.R;
import com.uzhu.project.util.AppUtil;


public class ExpandEnableView extends RelativeLayout {
    private TextView expandTextView;
    private View expandLayout;
    private ToggleButton toggleButton;

    private static int MAX = 5;//初始maxLine大小
    private static final int TIME = 20;//间隔时间
    public int maxLines = 0;//计算显示的文案的行数
    private boolean hasMesure = false;
    private Thread thread;


    private ExpandChangeListener mExpandChangeListener;

    public void setmExpandChangeListener(ExpandChangeListener mExpandChangeListener) {
        this.mExpandChangeListener = mExpandChangeListener;
    }

    public ExpandEnableView(Context context) {
        super(context);
        initView();

    }

    public ExpandEnableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();

    }

    public ExpandEnableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {

    }


    /**
     * 控件展开和折叠的回调。
     */
    public interface ExpandChangeListener {
        /**
         * 展开
         */
        void expand();

        /**
         * 折叠
         */
        void fold();
    }

    /**
     * 根据子view高度控制viewgroup的高度
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int cCount = getChildCount();
        int allHeight = 0;
        int cHeight = 0;
        for (int i = 0; i < cCount; i++) {
            View childView = getChildAt(i);
            cHeight = childView.getMeasuredHeight();
            allHeight += cHeight;
        }
        super.onMeasure(widthMeasureSpec, allHeight);
    }


    /**
     * @param myExpandLayout
     */
    public void setExpandView(int maxLines, String myExpandText, View myExpandLayout) {

        if (maxLines > 0) {
            MAX = maxLines;
        }

        //代码构建试图的显示位置及顺序
        if (myExpandText != null && myExpandText.trim().length() > 0) {
            this.expandTextView = new TextView(getContext());
            LayoutParams textLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            textLayoutParams.setMargins(10, 0, 10, 0);
            textLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            //获取一个可用id避免重复id
            this.expandTextView.setText(myExpandText);
            this.expandTextView.setId(AppUtil.generateViewId());
            this.expandTextView.setLayoutParams(textLayoutParams);
            this.addView(this.expandTextView);
        }


        if (myExpandLayout != null) {
            //自定显示的layout
            this.expandLayout = myExpandLayout;
            LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            //判断是否有textview
            if (this.expandTextView == null) {
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            } else {
                layoutParams.addRule(RelativeLayout.BELOW, this.expandTextView.getId());
            }
            this.expandLayout.setId(AppUtil.generateViewId());
            this.expandLayout.setLayoutParams(layoutParams);
            this.addView(this.expandLayout);

            //add button
            toggleButton = getDefaultButton(this.expandLayout.getId());
            this.addView(toggleButton);
        } else {
            if (this.expandTextView == null) {
                //两个视图都为空不添加button
            } else {
                toggleButton = getDefaultButton(this.expandTextView.getId());
                this.addView(toggleButton);
            }
        }

        //判断当前view的展开状态
        onExpandLayout();
    }


    /**
     * 判断当前view的展开状态
     */
    private void onExpandLayout() {
        if (expandTextView != null) {
            ViewTreeObserver viewTreeObserver = expandTextView.getViewTreeObserver();
            viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    //只需要获取一次就可以了
                    if (!hasMesure) {
                        //这里获取到完全展示的maxLine
                        maxLines = expandTextView.getLineCount();
                        //设置maxLine的默认值，这样用户看到View就是限制了maxLine的TextView
                        expandTextView.setEllipsize(TextUtils.TruncateAt.END);
                        expandTextView.setMaxLines(MAX);
                        expandTextView.postInvalidate();

                        if (maxLines <= MAX) {
                            //展开
                            setExpandLayout(true);
                        } else {
                            //折叠
                            setExpandLayout(false);
                        }
                        hasMesure = true;
                    }
                    return true;
                }
            });
        } else {
            //展开
            setExpandLayout(true);
        }

    }

    private void setExpandLayout(boolean isExpand) {
        if (expandLayout != null) {
            //展开还是折叠
            expandLayout.setVisibility(isExpand ? View.VISIBLE : View.GONE);
        }

        if (toggleButton != null) {
            toggleButton.setChecked(isExpand);
        }
    }

    private ToggleButton getDefaultButton(int topViewId) {
        ToggleButton toggleButton = new ToggleButton(getContext());
        LayoutParams btnLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        btnLayoutParams.addRule(RelativeLayout.BELOW, topViewId);
        toggleButton.setTextOff("点击展开");
        toggleButton.setTextOn("点击收起");
        btnLayoutParams.setMargins(20, 20, 20, 20);
        toggleButton.setBackgroundResource(R.color.blue_04);
        toggleButton.setOnCheckedChangeListener(new btnClick());
        toggleButton.setLayoutParams(btnLayoutParams);
        return toggleButton;
    }


    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int lines = msg.what;
            //这里接受到消息，让后更新TextView设置他的maxLine就行了
            expandTextView.setEllipsize(TextUtils.TruncateAt.END);
            expandTextView.setMaxLines(lines);
            expandTextView.postInvalidate();

        }
    };

    private void toggleExpandTextView(boolean isExpand) {
        if (isExpand) {
            if (thread != null)
                handler.removeCallbacks(thread);

            thread = new Thread() {
                @Override
                public void run() {
                    int count = MAX;
                    while (count++ <= maxLines) {
                        //每隔20mms发送消息
                        Message message = new Message();
                        message.what = count;
                        handler.sendMessage(message);

                        try {
                            Thread.sleep(TIME);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    super.run();
                }
            };
            thread.start();
        } else {
            expandTextView.setEllipsize(TextUtils.TruncateAt.END);
            expandTextView.setMaxLines(MAX);
            expandTextView.postInvalidate();

        }


    }

    public class btnClick implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            LogUtil.d(LogUtil.MYTAG, "isChecked=" + isChecked);
            if (isChecked) {
                //展开
                expandLayout.setVisibility(View.VISIBLE);
                toggleExpandTextView(isChecked);
                if (mExpandChangeListener != null) {
                    mExpandChangeListener.expand();
                }
            } else {
                //折叠
                expandLayout.setVisibility(View.GONE);
                toggleExpandTextView(isChecked);
                if (mExpandChangeListener != null) {
                    mExpandChangeListener.fold();
                }
            }

        }
    }


    public TextView getExpandTextView() {
        return expandTextView;
    }

    public View getExpandLayout() {
        return expandLayout;
    }

    public ToggleButton getToggleButton() {
        return toggleButton;
    }
}
