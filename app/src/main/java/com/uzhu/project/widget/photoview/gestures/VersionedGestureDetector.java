/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: VersionedGestureDetector.java
 * Author:   jeremy
 * Date:     2015-10-13 19:57:47
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-13 19:57:47> <version>   <desc>
 */

package com.uzhu.project.widget.photoview.gestures;

import android.content.Context;
import android.os.Build;

public final class VersionedGestureDetector {

    public static GestureDetector newInstance(Context context,
                                              OnGestureListener listener) {
        final int sdkVersion = Build.VERSION.SDK_INT;
        GestureDetector detector;

        if (sdkVersion < Build.VERSION_CODES.ECLAIR) {
            detector = new CupcakeGestureDetector(context);
        } else if (sdkVersion < Build.VERSION_CODES.FROYO) {
            detector = new EclairGestureDetector(context);
        } else {
            detector = new FroyoGestureDetector(context);
        }

        detector.setOnGestureListener(listener);

        return detector;
    }

}