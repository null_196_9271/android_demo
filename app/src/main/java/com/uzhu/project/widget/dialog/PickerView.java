package com.uzhu.project.widget.dialog;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * 滚动选择器
 */
public class PickerView extends View {

	public static final String TAG = "PickerView";
	/**
	 * text之间间距和minTextSize之比
	 */
	public static final float MARGIN_ALPHA = 3f;
	/**
	 * 自动回滚到中间的速度
	 */
	public static final float SPEED = 2;

//	private List<String> mDataList;
	/**
	 * 选中的位置，这个位置是mDataList的中心位置，一直不变
	 */
	private int mCurrentSelected;
//	private int defaultIndex;
	private Paint mPaint;
	private Paint linePaint;

	private float mMaxTextSize = 80;
	private float mMinTextSize = 30;

	private float mMaxTextAlpha = 255;
	private float mMinTextAlpha = 120;

	private int mLineColor = 0x999999;
	private int mNormalTextColor = 0xcccccc;
	private int mHighlightTextColor = 0x666666;

	private int mViewHeight;
	private int mViewWidth;

	private float mLastDownY;

	private float finalTop = 0;
	private float finalBottom = 0;
	private float finalBaseline = 0;
	/**
	 * 滑动的距离
	 */
	private float mMoveLen = 0;
	private boolean isInit = false;
	private onSelectListener mSelectListener;
//	private Timer timer;
//	private MyTimerTask mTask;
	// 是否可以循环滚动
	private boolean isCycle = false;
	Handler updateHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (Math.abs(mMoveLen) < SPEED) {
				mMoveLen = 0;
//				if (mTask != null) {
//					mTask.cancel();
//					mTask = null;
//				}
				performSelect();
			} else {
				// 这里mMoveLen / Math.abs(mMoveLen)是为了保有mMoveLen的正负号，以实现上滚或下滚
				mMoveLen = mMoveLen - mMoveLen / Math.abs(mMoveLen) * SPEED;
				updateHandler.sendEmptyMessageDelayed(0, 10);
			}
			invalidate();
		}

	};

	public PickerView(Context context) {
		super(context);
		init();
	}

	public PickerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void setOnSelectListener(onSelectListener listener) {
		mSelectListener = listener;
	}

	private void performSelect() {
		if (mSelectListener != null) {
			mSelectListener.onSelect(mCurrentSelected);
		}
	}

//	@Deprecated
//	public void setData(List<String> datas) {
//		setData(datas, 0);
//	}
//	@Deprecated
//	public void setData(List<String> datas, int defaultIndex) {
//		if (datas != null && !datas.isEmpty()) {
//			mDataList.clear();
//			mDataList.addAll(datas);
//			// 默认选中中间位置
////			defaultIndex = mCurrentSelected = datas.size() / 2;
//			mCurrentSelected = defaultIndex;
//			invalidate();
//		}
//	}
	private int mCount;
	public void setData(int count, OnPickerViewAdapter adapter) {
		mCount = count;
		mOnPickerViewAdapter = adapter;
	}
	public void setCurrentIndex(int index) {
		if (mOnPickerViewAdapter != null) {
			if (index < 0 || mCount <= 0 || index >= mCount) {
				throw new IllegalArgumentException("error current position");
			}
		} else if (index < 0) {
			throw new IllegalArgumentException("error current position");
		}
		mCurrentSelected = index;
		invalidate();
	}

	public boolean isCycle() {
		return isCycle;
	}

	public void setCycle(boolean isCycle) {
		this.isCycle = isCycle;
	}
	public int getCurrentIndex() {
		return mCurrentSelected;
	}

	/**
	 * 初始化并选中列表中间
	 */
//	public void initializeMiddle() {
//		if (mDataList != null && mDataList.size() > 0) {
//			for (int i = 0; i < mDataList.size() / 2; i++) {
//				moveTailToHead();
//			}
//			invalidate();
//		}
//	}

	private boolean moveUp() {
//		if (isCycle) {
//			return moveHeadToTail();
//		} else {
			if (mCurrentSelected < (mCount - 1)) {
				mCurrentSelected++;
				return true;
			} else {
				return false;
			}
//		}
	}

	private boolean moveDown() {
//		if (isCycle) {
//			return moveTailToHead();
//		} else {
			if (mCurrentSelected > 0) {
				mCurrentSelected--;
				return true;
			} else {
				return false;
			}
//		}
	}

	/**
	 * （每向上滑动一格）把列表中第一位移动到队尾
	 */
//	private boolean moveHeadToTail() {
//		String head = mDataList.get(0);
//		mDataList.remove(0);
//		mDataList.add(head);
//		return true;
//	}

	/**
	 * （每向下滑动一格）把列表最后一位移动到队首
	 */
//	private boolean moveTailToHead() {
//		String tail = mDataList.get(mDataList.size() - 1);
//		mDataList.remove(mDataList.size() - 1);
//		mDataList.add(0, tail);
//		return true;
//	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		mViewHeight = getMeasuredHeight();
		mViewWidth = getMeasuredWidth();
		// 按照View的高度计算字体大小
		mMaxTextSize = mViewHeight / 8.0f;
		mMinTextSize = mMaxTextSize / 2f;
		isInit = true;
		invalidate();
	}

	private void init() {
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setStyle(Style.FILL);
		mPaint.setTextAlign(Align.CENTER);
		mPaint.setColor(mNormalTextColor);
		linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePaint.setStyle(Style.FILL);
		linePaint.setColor(mHighlightTextColor);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// 根据index绘制view
		if (isInit)
			drawData(canvas);
		FontMetricsInt fmi = mPaint.getFontMetricsInt();
//		float x = (float) (mViewWidth / 2.0);
		float x = mViewWidth;

		float baseline = (float) ((mViewHeight - fmi.top - fmi.bottom) / 2.0);
		if (finalTop == 0)
			finalTop = fmi.top;
		if (finalBottom == 0)
			finalBottom = fmi.bottom;
		if (finalBaseline == 0)
			finalBaseline = baseline;
		mPaint.setStrokeWidth(1.0f); // 设置线宽
//		canvas.drawLine(x * 0.7f, finalBaseline + finalTop, x * 1.3f,
//				finalBaseline + finalTop, mPaint); // 绘制直线
		canvas.drawLine(x * 0, finalBaseline + finalTop, x * 1.0f,
				finalBaseline + finalTop, mPaint); // 绘制直线
		mPaint.setStrokeWidth(1.0f); // 设置线宽
		canvas.drawLine(x * 0, finalBaseline + finalBottom, x * 1.0f,
				finalBaseline + finalBottom, mPaint); // 绘制直线
	}

	private void drawData(Canvas canvas) {
		// 绘制上方data
		for (int i = 1; (mCurrentSelected - i) >= 0; i++) {
			drawOtherText(canvas, i, -1);
		}
		// 绘制下方data
		for (int i = 1;(mCurrentSelected + i) < mCount; i++) {
			drawOtherText(canvas, i, 1);
		}
		// 绘制选中的text
		float scale = parabola(mViewHeight / 4.0f, mMoveLen);
		float size = (mMaxTextSize - mMinTextSize) * scale + mMinTextSize;
		mPaint.setColor(mHighlightTextColor);
		mPaint.setTextSize(size);
		mPaint.setAlpha((int) ((mMaxTextAlpha - mMinTextAlpha) * scale + mMinTextAlpha));
		// text居中绘制，注意baseline的计算才能达到居中，y值是text中心坐标
		float x = (float) (mViewWidth / 2.0);
		float y = (float) (mViewHeight / 2.0 + mMoveLen);
		FontMetricsInt fmi = mPaint.getFontMetricsInt();
		float baseline = (float) (y - (fmi.top + fmi.bottom) / 2.0);
		if (mCurrentSelected < mCount) {
			if (mOnPickerViewAdapter != null) {
				canvas.drawText(mOnPickerViewAdapter.getItem(mCurrentSelected).toString(), x, baseline,
						mPaint);
			}
		}

	}

	/**
	 * @param canvas
	 * @param position 距离mCurrentSelected的差值
	 * @param type     1表示向下绘制，-1表示向上绘制
	 */
	private void drawOtherText(Canvas canvas, int position, int type) {
		float d = (MARGIN_ALPHA * mMinTextSize * position + type
				* mMoveLen);
		float scale = parabola(mViewHeight / 4.0f, d);
		float size = (mMaxTextSize - mMinTextSize) * scale + mMinTextSize;
		mPaint.setColor(mNormalTextColor);
		mPaint.setTextSize(size);
		mPaint.setAlpha((int) ((mMaxTextAlpha - mMinTextAlpha) * scale + mMinTextAlpha));
		float y = (float) (mViewHeight / 2.0 + type * d);
		FontMetricsInt fmi = mPaint.getFontMetricsInt();
		float baseline = (float) (y - (fmi.bottom / 2.0 + fmi.top / 2.0));
		if (mOnPickerViewAdapter != null) {
			canvas.drawText(mOnPickerViewAdapter.getItem(mCurrentSelected + type * position).toString(),
					(float) (mViewWidth / 2.0), baseline, mPaint);
		}
	}

	/**
	 * 抛物线
	 *
	 * @param zero 零点坐标
	 * @param x    偏移量
	 * @return scale
	 */
	private float parabola(float zero, float x) {
		float f = (float) (1 - Math.pow(x / zero, 2));
		return f < 0 ? 0 : f;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN:
				doDown(event);
				break;
			case MotionEvent.ACTION_MOVE:
				doMove(event);
				break;
			case MotionEvent.ACTION_UP:
				doUp(event);
				break;
			default:
				break;
		}
		return true;
	}

	private void doDown(MotionEvent event) {
//		if (mTask != null) {
//			mTask.cancel();
//			mTask = null;
//		}
		updateHandler.removeMessages(0);
		mLastDownY = event.getY();
	}

	private void doMove(MotionEvent event) {
		mMoveLen += (event.getY() - mLastDownY);
		
		float maxMargin = MARGIN_ALPHA * mMinTextSize / 2;

		if (mMoveLen > maxMargin) {
			// 往下滑超过离开距离
			boolean down = moveDown();
			if (down) {
				mMoveLen = mMoveLen - MARGIN_ALPHA * mMinTextSize;
			} else {
				mMoveLen = maxMargin;
			}

		} else if (mMoveLen < -maxMargin) {
			// 往上滑超过离开距离
			boolean up = moveUp();
			if (up) {
				mMoveLen = mMoveLen + MARGIN_ALPHA * mMinTextSize;
			} else {
				mMoveLen = -maxMargin;
			}
		}

		mLastDownY = event.getY();
		invalidate();
	}

	private void doUp(MotionEvent event) {
		// 抬起手后mCurrentSelected的位置由当前位置move到中间选中位置
		if (updateHandler.hasMessages(0)) {
			updateHandler.removeMessages(0);
		}
		if (Math.abs(mMoveLen) < 0.0001) {
			mMoveLen = 0;
			return;
		}
		updateHandler.sendEmptyMessage(0);
	}

	/**
	 * currIndex参数在isCycle=true时不起作用，因为在循环滚动模式下，currIndex是不变的
	 *
	 * @author JeremyAiYt
	 */
	public interface onSelectListener {
		void onSelect(int currIndex);
	}

	/**
	 * 如果数量不一致，则刷新；如果需要立即刷新，则刷新
	 * @param count
	 * @param immediately
	 */
	public void notifyDataSetChanged(int count, boolean immediately) {
		if (mCount != count || immediately) {
			mCount = count;
			if (mCurrentSelected >= mCount) {
				mCurrentSelected = mCount - 1;
			}
			invalidate();
		}
	}
	private OnPickerViewAdapter mOnPickerViewAdapter;
	public void setOnPickerViewAdapter(OnPickerViewAdapter inf) {
		mOnPickerViewAdapter = inf;
	}
	public interface OnPickerViewAdapter {
		CharSequence getItem(int position);
	}
}
