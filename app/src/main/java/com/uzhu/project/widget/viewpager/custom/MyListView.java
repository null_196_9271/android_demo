package com.uzhu.project.widget.viewpager.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.mvvm.framework.util.LogUtil;

public class MyListView extends ListView implements OnScrollListener {

	private View parentView;
	public boolean isScrollHeader = false;
	public boolean isScrollFoot = false;

	public void setParentView(View parentView) {
		this.parentView = parentView;
	}
	
	public void initialize(){
		isScrollHeader=false;
		isScrollFoot=false;
	}
	
	public MyListView(Context context) {
		super(context);
		this.setOnScrollListener(this);
	}

	public MyListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.setOnScrollListener(this);
	}

	public MyListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setOnScrollListener(this);
	}

	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		LogUtil.d("CC", "dispatchTouchEvent");
		if (isScrollHeader) {
			// return ((MyViewPager) parentView).onTouchEvent(ev);
			return false;
		} else {
			return super.onTouchEvent(ev);
		}
		//
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		LogUtil.d("CC", "onInterceptTouchEvent");
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		LogUtil.d("CC", "onTouchEvent");
		if (isScrollHeader) {
			return ((MyViewPager) parentView).dispatchTouchEvent(ev);
			// return ((MyViewPager) parentView).onTouchEvent(ev);
		} else {
			return super.dispatchTouchEvent(ev);
		}
	}

	@Override
	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
		LogUtil.d("CC", "onScroll~~~");
	}

	@Override
	public void onScrollStateChanged(AbsListView arg0, int scrollState) {
		LogUtil.d("CC", "onScrollStateChanged");
		switch (scrollState) {
		// 当不滚动时
		case OnScrollListener.SCROLL_STATE_IDLE:
			if (this.getFirstVisiblePosition() == 0) {// 判断滚动到顶部
				LogUtil.i("CC", "顶部~~~");
				isScrollHeader = true;
				isScrollFoot = false;
			} else if (this.getLastVisiblePosition() == (this.getCount() - 1)) {
				// 判断滚动到底部
				LogUtil.i("CC", "底部~~~");
				isScrollHeader = false;
				isScrollFoot = true;
			} else {
				isScrollHeader = false;
				isScrollFoot = false;
			}
			break;
		// case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
		// if (this.getFirstVisiblePosition() == 0&&!isScrollHeader) {
		// isScrollHeader = true;
		// isScrollFoot = false;
		// }
		// break;
		}
	}

}
