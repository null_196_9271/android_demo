package com.uzhu.project.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uzhu.project.R;


/**
 * Created by shard on 16/5/24.
 */
public class CommonAlert extends Dialog {
	private TextView titleTv, commonCancelTv, commonConfirmTv, contentTv;
	private LinearLayout contentLl;
	public View seperatorLine;

	public CommonAlert(Context context, AlertListener listener) {
		this(context, R.style.Dialog, listener);
	}

	public CommonAlert(Context context, int theme, AlertListener listener) {
		super(context, theme);
		init(context, listener);
	}

	private void init(Context context, final AlertListener listener) {
		View view = LayoutInflater.from(context).inflate(R.layout.common_alert, null);
		setContentView(view);
		titleTv = (TextView) view.findViewById(R.id.dialog_titile);
		commonCancelTv = (TextView) view.findViewById(R.id.dialog_cancel_tv);
		commonConfirmTv = (TextView) view.findViewById(R.id.dialog_confirm_tv);
		contentTv = (TextView) view.findViewById(R.id.tv_content);
		contentLl = (LinearLayout) view.findViewById(R.id.ll_content);
		seperatorLine = view.findViewById(R.id.dialog_line_v);
		commonCancelTv
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						listener.onCancelClick(CommonAlert.this);
					}
				});
		commonConfirmTv
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						listener.onConfirmClick(CommonAlert.this);
					}
				});
	}

	public void setTitle(String title) {
		titleTv.setText(title);
	}

	public void setCancelTitle(String cancelTitle) {
		commonCancelTv.setText(cancelTitle);
	}

	public void setConfirmTitle(String confirmTitle) {
		commonConfirmTv.setText(confirmTitle);
	}

	public void setContent(String content) {
		contentTv.setVisibility(View.VISIBLE);
		contentTv.setText(content);
	}

	public void setContent(View view) {
		contentLl.removeAllViews();
		contentLl.addView(view);
	}


	public interface AlertListener {
		void onConfirmClick(CommonAlert alert);

		void onCancelClick(CommonAlert alert);
	}
}
