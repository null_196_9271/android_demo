package com.uzhu.project.service.models;

import java.io.Serializable;

/**
 * Created by shard on 16/12/30.
 */

public class Lottery implements Serializable {
	public Number index;    //奖品索引
	public String prize;    //奖品内容
	public Number chance;   //中奖概率
	public Number count;   //剩余次数
	public Number prizeId;

	@Override
	public String toString() {
		return "Lottery{" +
				"index=" + index +
				", prize='" + prize + '\'' +
				", chance=" + chance +
				", count=" + count +
				", prizeId=" + prizeId +
				'}';
	}
}
