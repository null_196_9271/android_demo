/*
 *
 *  Image.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Image implements Serializable {

    public String httpUrl;    //图片完整地址
    public String url;    //图片相对地址

    @Override
    public String toString (){
        return "Image{" +
                "httpUrl='" + httpUrl + '\'' +
                ", url='" + url + '\'' +
                '}';
    }


}
