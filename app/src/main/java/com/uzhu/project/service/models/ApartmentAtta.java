/*
 *
 *  ApartmentAtta.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class ApartmentAtta implements Serializable {

    public Number loanProjectAttaId;    //附件编号
    public String attaName;    //附件名称
    public String attaTypeName;    //附件类型名称
    public String attaType;    //附件类型 PROJECT-项目图片 APARTMENT-公寓图片 TENEMENT-租房合同图片 PROJECTCONTRACT-项目合约
    public String attaUrl;    //附件地址

    @Override
    public String toString (){
        return "ApartmentAtta{" +
                "loanProjectAttaId=" + loanProjectAttaId +
                ", attaName='" + attaName + '\'' +
                ", attaTypeName='" + attaTypeName + '\'' +
                ", attaType='" + attaType + '\'' +
                ", attaUrl='" + attaUrl + '\'' +
                '}';
    }


}
