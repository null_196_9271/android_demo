/*
 *
 *  Banner.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Banner implements Serializable {

    public String pageUrl;    //点击图片跳转的页面
    public String imgUrl;    //展示的图片

    @Override
    public String toString (){
        return "Banner{" +
                "pageUrl='" + pageUrl + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }


}
