/*
 *
 *  P2PServiceMediatorTest.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service;

import android.test.AndroidTestCase;

import com.mvvm.framework.service.RequestResult;
import com.mvvm.framework.service.ServiceResponse;
import com.mvvm.framework.util.ENVConfig;
import com.uzhu.project.service.models.AccountInfo;
import com.uzhu.project.service.models.Banner;
import com.uzhu.project.service.models.IntegralInfo;
import com.uzhu.project.service.models.InvestSuccessInfo;
import com.uzhu.project.service.models.LoanProject;
import com.uzhu.project.service.models.LoanProjectAccountInfo;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Pager;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.service.models.RaiseProject;
import com.uzhu.project.service.models.RaiseProjectAccountInfo;
import com.uzhu.project.service.models.SecurityInfo;
import com.uzhu.project.service.models.User;
import com.uzhu.project.service.models.Version;

import java.util.ArrayList;
import java.util.List;

public class AppServiceMediatorTest extends AndroidTestCase {


    private AppServiceMediatorAPI mediatorAPI;

    protected void setUp() throws Exception {
        super.setUp();
        ENVConfig.configurationEnvironment(mContext);
        List<String> list = new ArrayList<String>();
        list.add("ServerURL");
//        ENVConfig.setEnvironmentConfig(R.raw.environment_config, list);
        mediatorAPI = new AppServiceMediatorAPI();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    //登录
    public void testLogin() {
    String accountName = "";
    String password = "";
        ServiceResponse<RequestResult<User>> response = mediatorAPI.login(accountName, password);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //登出
    public void testLogout() {
        ServiceResponse<RequestResult<String>> response = mediatorAPI.logout();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //注册
    public void testDoRegister() {
    String smsCode = "";
    String accountName = "";
    String password = "";
    String inviter = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.doRegister(smsCode, accountName, password, inviter);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //实名认证
    public void testAuthenticateName() {
    String userIdCard = "";
    String userName = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.authenticateName(userIdCard, userName);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //用户详情
    public void testUserInfo() {
        ServiceResponse<RequestResult<User>> response = mediatorAPI.userInfo();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //检查帐号
    public void testValidateAccountName() {
    String accountName = "";
    String type = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.validateAccountName(accountName, type);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //更改密码第二步:验证帐号身份证
    public void testValidateIdCard() {
    String smsCode = "";
    String accountName = "";
    String userIdCard = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.validateIdCard(smsCode, accountName, userIdCard);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //重置密码
    public void testResetPassword() {
    String randomCode = "";
    String accountName = "";
    String password = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.resetPassword(randomCode, accountName, password);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //修改密码
    public void testUpdatePassword() {
    String theNewPwd = "";
    String theOldPwd = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.updatePassword(theNewPwd, theOldPwd);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //注册设备
    public void testDoClientRegister() {
    String deviceMfd = "";
    String deviceModelId = "";
    String lastCityCode = "";
    String lastOsVersion = "";
    String deviceInfo = "";
    String os = "";
    String lastAppVersion = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.doClientRegister(deviceMfd, deviceModelId, lastCityCode, lastOsVersion, deviceInfo, os, lastAppVersion);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //版本号是否需要更新
    public void testIsVersionUpdate() {
        ServiceResponse<RequestResult<Version>> response = mediatorAPI.isVersionUpdate();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //短信验证码
    public void testGetSMSCode() {
    String smsType = "";
    String mobile = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.getSMSCode(smsType, mobile);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

//    //上传图片
//    public void testUpload() {
//    String uploadfile = "";
//        ServiceResponse<RequestResult<Image>> response = mediatorAPI.upload(uploadfile);
//        System.out.println(response.getReturnCode());
//        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
//            System.out.println(response.getResponse().getResult());
//        }else {
//            System.out.println(response.getReturnDesc());
//        }
//    }

    //banner列表
    public void testBannerList() {
        ServiceResponse<RequestResult<List<Banner>>> response = mediatorAPI.bannerList();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //资讯列表
    public void testInformationList() {
    Page page = new Page();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.informationList(page);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //签到
    public void testSignIn() {
        ServiceResponse<RequestResult<String>> response = mediatorAPI.signIn();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //房租宝项目列表
    public void testLoanProjectList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.loanProjectList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //月月升项目列表
    public void testRaiseProjectList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.raiseProjectList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //房租宝项目详情
    public void testLoanProjectDetail() {
    Number loanProjectId = 0;
        ServiceResponse<RequestResult<LoanProject>> response = mediatorAPI.loanProjectDetail(loanProjectId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //项目投资记录列表
    public void testProjectInvestmentList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.projectInvestmentList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //房租宝项目投资信息
    public void testLoanProjectInvestInfo() {
    Number loanProjectId = 0;
        ServiceResponse<RequestResult<LoanProject>> response = mediatorAPI.loanProjectInvestInfo(loanProjectId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //月月升项目详情
    public void testRaiseProjectDetail() {
    Number raiseProjectId = 0;
        ServiceResponse<RequestResult<RaiseProject>> response = mediatorAPI.raiseProjectDetail(raiseProjectId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //月月升年化利率列表
//    public void testInterestRateList() {
//    Number raiseProjectId = 0;
//        ServiceResponse<RequestResult> response = mediatorAPI.interestRateList(raiseProjectId);
//        System.out.println(response.getReturnCode());
//        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
//            System.out.println(response.getResponse().getResult());
//        }else {
//            System.out.println(response.getReturnDesc());
//        }
//    }

    //投资成功信息
    public void testInvestSuccess() {
    String type = "";
    Number investmentId = 0;
        ServiceResponse<RequestResult<InvestSuccessInfo>> response = mediatorAPI.investSuccess(type, investmentId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //账号信息
    public void testAccountInfo() {
        ServiceResponse<RequestResult<AccountInfo>> response = mediatorAPI.accountInfo();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //安全信息
    public void testSecurityInfo() {
        ServiceResponse<RequestResult<SecurityInfo>> response = mediatorAPI.securityInfo();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //修改头像
    public void testModAvatar() {
    String avatarUrl = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.modAvatar(avatarUrl);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //设置支付密码
    public void testSetPayPassword() {
        ServiceResponse<RequestResult<String>> response = mediatorAPI.setPayPassword();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //修改支付密码
    public void testModPayPassword() {
        ServiceResponse<RequestResult<String>> response = mediatorAPI.modPayPassword();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //重置支付密码
    public void testResetPayPassword() {
        ServiceResponse<RequestResult<String>> response = mediatorAPI.resetPayPassword();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //站内信列表
    public void testUserMessageList() {
    Page page = new Page();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.userMessageList(page);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //账户交易记录列表
    public void testAccountTradeList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.accountTradeList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //获取优惠券列表
    public void testUserFinancialCouponList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.userFinancialCouponList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //积分信息
    public void testUserIntegralInfo() {
        ServiceResponse<RequestResult<IntegralInfo>> response = mediatorAPI.userIntegralInfo();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //积分明细列表
    public void testUserIntegralList() {
    Page page = new Page();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.userIntegralList(page);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //积分商品列表
    public void testUserIntegralProductList() {
    Page page = new Page();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.userIntegralProductList(page);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //积分商品兑换
    public void testUserIntegralExchange() {
    Number financialCouponId = 0;
        ServiceResponse<RequestResult<String>> response = mediatorAPI.userIntegralExchange(financialCouponId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //房租宝资产信息
    public void testLoanProjectAccountInfo() {
        ServiceResponse<RequestResult<LoanProjectAccountInfo>> response = mediatorAPI.loanProjectAccountInfo();
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //房租宝投资记录列表
    public void testUserInvestmentList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.userInvestmentList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //支付投资记录
    public void testPayUserInvestment() {
    String type = "";
    Number investmentId = 0;
        ServiceResponse<RequestResult<String>> response = mediatorAPI.payUserInvestment(type, investmentId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //房租宝回款计划表
    public void testInvestmentReturnedList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.investmentReturnedList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //月月升资产信息
    public void testRaiseProjectAccountInfo() {
    Number raiseProjectId = 0;
        ServiceResponse<RequestResult<RaiseProjectAccountInfo>> response = mediatorAPI.raiseProjectAccountInfo(raiseProjectId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //月月升用户投资记录列表
    public void testUserRaiseInvestmentList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.userRaiseInvestmentList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //月月升已匹配资产列表
    public void testRaiseProjectUserInvestmentList() {
    Page page = new Page();
    Params params = new Params();
        ServiceResponse<RequestResult<Pager>> response = mediatorAPI.raiseProjectUserInvestmentList(page, params);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //申请转出月月升投资
    public void testRollOutUserRaiseInvestment() {
    Number userRaiseInvestmentId = 0;
        ServiceResponse<RequestResult<String>> response = mediatorAPI.rollOutUserRaiseInvestment(userRaiseInvestmentId);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //充值
    public void testRecharge() {
    String amount = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.recharge(amount);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }

    //提现
    public void testWithdraw() {
    String amount = "";
        ServiceResponse<RequestResult<String>> response = mediatorAPI.withdraw(amount);
        System.out.println(response.getReturnCode());
        if (response.getReturnCode() == ServiceResponse.Service_Return_Success) {
            System.out.println(response.getResponse().getResult());
        }else {
            System.out.println(response.getReturnDesc());
        }
    }


}
