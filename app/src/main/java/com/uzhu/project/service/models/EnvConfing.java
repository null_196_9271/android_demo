package com.uzhu.project.service.models;

import java.io.Serializable;

public class EnvConfing implements Serializable {
    public String configTitle;
    public String configValue;

    public String getConfigTitle() {
        return configTitle;
    }

    public void setConfigTitle(String configTitle) {
        this.configTitle = configTitle;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }
}