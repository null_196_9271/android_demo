package com.uzhu.project.service;/*
 *
 *  AppServiceMediatorAPI.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mvvm.framework.netWork.NetworkResponse;
import com.mvvm.framework.netWork.RequestHandler;
import com.mvvm.framework.service.RequestResult;
import com.mvvm.framework.service.ServiceResponse;
import com.mvvm.framework.util.JsonHandler;
import com.uzhu.project.service.models.AccountInfo;
import com.uzhu.project.service.models.Banner;
import com.uzhu.project.service.models.Image;
import com.uzhu.project.service.models.IntegralInfo;
import com.uzhu.project.service.models.InvestSuccessInfo;
import com.uzhu.project.service.models.LoanProject;
import com.uzhu.project.service.models.LoanProjectAccountInfo;
import com.uzhu.project.service.models.Lottery;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Pager;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.service.models.RaiseProject;
import com.uzhu.project.service.models.RaiseProjectAccountInfo;
import com.uzhu.project.service.models.SecurityInfo;
import com.uzhu.project.service.models.User;
import com.uzhu.project.service.models.Version;

import java.io.File;
import java.util.HashMap;
import java.util.List;


public class AppServiceMediatorAPI {


    public static String serverAddress = "";
    public static String securityAddress = "";
    public static String h5Address = "";
    public static String imageAddress = "";
    public static String messageCollectorAddress = "";

    public static AppRequestDataType requestDataType;

    //登录
    public ServiceResponse<RequestResult<User>> login(String accountName, String password) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<User>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOGIN);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("accountName", accountName);
        jsonObj.addProperty("password", password);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOGIN, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<User>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //登出
    public ServiceResponse<RequestResult<String>> logout() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOGOUT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOGOUT, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //注册
    public ServiceResponse<RequestResult<String>> doRegister(String smsCode, String accountName, String password, String inviter) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.DO_REGISTER);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("smsCode", smsCode);
        jsonObj.addProperty("accountName", accountName);
        jsonObj.addProperty("password", password);
        if (!TextUtils.isEmpty(inviter)) {
            jsonObj.addProperty("inviter", inviter);
        }
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.DO_REGISTER, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //实名认证
    public ServiceResponse<RequestResult<String>> authenticateName(String userIdCard, String userName) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.AUTHENTICATE_NAME);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("userIdCard", userIdCard);
        jsonObj.addProperty("userName", userName);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.AUTHENTICATE_NAME, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //用户详情
    public ServiceResponse<RequestResult<User>> userInfo() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<User>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<User>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //检查帐号
    public ServiceResponse<RequestResult<String>> validateAccountName(String accountName, String type) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.VALIDATE_ACCOUNT_NAME);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("accountName", accountName);
        jsonObj.addProperty("type", type);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.VALIDATE_ACCOUNT_NAME, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //更改密码第二步:验证帐号身份证
    public ServiceResponse<RequestResult<String>> validateIdCard(String smsCode, String accountName, String userIdCard) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.VALIDATE_ID_CARD);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("smsCode", smsCode);
        jsonObj.addProperty("accountName", accountName);
        if (!TextUtils.isEmpty(userIdCard)) {
            jsonObj.addProperty("userIdCard", userIdCard);
        }
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.VALIDATE_ID_CARD, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //重置密码
    public ServiceResponse<RequestResult<String>> resetPassword(String randomCode, String accountName, String password) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RESET_PASSWORD);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("randomCode", randomCode);
        jsonObj.addProperty("accountName", accountName);
        jsonObj.addProperty("password", password);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RESET_PASSWORD, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //修改密码
    public ServiceResponse<RequestResult<String>> updatePassword(String theNewPwd, String theOldPwd) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.UPDATE_PASSWORD);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("theNewPwd", theNewPwd);
        jsonObj.addProperty("theOldPwd", theOldPwd);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.UPDATE_PASSWORD, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //注册设备
    public ServiceResponse<RequestResult<String>> doClientRegister(String deviceMfd, String deviceModelId, String lastCityCode, String lastOsVersion, String deviceInfo, String os, String lastAppVersion) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.DO_CLIENT_REGISTER);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("deviceMfd", deviceMfd);
        jsonObj.addProperty("deviceModelId", deviceModelId);
        jsonObj.addProperty("lastCityCode", lastCityCode);
        jsonObj.addProperty("lastOsVersion", lastOsVersion);
        jsonObj.addProperty("deviceInfo", deviceInfo);
        jsonObj.addProperty("os", os);
        jsonObj.addProperty("lastAppVersion", lastAppVersion);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.DO_CLIENT_REGISTER, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //版本号是否需要更新
    public ServiceResponse<RequestResult<Version>> isVersionUpdate() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Version>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.IS_VERSION_UPDATE);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.IS_VERSION_UPDATE, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Version>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //短信验证码
    public ServiceResponse<RequestResult<String>> getSMSCode(String smsType, String mobile) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.GET_S_M_S_CODE);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("smsType", smsType);
        jsonObj.addProperty("mobile", mobile);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.GET_S_M_S_CODE, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    // 上传图片
    public ServiceResponse<RequestResult<Image>> upload(File uploadfile) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Image>> response = new ServiceResponse<RequestResult<Image>>();
        response.setOperateCode(requestDataType.UPLOAD);

        HashMap<String, String> params = new HashMap<String, String>();
        // params.put("uploadfile", uploadfile);
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequestByForm(
                    requestDataType.UPLOAD, params, uploadfile, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Image>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //banner列表
    public ServiceResponse<RequestResult<List<Banner>>> bannerList() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<List<Banner>>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.BANNER_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.BANNER_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<List<Banner>>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //资讯列表
    public ServiceResponse<RequestResult<Pager>> informationList(Page page) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.INFORMATION_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.INFORMATION_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //签到
    public ServiceResponse<RequestResult<String>> signIn() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.SIGN_IN);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.SIGN_IN, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //房租宝项目列表
    public ServiceResponse<RequestResult<Pager>> loanProjectList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOAN_PROJECT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOAN_PROJECT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升项目列表
    public ServiceResponse<RequestResult<Pager>> raiseProjectList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RAISE_PROJECT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RAISE_PROJECT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //房租宝项目详情
    public ServiceResponse<RequestResult<LoanProject>> loanProjectDetail(Number loanProjectId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<LoanProject>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOAN_PROJECT_DETAIL);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("loanProjectId", loanProjectId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOAN_PROJECT_DETAIL, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<LoanProject>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //项目投资记录列表
    public ServiceResponse<RequestResult<Pager>> projectInvestmentList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.PROJECT_INVESTMENT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.PROJECT_INVESTMENT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //房租宝项目投资信息
    public ServiceResponse<RequestResult<LoanProject>> loanProjectInvestInfo(Number loanProjectId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<LoanProject>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOAN_PROJECT_INVEST_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("loanProjectId", loanProjectId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOAN_PROJECT_INVEST_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<LoanProject>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升项目详情
    public ServiceResponse<RequestResult<RaiseProject>> raiseProjectDetail(Number raiseProjectId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<RaiseProject>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RAISE_PROJECT_DETAIL);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("raiseProjectId", raiseProjectId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RAISE_PROJECT_DETAIL, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<RaiseProject>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升年化利率列表
    public ServiceResponse<RequestResult<List>> interestRateList(Number raiseProjectId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<List>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.INTEREST_RATE_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("raiseProjectId", raiseProjectId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.INTEREST_RATE_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<List>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //投资项目
    public ServiceResponse<RequestResult<String>> investProject(String investType, String amount, Number userFinancialCouponId, Number projectId, String type) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.INVEST_PROJECT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        if (investType != null) {
            jsonObj.addProperty("investType", investType);
        }
        jsonObj.addProperty("amount", amount);
        if (userFinancialCouponId != null) {
            jsonObj.addProperty("userFinancialCouponId", userFinancialCouponId);
        }
        jsonObj.addProperty("projectId", projectId);
        jsonObj.addProperty("type", type);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.INVEST_PROJECT, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //投资成功信息
    public ServiceResponse<RequestResult<InvestSuccessInfo>> investSuccess(String type, Number investmentId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<InvestSuccessInfo>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.INVEST_SUCCESS);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("type", type);
        jsonObj.addProperty("investmentId", investmentId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.INVEST_SUCCESS, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<InvestSuccessInfo>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //账号信息
    public ServiceResponse<RequestResult<AccountInfo>> accountInfo() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<AccountInfo>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.ACCOUNT_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.ACCOUNT_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<AccountInfo>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //安全信息
    public ServiceResponse<RequestResult<SecurityInfo>> securityInfo() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<SecurityInfo>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.SECURITY_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.SECURITY_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<SecurityInfo>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //修改头像
    public ServiceResponse<RequestResult<String>> modAvatar(String avatarUrl) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.MOD_AVATAR);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("avatarUrl", avatarUrl);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.MOD_AVATAR, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //设置支付密码
    public ServiceResponse<RequestResult<String>> setPayPassword() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.SET_PAY_PASSWORD);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.SET_PAY_PASSWORD, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //修改支付密码
    public ServiceResponse<RequestResult<String>> modPayPassword() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.MOD_PAY_PASSWORD);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.MOD_PAY_PASSWORD, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //重置支付密码
    public ServiceResponse<RequestResult<String>> resetPayPassword() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RESET_PAY_PASSWORD);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RESET_PAY_PASSWORD, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //站内信列表
    public ServiceResponse<RequestResult<Pager>> userMessageList(Page page) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_MESSAGE_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_MESSAGE_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //账户交易记录列表
    public ServiceResponse<RequestResult<Pager>> accountTradeList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.ACCOUNT_TRADE_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.ACCOUNT_TRADE_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //获取优惠券列表
    public ServiceResponse<RequestResult<Pager>> userFinancialCouponList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_FINANCIAL_COUPON_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_FINANCIAL_COUPON_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //积分信息
    public ServiceResponse<RequestResult<IntegralInfo>> userIntegralInfo() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<IntegralInfo>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_INTEGRAL_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_INTEGRAL_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<IntegralInfo>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //积分明细列表
    public ServiceResponse<RequestResult<Pager>> userIntegralList(Page page) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_INTEGRAL_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_INTEGRAL_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //积分商品列表
    public ServiceResponse<RequestResult<Pager>> userIntegralProductList(Page page) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_INTEGRAL_PRODUCT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_INTEGRAL_PRODUCT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //积分商品兑换
    public ServiceResponse<RequestResult<String>> userIntegralExchange(Number financialCouponId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_INTEGRAL_EXCHANGE);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("financialCouponId", financialCouponId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_INTEGRAL_EXCHANGE, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //房租宝资产信息
    public ServiceResponse<RequestResult<LoanProjectAccountInfo>> loanProjectAccountInfo() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<LoanProjectAccountInfo>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOAN_PROJECT_ACCOUNT_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOAN_PROJECT_ACCOUNT_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<LoanProjectAccountInfo>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //房租宝投资记录列表
    public ServiceResponse<RequestResult<Pager>> userInvestmentList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_INVESTMENT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_INVESTMENT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //支付投资记录
    public ServiceResponse<RequestResult<String>> payUserInvestment(String type, Number investmentId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.PAY_USER_INVESTMENT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("type", type);
        jsonObj.addProperty("investmentId", investmentId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.PAY_USER_INVESTMENT, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //房租宝回款计划表
    public ServiceResponse<RequestResult<Pager>> investmentReturnedList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.INVESTMENT_RETURNED_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.INVESTMENT_RETURNED_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升资产信息
    public ServiceResponse<RequestResult<RaiseProjectAccountInfo>> raiseProjectAccountInfo(Number raiseProjectId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<RaiseProjectAccountInfo>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RAISE_PROJECT_ACCOUNT_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("raiseProjectId", raiseProjectId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RAISE_PROJECT_ACCOUNT_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<RaiseProjectAccountInfo>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升用户投资记录列表
    public ServiceResponse<RequestResult<Pager>> userRaiseInvestmentList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.USER_RAISE_INVESTMENT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.USER_RAISE_INVESTMENT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升已匹配资产列表
    public ServiceResponse<RequestResult<Pager>> raiseProjectUserInvestmentList(Page page, Params params) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Pager>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RAISE_PROJECT_USER_INVESTMENT_LIST);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.add("page", new Gson().toJsonTree(page));
        jsonObj.add("params", new Gson().toJsonTree(params));
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RAISE_PROJECT_USER_INVESTMENT_LIST, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Pager>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //申请转出月月升投资
    public ServiceResponse<RequestResult<String>> rollOutUserRaiseInvestment(Number userRaiseInvestmentId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.ROLL_OUT_USER_RAISE_INVESTMENT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("userRaiseInvestmentId", userRaiseInvestmentId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.ROLL_OUT_USER_RAISE_INVESTMENT, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //充值
    public ServiceResponse<RequestResult<String>> recharge(String amount) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RECHARGE);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("amount", amount);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RECHARGE, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //提现
    public ServiceResponse<RequestResult<String>> withdraw(String amount) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.WITHDRAW);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("amount", amount);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.WITHDRAW, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //月月升项目投资信息
    public ServiceResponse<RequestResult<RaiseProject>> raiseProjectInvestInfo(Number raiseProjectId) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<RaiseProject>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.RAISE_PROJECT_INVEST_INFO);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("raiseProjectId", raiseProjectId);
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.RAISE_PROJECT_INVEST_INFO, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<RaiseProject>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //抽奖
    public ServiceResponse<RequestResult<Lottery>> lottery() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Lottery>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOTTERY);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOTTERY, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Lottery>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    //获取我的抽奖次数
    public ServiceResponse<RequestResult<Number>> lotteryCount() {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Number>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.LOTTERY_COUNT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.LOTTERY_COUNT, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Number>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    public ServiceResponse<RequestResult<Number>> getMessagecount(String searchTime) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<Number>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.MESSAGE_COUNT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        if (!TextUtils.isEmpty(searchTime)) {
            jsonObj.addProperty("searchTime", searchTime);
        }
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.MESSAGE_COUNT, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<Number>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }

    public ServiceResponse<RequestResult<String>> splashImage(int displayWidth, int displayHeight) {
        // 默认初始化为失败信息
        ServiceResponse<RequestResult<String>> response = new ServiceResponse();
        response.setOperateCode(requestDataType.MESSAGE_COUNT);

        // 参数拼装并转换为JsonString
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("width", displayWidth + "");
        jsonObj.addProperty("height", displayHeight + "");
        String requestStr = jsonObj.toString();
        // 发起网络请求
        NetworkResponse netResponse = new NetworkResponse();
        try {
            netResponse = RequestHandler.doRequest(requestDataType.SPLASH_IMAGE, requestStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (netResponse.getStatusCode() == ServiceResponse.Service_Return_Success) {
            JsonHandler.jsonToObject(netResponse.getData(), new TypeToken<RequestResult<String>>() {
            }.getType(), response);
        } else {
            response.setReturnCode(netResponse.getStatusCode());
            JsonHandler.jsonToErrorString(netResponse, response);
        }
        return response;
    }
}
