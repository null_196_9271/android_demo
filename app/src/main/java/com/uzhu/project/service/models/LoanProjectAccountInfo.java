/*
 *
 *  LoanProjectAccountInfo.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class LoanProjectAccountInfo implements Serializable {

    public String totalInterest;    //总利息
    public String loanProjectAmount;    //房租宝总资产
    public Number investNum;    //投资数量

    @Override
    public String toString (){
        return "LoanProjectAccountInfo{" +
                "totalInterest='" + totalInterest + '\'' +
                ", loanProjectAmount='" + loanProjectAmount + '\'' +
                ", investNum=" + investNum +
                '}';
    }


}
