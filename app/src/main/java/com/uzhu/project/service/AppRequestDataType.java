package com.uzhu.project.service;/*
 *
 *  AppRequestDataType.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */


public class AppRequestDataType {

    //单例实现
    private static AppRequestDataType requestDataType;


    public static AppRequestDataType sharedInstance() {
        if (requestDataType == null) {
            synchronized (AppRequestDataType.class) {
                if (requestDataType == null) {
                    requestDataType = new AppRequestDataType();
                }
            }
        }
        return requestDataType;
    }

    public String LOGIN = AppServiceMediatorAPI.serverAddress + "/service/user/login/0"; //登录
    public String LOGOUT = AppServiceMediatorAPI.serverAddress + "/service/user/logout/0"; //登出
    public String DO_REGISTER = AppServiceMediatorAPI.serverAddress + "/service/user/doRegister/0"; //注册
    public String AUTHENTICATE_NAME = AppServiceMediatorAPI.serverAddress + "/service/user/authenticateName/0"; //实名认证
    public String USER_INFO = AppServiceMediatorAPI.serverAddress + "/service/user/userInfo/0"; //用户详情
    public String VALIDATE_ACCOUNT_NAME = AppServiceMediatorAPI.serverAddress + "/service/user/validateAccountName/0"; //检查帐号
    public String VALIDATE_ID_CARD = AppServiceMediatorAPI.serverAddress + "/service/user/validateIdCard/0"; //更改密码第二步:验证帐号身份证
    public String RESET_PASSWORD = AppServiceMediatorAPI.serverAddress + "/service/user/resetPassword/0"; //重置密码
    public String UPDATE_PASSWORD = AppServiceMediatorAPI.serverAddress + "/service/user/updatePassword/0"; //修改密码
    public String DO_CLIENT_REGISTER = AppServiceMediatorAPI.serverAddress + "/service/client/doClientRegister/0"; //注册设备
    public String IS_VERSION_UPDATE = AppServiceMediatorAPI.serverAddress + "/service/client/isVersionUpdate"; //版本号是否需要更新
    public String GET_S_M_S_CODE = AppServiceMediatorAPI.serverAddress + "/service/common/getSMSCode/0"; //短信验证码
    public String UPLOAD = AppServiceMediatorAPI.serverAddress + "/service/common/upload/0"; //上传图片
    public String BANNER_LIST = AppServiceMediatorAPI.serverAddress + "/service/home/bannerList/0"; //banner列表
    public String INFORMATION_LIST = AppServiceMediatorAPI.serverAddress + "/service/home/informationList/0"; //资讯列表
    public String SIGN_IN = AppServiceMediatorAPI.serverAddress + "/service/home/signIn/0"; //签到
    public String LOAN_PROJECT_LIST = AppServiceMediatorAPI.serverAddress + "/service/finance/loanProjectList/0"; //房租宝项目列表
    public String RAISE_PROJECT_LIST = AppServiceMediatorAPI.serverAddress + "/service/finance/raiseProjectList/0"; //月月升项目列表
    public String LOAN_PROJECT_DETAIL = AppServiceMediatorAPI.serverAddress + "/service/finance/loanProjectDetail/0"; //房租宝项目详情
    public String PROJECT_INVESTMENT_LIST = AppServiceMediatorAPI.serverAddress + "/service/finance/projectInvestmentList/0"; //项目投资记录列表
    public String LOAN_PROJECT_INVEST_INFO = AppServiceMediatorAPI.serverAddress + "/service/finance/loanProjectInvestInfo/0"; //房租宝项目投资信息
    public String RAISE_PROJECT_DETAIL = AppServiceMediatorAPI.serverAddress + "/service/finance/raiseProjectDetail/0"; //月月升项目详情
    public String INTEREST_RATE_LIST = AppServiceMediatorAPI.serverAddress + "/service/finance/interestRateList/0"; //月月升年化利率列表
    public String INVEST_PROJECT = AppServiceMediatorAPI.serverAddress + "/service/finance/investProject/0"; //投资项目
    public String INVEST_SUCCESS = AppServiceMediatorAPI.serverAddress + "/service/finance/investSuccess/0"; //投资成功信息
    public String ACCOUNT_INFO = AppServiceMediatorAPI.serverAddress + "/service/userCenter/accountInfo/0"; //账号信息
    public String SECURITY_INFO = AppServiceMediatorAPI.serverAddress + "/service/userCenter/securityInfo/0"; //安全信息
    public String MOD_AVATAR = AppServiceMediatorAPI.serverAddress + "/service/userCenter/modAvatar/0"; //修改头像
    public String SET_PAY_PASSWORD = AppServiceMediatorAPI.serverAddress + "/service/userCenter/setPayPassword/0"; //设置支付密码
    public String MOD_PAY_PASSWORD = AppServiceMediatorAPI.serverAddress + "/service/userCenter/modPayPassword/0"; //修改支付密码
    public String RESET_PAY_PASSWORD = AppServiceMediatorAPI.serverAddress + "/service/userCenter/resetPayPassword/0"; //重置支付密码
    public String USER_MESSAGE_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userMessageList/0"; //站内信列表
    public String ACCOUNT_TRADE_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/accountTradeList/0"; //账户交易记录列表
    public String USER_FINANCIAL_COUPON_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userFinancialCouponList/0"; //获取优惠券列表
    public String USER_INTEGRAL_INFO = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userIntegralInfo/0"; //积分信息
    public String USER_INTEGRAL_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userIntegralList/0"; //积分明细列表
    public String USER_INTEGRAL_PRODUCT_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userIntegralProductList/0"; //积分商品列表
    public String USER_INTEGRAL_EXCHANGE = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userIntegralExchange/0"; //积分商品兑换
    public String LOAN_PROJECT_ACCOUNT_INFO = AppServiceMediatorAPI.serverAddress + "/service/userCenter/loanProjectAccountInfo/0"; //房租宝资产信息
    public String USER_INVESTMENT_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userInvestmentList/0"; //房租宝投资记录列表
    public String PAY_USER_INVESTMENT = AppServiceMediatorAPI.serverAddress + "/service/userCenter/payUserInvestment/0"; //支付投资记录
    public String INVESTMENT_RETURNED_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/investmentReturnedList/0"; //房租宝回款计划表
    public String RAISE_PROJECT_ACCOUNT_INFO = AppServiceMediatorAPI.serverAddress + "/service/userCenter/raiseProjectAccountInfo/0"; //月月升资产信息
    public String USER_RAISE_INVESTMENT_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userRaiseInvestmentList/0"; //月月升用户投资记录列表
    public String RAISE_PROJECT_USER_INVESTMENT_LIST = AppServiceMediatorAPI.serverAddress + "/service/userCenter/raiseProjectUserInvestmentList/0"; //月月升已匹配资产列表
    public String ROLL_OUT_USER_RAISE_INVESTMENT = AppServiceMediatorAPI.serverAddress + "/service/userCenter/rollOutUserRaiseInvestment/0"; //申请转出月月升投资
    public String RECHARGE = AppServiceMediatorAPI.serverAddress + "/service/userCenter/recharge/0"; //充值
    public String WITHDRAW = AppServiceMediatorAPI.serverAddress + "/service/userCenter/withdraw/0"; //提现
    public String RAISE_PROJECT_INVEST_INFO = AppServiceMediatorAPI.serverAddress + "/service/finance/raiseProjectInvestInfo/0"; //月月升项目投资信息
    public String INVITE_SHARE_PAGE = AppServiceMediatorAPI.h5Address + "/inviteFriendsActivity?accountName="; //邀请好友页面
    public String INVITE_PAGE = AppServiceMediatorAPI.serverAddress + "/activity/activity_invite_friends.html"; //邀请好友页面
    public String LOTTERY_PAGE = AppServiceMediatorAPI.serverAddress + "/activityLottery"; //抽奖页面
    public String LOTTERY = AppServiceMediatorAPI.serverAddress + "/service/activity/lottery/0"; //抽奖
    public String LOTTERY_COUNT = AppServiceMediatorAPI.serverAddress + "/service/activity/lotteryCount/0"; //获取我的抽奖次数
    public String MESSAGE_COUNT = AppServiceMediatorAPI.serverAddress + "/service/userCenter/userMessageCount/0"; //获取我的抽奖次数
    public String SPLASH_IMAGE = AppServiceMediatorAPI.serverAddress + "/service/home/splashImage/0";
    public String ENSURE = AppServiceMediatorAPI.serverAddress + "/ensure/ensure.html";
    public String WECHAT_SERVICE = AppServiceMediatorAPI.serverAddress + "/activity/share_wechat_service.html";
}
