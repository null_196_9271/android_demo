/*
 *
 *  User.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class User implements Serializable {

    public String userIDCard;    //身份证号
    public String accountName;    //账号
    public String userName;    //真实姓名
    public String userToken;    //登录令牌
    public String displayName;    //昵称
    public String avatarUrl;    //用户头像

    @Override
    public String toString (){
        return "User{" +
                "userIDCard='" + userIDCard + '\'' +
                ", accountName='" + accountName + '\'' +
                ", userName='" + userName + '\'' +
                ", userToken='" + userToken + '\'' +
                ", displayName='" + displayName + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                '}';
    }


}
