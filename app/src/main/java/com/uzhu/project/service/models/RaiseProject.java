/*
 *
 *  RaiseProject.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class RaiseProject implements Serializable {

    public String periodName;    //投资周期名称
    public String transferDesc;    //转出规则
    public ArrayList<String> perMillionRevenueList;    //万份收益额列表
    public String interestDesc;    //计息规则
    public String perAmount;    //每份金额
    public String state;    //项目状态 INRAISE-募集中 FROZEN-冻结 DBL-已删除
    public String investProcess;    //投资进度
    public String startAmount;    //起投金额
    public String startInterestRate;    //初始年化利率
    public String raiseProjectName;    //项目名称
    public String investDesc;    //投资方式
    public String endInterestRate;    //最终年化利率
    public String balance;    //可投金额
    public String userInvestmentAmount;    //用户的投资总额
    public String stateName;    //项目状态名称
    public String profitWay;    //收益方式
    public String transferTimeDesc;    //转出到账时间
    public String period;    //投资周期 DAY-天 WEEK-周 MONTH-月 QUARTER-季度 HALFYEAR-半年 YEAR-年
    public String limitAmount;    //限购金额
    public ArrayList<String> annualizedRateList;    //年化收益率列表
    public String projectType;    //项目类型 NEWER-新手标 MONTH-月月升 QUARTER-季季升
    public Number raiseProjectId;    //项目编号
    public Number periodNum;
    public Number periodCount;
    public String ensureUrl;
    public String serviceProtocolUrl;
    public String moreUrl;
    public String transferLimit; //转出金额上限


    @Override
    public String toString() {
        return "RaiseProject{" +
                "annualizedRateList=" + annualizedRateList +
                ", periodName='" + periodName + '\'' +
                ", transferDesc='" + transferDesc + '\'' +
                ", perMillionRevenueList=" + perMillionRevenueList +
                ", interestDesc='" + interestDesc + '\'' +
                ", perAmount='" + perAmount + '\'' +
                ", state='" + state + '\'' +
                ", investProcess='" + investProcess + '\'' +
                ", startAmount='" + startAmount + '\'' +
                ", startInterestRate='" + startInterestRate + '\'' +
                ", raiseProjectName='" + raiseProjectName + '\'' +
                ", investDesc='" + investDesc + '\'' +
                ", endInterestRate='" + endInterestRate + '\'' +
                ", balance='" + balance + '\'' +
                ", userInvestmentAmount='" + userInvestmentAmount + '\'' +
                ", stateName='" + stateName + '\'' +
                ", profitWay='" + profitWay + '\'' +
                ", transferTimeDesc='" + transferTimeDesc + '\'' +
                ", period='" + period + '\'' +
                ", limitAmount='" + limitAmount + '\'' +
                ", projectType='" + projectType + '\'' +
                ", raiseProjectId=" + raiseProjectId +
                ", periodNum=" + periodNum +
                ", periodCount=" + periodCount +
                ", ensureUrl='" + ensureUrl + '\'' +
                ", serviceProtocolUrl='" + serviceProtocolUrl + '\'' +
                ", moreUrl='" + moreUrl + '\'' +
                ", transferLimit='" + transferLimit + '\'' +
                '}';
    }


}
