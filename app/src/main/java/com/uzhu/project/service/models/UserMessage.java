/*
 *
 *  UserMessage.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class UserMessage implements Serializable {

    public String sendTime;    //发送时间
    public Number userMessageId;    //消息编号
    public String title;    //消息标题
    public String content;    //消息内容

    @Override
    public String toString (){
        return "UserMessage{" +
                "sendTime='" + sendTime + '\'' +
                ", userMessageId=" + userMessageId +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }


}
