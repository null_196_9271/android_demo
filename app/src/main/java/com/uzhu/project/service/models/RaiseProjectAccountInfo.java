/*
 *
 *  RaiseProjectAccountInfo.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class RaiseProjectAccountInfo implements Serializable {

    public String totalInterest;    //累计收益(包含已结束的)
    public String raiseProjectAmount;    //总资产
    public String currentInterest;    //当前收益

    @Override
    public String toString (){
        return "RaiseProjectAccountInfo{" +
                "totalInterest='" + totalInterest + '\'' +
                ", raiseProjectAmount='" + raiseProjectAmount + '\'' +
                ", currentInterest='" + currentInterest + '\'' +
                '}';
    }


}
