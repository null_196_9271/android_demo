/*
 *
 *  FinancialCoupon.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class FinancialCoupon implements Serializable {

    public String financialCouponBody;    //优惠券内容
    public String state;    //项目状态 CREATE-创建中 APPLY-申请中 INRAISE-募集中 RAISED-募集完成 SETUP-项目成立 REPAY-还款中 COMPLETE-已完成 CANCEL-取消 DBL-已删除
    public String financialCouponType;    //优惠券类型 INTEREST:加息券 CASH:代金券
    public Number financialCouponId;    //优惠券编号
    public String stateName;    //项目状态名称
    public Number validDay;    //优惠券有效天数
    public String financialCouponName;    //优惠券类型名字

    @Override
    public String toString (){
        return "FinancialCoupon{" +
                "financialCouponBody='" + financialCouponBody + '\'' +
                ", state='" + state + '\'' +
                ", financialCouponType='" + financialCouponType + '\'' +
                ", financialCouponId=" + financialCouponId +
                ", stateName='" + stateName + '\'' +
                ", validDay=" + validDay +
                ", financialCouponName='" + financialCouponName + '\'' +
                '}';
    }


}
