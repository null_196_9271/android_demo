/*
 *
 *  LoanApartment.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class LoanApartment implements Serializable {

    public String apartmentCompany;    //运营公司名称
    public String apartmentDesc;    //公寓描述
    public Number loanApartmentId;    //贷款公寓编号
    public String apartmentAddress;    //公寓地址
    public Number apartmentId;    //公寓编号
    public String apartmentName;    //公寓名称
    public Number roomNum;    //房间总数

    @Override
    public String toString (){
        return "LoanApartment{" +
                "apartmentCompany='" + apartmentCompany + '\'' +
                ", apartmentDesc='" + apartmentDesc + '\'' +
                ", loanApartmentId=" + loanApartmentId +
                ", apartmentAddress='" + apartmentAddress + '\'' +
                ", apartmentId=" + apartmentId +
                ", apartmentName='" + apartmentName + '\'' +
                ", roomNum=" + roomNum +
                '}';
    }


}
