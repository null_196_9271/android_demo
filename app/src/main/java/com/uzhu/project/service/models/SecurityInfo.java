/*
 *
 *  SecurityInfo.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class SecurityInfo implements Serializable {

    public String userIdCard;    //身份证号
    public String userName;    //真实姓名
    public String mobile;    //手机号
    public String hasPayPassword;    //是否设置过支付密码 0-否 1-是

    @Override
    public String toString (){
        return "SecurityInfo{" +
                "userIdCard='" + userIdCard + '\'' +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", hasPayPassword='" + hasPayPassword + '\'' +
                '}';
    }


}
