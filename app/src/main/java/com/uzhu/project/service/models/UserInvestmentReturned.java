/*
 *
 *  UserInvestmentReturned.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class UserInvestmentReturned implements Serializable {

    public String dueDate;    //实际回款日期
    public Number userInvestmentReturnedId;    //用户回款计划编号
    public String stateName;    //回款计划状态名称
    public String generateTime;    //投资时间
    public String state;    //回款计划状态 INVESTMENT-已投资 REPAYMENT-还款中 COMPLETE-已完成 CANCEL-取消 DBL-已删除
    public String expectedDueDate;    //预计回款日期
    public String returnedType;    //回款类型 PRINCIPAL-本金 INTEREST-利息 RAISEINTEREST-加息券利息
    public String dueAmount;    //回款金额
    public String returnedTypeName;    //回款类型名称
    public String loanProjectName;    //项目名称

    @Override
    public String toString (){
        return "UserInvestmentReturned{" +
                "dueDate='" + dueDate + '\'' +
                ", userInvestmentReturnedId=" + userInvestmentReturnedId +
                ", stateName='" + stateName + '\'' +
                ", generateTime='" + generateTime + '\'' +
                ", state='" + state + '\'' +
                ", expectedDueDate='" + expectedDueDate + '\'' +
                ", returnedType='" + returnedType + '\'' +
                ", dueAmount='" + dueAmount + '\'' +
                ", returnedTypeName='" + returnedTypeName + '\'' +
                ", loanProjectName='" + loanProjectName + '\'' +
                '}';
    }


}
