/*
 *
 *  UserInvestment.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class UserInvestment implements Serializable {

    public String dueDate;    //到期日期(没有到期日期返回预计的到期日期)
    public String accountName;    //投资用户账号
    public Number interestDay;    //计息日
    public String interestRate;    //利率
    public Number userInvestmentId;    //用户投资编号
    public String generateTime;    //投资时间
    public String generateDate;    //投资时间
    public String state;    //投资记录状态 UNPAY-未付款 INVESTMENT-已投资 REPAYMENT-还款中 COMPLETE-已完成 REFUND-退款 CLOSE-取消 DBL-已删除
    public String userInvestmentNo;    //用户投资号
    public String loanProjectName;    //项目名称
    public String investmentAmount;    //项目总金额
    public String contractUrl;    //合同链接地址
    public String userInvestmentAmount;    //用户投资金额
    public String stateName;    //投资记录状态名称
    public String borrower;    //借款人
    public Number loanProjectId;    //项目编号
    public String investUserName;    //项目编号

    @Override
    public String toString() {
        return "UserInvestment{" +
                "accountName='" + accountName + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", interestDay=" + interestDay +
                ", interestRate='" + interestRate + '\'' +
                ", userInvestmentId=" + userInvestmentId +
                ", generateTime='" + generateTime + '\'' +
                ", generateDate='" + generateDate + '\'' +
                ", state='" + state + '\'' +
                ", userInvestmentNo='" + userInvestmentNo + '\'' +
                ", loanProjectName='" + loanProjectName + '\'' +
                ", investmentAmount='" + investmentAmount + '\'' +
                ", contractUrl='" + contractUrl + '\'' +
                ", userInvestmentAmount='" + userInvestmentAmount + '\'' +
                ", stateName='" + stateName + '\'' +
                ", borrower='" + borrower + '\'' +
                ", loanProjectId=" + loanProjectId +
                '}';
    }
}
