/*
 *
 *  LoanProject.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class LoanProject implements Serializable {

    public ArrayList<FinancialCoupon> financialCouponList;    //支持的优惠券列表
    public String interestRate;    //年化利率
    public String perAmount;    //每份金额
    public String loanProjectDesc;    //项目描述
    public String state;    //项目状态 CREATE-创建中 APPLY-申请中 INRAISE-募集中 RAISED-募集完成 SETUP-项目成立 REPAY-还款中 COMPLETE-已完成 CANCEL-取消 DBL-已删除
    public String startAmount;    //起投金额
    public Number periodNum;    //投资期限
    public String investProcess;    //项目投资进度
    public String loanProjectName;    //项目名称
    public ArrayList<ApartmentAtta> apartmentAttaList;    //租客合同附件列表
    public String loanProjectPurpose;    //资金用途
    public String balance;    //项目剩余可投金额
    public String amount;    //项目总额
    public String contractUrl;    //合同链接地址
    public ArrayList<TenementAtta> tenementAttaList;    //公寓附件列表
    public String stateName;    //项目状态名称
    public String picUrl;    //图片地址
    public String interestDate;    //起息日
    public ArrayList<LoanProjectEnsure> loanProjectEnsureList;    //保障措施列表
    public String endDate;    //项目截止日期(兑付日)
    public String repaySource;    //还款来源
    public LoanApartment loanApartment;    //贷款公寓信息
    public Number loanProjectId;    //项目编号

    @Override
    public String toString (){
        return "LoanProject{" +
                "financialCouponList=" + financialCouponList +
                ", interestRate='" + interestRate + '\'' +
                ", perAmount='" + perAmount + '\'' +
                ", loanProjectDesc='" + loanProjectDesc + '\'' +
                ", state='" + state + '\'' +
                ", startAmount='" + startAmount + '\'' +
                ", periodNum=" + periodNum +
                ", investProcess='" + investProcess + '\'' +
                ", loanProjectName='" + loanProjectName + '\'' +
                ", apartmentAttaList=" + apartmentAttaList +
                ", loanProjectPurpose='" + loanProjectPurpose + '\'' +
                ", balance='" + balance + '\'' +
                ", amount='" + amount + '\'' +
                ", contractUrl='" + contractUrl + '\'' +
                ", tenementAttaList=" + tenementAttaList +
                ", stateName='" + stateName + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", interestDate='" + interestDate + '\'' +
                ", loanProjectEnsureList=" + loanProjectEnsureList +
                ", endDate='" + endDate + '\'' +
                ", repaySource='" + repaySource + '\'' +
                ", loanApartment=" + loanApartment +
                ", loanProjectId=" + loanProjectId +
                '}';
    }


}
