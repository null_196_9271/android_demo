/*
 *
 *  Information.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Information implements Serializable {
    public Information(String author, String imageUrl, Number informationId, String infoTitle, String infoUrl, String releaseTime) {
        this.author = author;
        this.imageUrl = imageUrl;
        this.informationId = informationId;
        this.infoTitle = infoTitle;
        this.infoUrl = infoUrl;
        this.releaseTime = releaseTime;
    }

    public Information() {
    }

    public String infoTitle;    //资讯标题
    public Number informationId;    //资讯编号
    public String releaseTime;    //资讯发布时间
    public String imageUrl;    //资讯图片地址
    public String infoUrl;    //资讯链接
    public String author;    //发布者

    @Override
    public String toString (){
        return "Information{" +
                "infoTitle='" + infoTitle + '\'' +
                ", informationId=" + informationId +
                ", releaseTime='" + releaseTime + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", infoUrl='" + infoUrl + '\'' +
                ", author='" + author + '\'' +
                '}';
    }


}
