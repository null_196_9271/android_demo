package com.uzhu.project.service;/*
 *
 *  AppServiceMediator.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */


import android.content.Context;

import com.mvvm.framework.service.MobileHead;
import com.mvvm.framework.service.RequestResult;
import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.service.ServiceResponse;
import com.mvvm.framework.service.task.BaseAsyncTask;
import com.mvvm.framework.util.DeviceUtil;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.service.models.AccountInfo;
import com.uzhu.project.service.models.Banner;
import com.uzhu.project.service.models.Image;
import com.uzhu.project.service.models.IntegralInfo;
import com.uzhu.project.service.models.InvestSuccessInfo;
import com.uzhu.project.service.models.LoanProject;
import com.uzhu.project.service.models.LoanProjectAccountInfo;
import com.uzhu.project.service.models.Lottery;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Pager;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.service.models.RaiseProject;
import com.uzhu.project.service.models.RaiseProjectAccountInfo;
import com.uzhu.project.service.models.SecurityInfo;
import com.uzhu.project.service.models.User;
import com.uzhu.project.service.models.Version;
import com.uzhu.project.util.AppConfig;

import java.io.File;
import java.util.List;

import static com.uzhu.project.service.AppServiceMediatorAPI.requestDataType;


@SuppressWarnings({"rawtypes", "unchecked"})

public class AppServiceMediator {

    public final static String APP_CODE = "PMSAPP";
    public final static String SOURCE_CODE = "GooglePlay";// 下载渠道号
    public final static String PLATFORM = "android";// 设备平台
    public final static String CLIENT_ID = "clientId";// 设备注册获得的id
    public final static String SOURCE_FROM = "mobile";// 用户注册来源

    private AppServiceMediatorAPI mAppServiceMediatorAPI;
    //单例实现
    private static AppServiceMediator mediator;

    public static AppServiceMediator sharedInstance() {
        if (mediator == null) {
            synchronized (AppServiceMediator.class) {
                if (mediator == null) {
                    mediator = new AppServiceMediator();
                    MobileHead.getMobileHead().applicationCode = APP_CODE;
                    MobileHead.getMobileHead().sourceId = SOURCE_CODE;
                    ;
                    MobileHead.getMobileHead().version = DeviceUtil.getAppVersionName(AppApplication.getContext());
                }
            }
        }
        return mediator;
    }

    //构造方法 生成服务所需的API实例
    private AppServiceMediator() {
        getAppServiceMediatorAPI();
    }


    //业务服务API实例初始化配置
    public AppServiceMediatorAPI getAppServiceMediatorAPI() {
        if (mAppServiceMediatorAPI == null) {
            mAppServiceMediatorAPI = new AppServiceMediatorAPI();
            configAddress();
        }
        return mAppServiceMediatorAPI;
    }

    public void configAddress() {
        boolean developing = AppApplication.getInstance().isDeveloping();
        try {
            AppServiceMediatorAPI.serverAddress = developing ? AppConfig.env.getCurrServerURL() : AppConfig.SERVERURL_PRD;
            AppServiceMediatorAPI.securityAddress = developing ? AppConfig.env.getCurrSecurityURL() : AppConfig.SECURITYSERVERURL_PRD;
            AppServiceMediatorAPI.h5Address = developing ? AppConfig.env.getCurrSecurityURL() : AppConfig.H5URL_PRD;
            requestDataType = new AppRequestDataType();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    //登录
    public ServiceFuture login(final String accountName, final String password) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<User> task = new BaseAsyncTask<User>() {
            @Override
            protected ServiceResponse<RequestResult<User>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.login(accountName, password);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //登出
    public ServiceFuture logout() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.logout();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //注册
    public ServiceFuture doRegister(final String smsCode, final String accountName, final String password, final String inviter) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.doRegister(smsCode, accountName, password, inviter);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //实名认证
    public ServiceFuture authenticateName(final String userIdCard, final String userName) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.authenticateName(userIdCard, userName);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //用户详情
    public ServiceFuture userInfo() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<User> task = new BaseAsyncTask<User>() {
            @Override
            protected ServiceResponse<RequestResult<User>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userInfo();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //检查帐号
    public ServiceFuture validateAccountName(final String accountName, final String type) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.validateAccountName(accountName, type);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //更改密码第二步:验证帐号身份证
    public ServiceFuture validateIdCard(final String smsCode, final String accountName, final String userIdCard) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.validateIdCard(smsCode, accountName, userIdCard);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //重置密码
    public ServiceFuture resetPassword(final String randomCode, final String accountName, final String password) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.resetPassword(randomCode, accountName, password);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //修改密码
    public ServiceFuture updatePassword(final String theNewPwd, final String theOldPwd) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.updatePassword(theNewPwd, theOldPwd);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //注册设备
    public ServiceFuture doClientRegister(Context context) {
        ServiceFuture serviceFuture = new ServiceFuture();
        final String os = PLATFORM;
        final String lastCityCode = "";
        final String deviceInfo = DeviceUtil.getDeviceModel();
        final String lastAppVersion = DeviceUtil.getAppVersionName(context);
        final String deviceModelId = DeviceUtil.getDeviceModel();
        final String deviceMfd = DeviceUtil.getDeviceManufacturer();
        final String lastOsVersion = DeviceUtil.getDeviceVersion();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.doClientRegister(deviceMfd, deviceModelId, lastCityCode, lastOsVersion, deviceInfo, os, lastAppVersion);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //版本号是否需要更新
    public ServiceFuture isVersionUpdate() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Version> task = new BaseAsyncTask<Version>() {
            @Override
            protected ServiceResponse<RequestResult<Version>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.isVersionUpdate();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //短信验证码
    public ServiceFuture getSMSCode(final String smsType, final String mobile) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.getSMSCode(smsType, mobile);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //上传图片
    public ServiceFuture upload(final File uploadfile) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Image> task = new BaseAsyncTask<Image>() {
            @Override
            protected ServiceResponse<RequestResult<Image>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.upload(uploadfile);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //banner列表
    public ServiceFuture bannerList() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<List<Banner>> task = new BaseAsyncTask<List<Banner>>() {
            @Override
            protected ServiceResponse<RequestResult<List<Banner>>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.bannerList();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //资讯列表
    public ServiceFuture informationList(final Page page) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.informationList(page);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //签到
    public ServiceFuture signIn() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.signIn();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //房租宝项目列表
    public ServiceFuture loanProjectList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.loanProjectList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升项目列表
    public ServiceFuture raiseProjectList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.raiseProjectList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //房租宝项目详情
    public ServiceFuture loanProjectDetail(final Number loanProjectId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<LoanProject> task = new BaseAsyncTask<LoanProject>() {
            @Override
            protected ServiceResponse<RequestResult<LoanProject>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.loanProjectDetail(loanProjectId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //项目投资记录列表
    public ServiceFuture projectInvestmentList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.projectInvestmentList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //房租宝项目投资信息
    public ServiceFuture loanProjectInvestInfo(final Number loanProjectId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<LoanProject> task = new BaseAsyncTask<LoanProject>() {
            @Override
            protected ServiceResponse<RequestResult<LoanProject>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.loanProjectInvestInfo(loanProjectId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升项目详情
    public ServiceFuture raiseProjectDetail(final Number raiseProjectId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<RaiseProject> task = new BaseAsyncTask<RaiseProject>() {
            @Override
            protected ServiceResponse<RequestResult<RaiseProject>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.raiseProjectDetail(raiseProjectId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升年化利率列表
    public ServiceFuture interestRateList(final Number raiseProjectId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<List> task = new BaseAsyncTask<List>() {
            @Override
            protected ServiceResponse<RequestResult<List>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.interestRateList(raiseProjectId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //投资项目
    public ServiceFuture investProject(final String investType, final String amount, final Number userFinancialCouponId, final Number projectId, final String type) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.investProject(investType, amount, userFinancialCouponId, projectId, type);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //投资成功信息
    public ServiceFuture investSuccess(final String type, final Number investmentId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<InvestSuccessInfo> task = new BaseAsyncTask<InvestSuccessInfo>() {
            @Override
            protected ServiceResponse<RequestResult<InvestSuccessInfo>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.investSuccess(type, investmentId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //账号信息
    public ServiceFuture accountInfo() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<AccountInfo> task = new BaseAsyncTask<AccountInfo>() {
            @Override
            protected ServiceResponse<RequestResult<AccountInfo>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.accountInfo();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //安全信息
    public ServiceFuture securityInfo() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<SecurityInfo> task = new BaseAsyncTask<SecurityInfo>() {
            @Override
            protected ServiceResponse<RequestResult<SecurityInfo>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.securityInfo();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //修改头像
    public ServiceFuture modAvatar(final String avatarUrl) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.modAvatar(avatarUrl);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //设置支付密码
    public ServiceFuture setPayPassword() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.setPayPassword();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //修改支付密码
    public ServiceFuture modPayPassword() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.modPayPassword();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //重置支付密码
    public ServiceFuture resetPayPassword() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.resetPayPassword();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //站内信列表
    public ServiceFuture userMessageList(final Page page) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userMessageList(page);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //账户交易记录列表
    public ServiceFuture accountTradeList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.accountTradeList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //获取优惠券列表
    public ServiceFuture userFinancialCouponList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userFinancialCouponList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //积分信息
    public ServiceFuture userIntegralInfo() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<IntegralInfo> task = new BaseAsyncTask<IntegralInfo>() {
            @Override
            protected ServiceResponse<RequestResult<IntegralInfo>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userIntegralInfo();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //积分明细列表
    public ServiceFuture userIntegralList(final Page page) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userIntegralList(page);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //积分商品列表
    public ServiceFuture userIntegralProductList(final Page page) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userIntegralProductList(page);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //积分商品兑换
    public ServiceFuture userIntegralExchange(final Number financialCouponId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userIntegralExchange(financialCouponId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //房租宝资产信息
    public ServiceFuture loanProjectAccountInfo() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<LoanProjectAccountInfo> task = new BaseAsyncTask<LoanProjectAccountInfo>() {
            @Override
            protected ServiceResponse<RequestResult<LoanProjectAccountInfo>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.loanProjectAccountInfo();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //房租宝投资记录列表
    public ServiceFuture userInvestmentList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userInvestmentList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //支付投资记录
    public ServiceFuture payUserInvestment(final String type, final Number investmentId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.payUserInvestment(type, investmentId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //房租宝回款计划表
    public ServiceFuture investmentReturnedList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.investmentReturnedList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升资产信息
    public ServiceFuture raiseProjectAccountInfo(final Number raiseProjectId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<RaiseProjectAccountInfo> task = new BaseAsyncTask<RaiseProjectAccountInfo>() {
            @Override
            protected ServiceResponse<RequestResult<RaiseProjectAccountInfo>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.raiseProjectAccountInfo(raiseProjectId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升用户投资记录列表
    public ServiceFuture userRaiseInvestmentList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.userRaiseInvestmentList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升已匹配资产列表
    public ServiceFuture raiseProjectUserInvestmentList(final Page page, final Params params) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Pager> task = new BaseAsyncTask<Pager>() {
            @Override
            protected ServiceResponse<RequestResult<Pager>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.raiseProjectUserInvestmentList(page, params);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //申请转出月月升投资
    public ServiceFuture rollOutUserRaiseInvestment(final Number userRaiseInvestmentId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.rollOutUserRaiseInvestment(userRaiseInvestmentId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //充值
    public ServiceFuture recharge(final String amount) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.recharge(amount);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //提现
    public ServiceFuture withdraw(final String amount) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.withdraw(amount);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //月月升项目投资信息
    public ServiceFuture raiseProjectInvestInfo(final Number raiseProjectId) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<RaiseProject> task = new BaseAsyncTask<RaiseProject>() {
            @Override
            protected ServiceResponse<RequestResult<RaiseProject>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.raiseProjectInvestInfo(raiseProjectId);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //抽奖
    public ServiceFuture lottery() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Lottery> task = new BaseAsyncTask<Lottery>() {
            @Override
            protected ServiceResponse<RequestResult<Lottery>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.lottery();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //获取我的抽奖次数
    public ServiceFuture lotteryCount() {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Number> task = new BaseAsyncTask<Number>() {
            @Override
            protected ServiceResponse<RequestResult<Number>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.lotteryCount();
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //获取我的消息数量
    public ServiceFuture getMessagecount(final String searchTime) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<Number> task = new BaseAsyncTask<Number>() {
            @Override
            protected ServiceResponse<RequestResult<Number>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.getMessagecount(searchTime);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }

    //获取splash图片
    public ServiceFuture splashImage(final int displayWidth, final int displayHeight) {
        ServiceFuture serviceFuture = new ServiceFuture();
        BaseAsyncTask<String> task = new BaseAsyncTask<String>() {
            @Override
            protected ServiceResponse<RequestResult<String>> doInBackground(String... param) {
                return mAppServiceMediatorAPI.splashImage(displayWidth, displayHeight);
            }
        };
        serviceFuture.setFutureTask(task);
        task.execute();
        return serviceFuture;
    }
}
