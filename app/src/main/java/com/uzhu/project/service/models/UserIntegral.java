/*
 *
 *  UserIntegral.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class UserIntegral implements Serializable {

    public String stateName;    //积分状态名称
    public String state;    //积分状态 DUE-已生效 DBL-删除
    public Number integral;    //产生积分
    public String generateDate;    //产生日期
    public Number userIntegralId;    //积分编号
    public String integralTypeName;    //积分类型名称
    public String integralType;    //积分类型 REGISTER-注册 INVEST-投资 SIGNIN-签到 EXCHANGE-兑换

    @Override
    public String toString (){
        return "UserIntegral{" +
                "stateName='" + stateName + '\'' +
                ", state='" + state + '\'' +
                ", integral=" + integral +
                ", generateDate='" + generateDate + '\'' +
                ", userIntegralId=" + userIntegralId +
                ", integralTypeName='" + integralTypeName + '\'' +
                ", integralType='" + integralType + '\'' +
                '}';
    }


}
