package com.uzhu.project.service.models;

import java.io.Serializable;

/**
 * Created by John on 2017/1/3 0003.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 11:46
 * UpdateDescribe
 */

public class CouponBody implements Serializable {
    public Number amount;
    public Number investAmount;
    public Number discount;
    public Number integral;

}
