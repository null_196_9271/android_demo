/*
 *
 *  LoanProjectEnsure.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class LoanProjectEnsure implements Serializable {

    public String ensureTitle;    //保障标题
    public String ensureContext;    //保障内容
    public Number loanProjectEnsureId;    //保障编号

    @Override
    public String toString (){
        return "LoanProjectEnsure{" +
                "ensureTitle='" + ensureTitle + '\'' +
                ", ensureContext='" + ensureContext + '\'' +
                ", loanProjectEnsureId=" + loanProjectEnsureId +
                '}';
    }


}
