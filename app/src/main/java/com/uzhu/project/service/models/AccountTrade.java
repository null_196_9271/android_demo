/*
 *
 *  AccountTrade.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class AccountTrade implements Serializable {

    public String logTime;    //交易时间
    public String balance;    //交易后余额
    public String amount;    //交易金额
    public String logTypeName;    //交易类型名称
    public String remark;    //交易详情
    public String logType;    //交易类型 DEPOSIT-充值 WITHDRAW-提现 INVEST-投资 PRINCIPAL-本金返还 INTEREST-利息返还 EXTRAINTEREST-加息券利息返还 OTHER-其他
    public Number accountTradeId;    //账户交易编号

    @Override
    public String toString (){
        return "AccountTrade{" +
                "logTime='" + logTime + '\'' +
                ", balance='" + balance + '\'' +
                ", amount='" + amount + '\'' +
                ", logTypeName='" + logTypeName + '\'' +
                ", remark='" + remark + '\'' +
                ", logType='" + logType + '\'' +
                ", accountTradeId=" + accountTradeId +
                '}';
    }


}
