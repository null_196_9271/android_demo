/*
 *
 *  IntegralInfo.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class IntegralInfo implements Serializable {

    public Number couponNum;    //已兑换优惠券数量
    public Number usedIntegral;    //已用积分
    public Number integralAmount;    //可用积分

    @Override
    public String toString (){
        return "IntegralInfo{" +
                "couponNum=" + couponNum +
                ", usedIntegral=" + usedIntegral +
                ", integralAmount=" + integralAmount +
                '}';
    }


}
