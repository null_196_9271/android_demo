package com.uzhu.project.service.models;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Environment implements Serializable {

    private String currEnvironment;

    private ArrayList<EnvConfing> serverURLs;

    private ArrayList<EnvConfing> securityURLs;

    private ArrayList<EnvConfing> imageURLs;

    private ArrayList<EnvConfing> messageCollectorServerURLs;

    public Environment() {
        currEnvironment = "";
        serverURLs = new ArrayList<EnvConfing>();
        securityURLs = new ArrayList<EnvConfing>();
        imageURLs = new ArrayList<EnvConfing>();
        messageCollectorServerURLs = new ArrayList<EnvConfing>();
    }

    public String getCurrEnvironment() {
        return currEnvironment;
    }

    public void setCurrEnvironment(String currEnvironment) {
        this.currEnvironment = currEnvironment;
    }

    public ArrayList<EnvConfing> getServerURLs() {
        return serverURLs;
    }

    public void setServerURLs(ArrayList<EnvConfing> serverURLs) {
        this.serverURLs = serverURLs;
    }

    public ArrayList<EnvConfing> getSecurityURLs() {
        return securityURLs;
    }

    public void setSecurityURLs(ArrayList<EnvConfing> securityURLs) {
        this.securityURLs = securityURLs;
    }

    public ArrayList<EnvConfing> getImageURLs() {
        return imageURLs;
    }

    public void setImageURLs(ArrayList<EnvConfing> imageURLs) {
        this.imageURLs = imageURLs;
    }

    public ArrayList<EnvConfing> getMessageCollectorServerURLs() {
        return messageCollectorServerURLs;
    }

    public void setMessageCollectorServerURLs(ArrayList<EnvConfing> messageCollectorServerURLs) {
        this.messageCollectorServerURLs = messageCollectorServerURLs;
    }

    /**
     * 获取当前环境的服务地址
     */
    public String getCurrServerURL() throws NoSuchFieldException {
        for (EnvConfing confing:serverURLs) {
            if (confing !=null && confing.configTitle.equals(currEnvironment)) {
                return confing.configValue;
            }
        }
        throw new NoSuchFieldException("No server URL found in environment:"+currEnvironment);
    }

    /**
     * 获取当前环境的安全地址
     */
    public String getCurrSecurityURL() throws NoSuchFieldException {
        for (EnvConfing confing:securityURLs) {
            if (confing.configTitle.equals(currEnvironment)) {
                return confing.configValue;
            }
        }
        throw new NoSuchFieldException("No security URL found in environment:"+currEnvironment);
    }

    /**
     * 获取当前环境的图片地址
     */
    public String getCurrImageURL() throws NoSuchFieldException {
        for (EnvConfing confing:imageURLs) {
            if (confing.configTitle.equals(currEnvironment)) {
                return confing.configValue;
            }
        }
        throw new NoSuchFieldException("No image URL found in environment:"+currEnvironment);
    }

    /**
     * 获取当前环境的信息搜集地址
     */
    public String getCurrMessageCollectorServerURL() throws NoSuchFieldException {
        for (EnvConfing confing:messageCollectorServerURLs) {
            if (confing.configTitle.equals(currEnvironment)) {
                return confing.configValue;
            }
        }
        throw new NoSuchFieldException("No messageCollectorServer URL found in environment:"+currEnvironment);
    }

    /**
     * 当前是否开发环境
     */
    public Boolean isDevelopEnvironment() {
        if (!currEnvironment.equals("PRD")) {
            return true;
        } else {
            return false;
        }
    }
}
