/*
 *
 *  RaiseProjectInvestment.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class RaiseProjectInvestment implements Serializable {

    public String dueDate;    //到期日期(没有到期日期返回预计的到期日期)
    public String contractUrl;    //合同链接地址
    public String userInvestmentAmount;    //用户投资金额
    public String stateName;    //投资记录状态名称
    public String state;    //投资记录状态 UNPAY-未支付 INVESTMENT-已投资 REPAYMENT-还款中 COMPLETE-已完成 REFUND-退款 CLOSE-取消 DBL-已删除
    public String borrower;    //借款人
    public String generateDate;    //投资日期
    public String loanProjectName;    //项目名称
    public Number loanProjectId;    //项目编号
    public Number raiseProjectInvestmentId;    //用户投资编号

    @Override
    public String toString (){
        return "RaiseProjectInvestment{" +
                "dueDate='" + dueDate + '\'' +
                ", contractUrl='" + contractUrl + '\'' +
                ", userInvestmentAmount='" + userInvestmentAmount + '\'' +
                ", stateName='" + stateName + '\'' +
                ", state='" + state + '\'' +
                ", borrower='" + borrower + '\'' +
                ", generateDate='" + generateDate + '\'' +
                ", loanProjectName='" + loanProjectName + '\'' +
                ", loanProjectId=" + loanProjectId +
                ", raiseProjectInvestmentId=" + raiseProjectInvestmentId +
                '}';
    }


}
