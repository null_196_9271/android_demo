/*
 *
 *  Version.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class Version implements Serializable {

    public String intro;    //描述
    public String title;    //版本title
    public String needUpdate;    //是否需要强制更新
    public String packageSize;    //安装包大小
    public String downloadUrl;    //下载地址
    public String version;    //版本号

    @Override
    public String toString (){
        return "Version{" +
                "intro='" + intro + '\'' +
                ", title='" + title + '\'' +
                ", needUpdate='" + needUpdate + '\'' +
                ", packageSize='" + packageSize + '\'' +
                ", downloadUrl='" + downloadUrl + '\'' +
                ", version='" + version + '\'' +
                '}';
    }


}
