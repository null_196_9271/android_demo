/*
 *
 *  InvestSuccessInfo.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class InvestSuccessInfo implements Serializable {

    public String projectName;    //项目名称
    public String amount;    //投资金额
    public Number projectId;    //项目编号

    @Override
    public String toString (){
        return "InvestSuccessInfo{" +
                "projectName='" + projectName + '\'' +
                ", amount='" + amount + '\'' +
                ", projectId=" + projectId +
                '}';
    }


}
