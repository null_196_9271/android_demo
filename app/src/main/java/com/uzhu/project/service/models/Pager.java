/*
 *
 *  Pager.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Pager implements Serializable {

    public ArrayList<Information> informationList;
    public ArrayList<FinancialCoupon> financialCouponList;
    public ArrayList<UserRaiseInvestment> userRaiseInvestmentList;
    public ArrayList<AccountTrade> accountTradeList;
    public ArrayList<UserInvestmentReturned> userInvestmentReturnedList;
    public ArrayList<RaiseProjectInvestment> raiseProjectInvestmentList;
    public ArrayList<UserIntegral> userIntegralList;
    public ArrayList<UserInvestment> userInvestmentList;
    public ArrayList<UserMessage> userMessageList;
    public ArrayList<UserFinancialCoupon> userFinancialCouponList;
    public Page page;
    public ArrayList<RaiseProject> raiseProjectList;
    public ArrayList<LoanProject> loanProjectList;

    public Pager() {
        page = new Page();
        informationList = new ArrayList<>();
        financialCouponList = new ArrayList<>();
        userRaiseInvestmentList = new ArrayList<>();
        accountTradeList = new ArrayList<>();
        userInvestmentReturnedList = new ArrayList<>();
        raiseProjectInvestmentList = new ArrayList<>();
        userIntegralList = new ArrayList<>();
        userInvestmentList = new ArrayList<>();
        userMessageList = new ArrayList<>();
        userFinancialCouponList = new ArrayList<>();
        raiseProjectList = new ArrayList<>();
        loanProjectList = new ArrayList<>();
    }

    @Override
    public String toString (){
        return "Pager{" +
                "informationList=" + informationList +
                ", financialCouponList=" + financialCouponList +
                ", userRaiseInvestmentList=" + userRaiseInvestmentList +
                ", accountTradeList=" + accountTradeList +
                ", userInvestmentReturnedList=" + userInvestmentReturnedList +
                ", raiseProjectInvestmentList=" + raiseProjectInvestmentList +
                ", userIntegralList=" + userIntegralList +
                ", userInvestmentList=" + userInvestmentList +
                ", userMessageList=" + userMessageList +
                ", userFinancialCouponList=" + userFinancialCouponList +
                ", page=" + page +
                ", homeRaiseProjectList=" + raiseProjectList +
                ", loanProjectList=" + loanProjectList +
                '}';
    }


}
