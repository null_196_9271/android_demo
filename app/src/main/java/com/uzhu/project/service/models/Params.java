/*
 *
 *  Params.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Params implements Serializable {

    public ArrayList<String> stateList = new ArrayList<>();    //项目投资状态列表
    public Number projectId;    //房租宝项目编号 非必填字段
    public Number sortBy;    //排序规则 1-按入库顺序排列
    public Number userInvestmentId;    //投资记录编号
    public ArrayList<String> logTypeList = new ArrayList<>();    //​交易类型列表
    public Number raiseProjectId;    //月月升项目编号
    public String isHome;    //是否首页查询（首页不查询出季季升） 1-是 0-否
    public String type;    //项目类型 1-房租宝项目 2-月月升项目 非必填字段

    @Override
    public String toString() {
        return "Params{" +
                "isHome='" + isHome + '\'' +
                ", stateList=" + stateList +
                ", projectId=" + projectId +
                ", sortBy='" + sortBy + '\'' +
                ", userInvestmentId=" + userInvestmentId +
                ", logTypeList=" + logTypeList +
                ", raiseProjectId=" + raiseProjectId +
                ", type='" + type + '\'' +
                '}';
    }
}
