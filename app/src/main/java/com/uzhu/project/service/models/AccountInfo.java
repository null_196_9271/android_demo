/*
 *
 *  AccountInfo.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class AccountInfo implements Serializable {

    public String amount;    //现有资产
    public String balance;    //账户余额
    public Number integralAmount;    //总积分
    public Number accountId;    //账户编号
    public String loanProjectAmount;    //房租宝资产
    public Number financialCouponNum;    //优惠券数量
    public ArrayList<RaiseProject> raiseProjectList;
    public String totalIncome;    //累计收益

    @Override
    public String toString (){
        return "AccountInfo{" +
                "amount='" + amount + '\'' +
                ", balance='" + balance + '\'' +
                ", integralAmount=" + integralAmount +
                ", accountId=" + accountId +
                ", loanProjectAmount='" + loanProjectAmount + '\'' +
                ", financialCouponNum=" + financialCouponNum +
                ", homeRaiseProjectList=" + raiseProjectList +
                ", totalIncome='" + totalIncome + '\'' +
                '}';
    }


}
