/*
 *
 *  UserFinancialCoupon.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class UserFinancialCoupon implements Serializable {

    public String dueDate;    //优惠券到期日期
    public String financialCouponBody;    //优惠券内容
    public String financialCouponType;    //优惠券类型 INTEREST:加息券 CASH:代金券
    public Number financialCouponId;    //优惠券编号
    public Number userFinancialCouponId;    //用户优惠券编号
    public String source;    //优惠券来源 REGISTER-注册获取 IDCARDAUTH-实名认证 PAYPASSWORD-支付密码 RECOMMEND-推荐别人 RECOMMENDCODE-使用推荐码
    public String stateName;    //优惠券状态名称
    public String financialCouponName;    //优惠券类型名字
    public String state;    //优惠券状态 USABLE-可用 PASTDUE-过期 USED-已使用 SETTLE-已结算
    public String sourceName;    //优惠券来源名称
    public String generateDate;    //优惠券获取日期

    @Override
    public String toString (){
        return "UserFinancialCoupon{" +
                "dueDate='" + dueDate + '\'' +
                ", financialCouponBody='" + financialCouponBody + '\'' +
                ", financialCouponType='" + financialCouponType + '\'' +
                ", financialCouponId=" + financialCouponId +
                ", userFinancialCouponId=" + userFinancialCouponId +
                ", source='" + source + '\'' +
                ", stateName='" + stateName + '\'' +
                ", financialCouponName='" + financialCouponName + '\'' +
                ", state='" + state + '\'' +
                ", sourceName='" + sourceName + '\'' +
                ", generateDate='" + generateDate + '\'' +
                '}';
    }


}
