/*
 *
 *  UserRaiseInvestment.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;
import java.io.Serializable;

@SuppressWarnings("serial")
public class UserRaiseInvestment implements Serializable {

    public String transferApplyTime;    //转出申请时间
    public String interestRate;    //当前年化收益率
    public String state;    //投资记录状态 UNPAY-未支付 INVESTMENT-已转入 WITHDRAWAPPLY-转出申请 WITHDRAWPENDING-转出排队中 COMPLETE-已完成 REFUND-退款 CLOSE-取消 DBL-删除
    public String userInvestmentNo;    //用户投资号
    public Number transferDay;    //可转出日期
    public String financialAmount;    //理财中的金额
    public String userInvestAmount;    //投资金额
    public String transferTime;    //实际转出时间
    public String stateName;    //投资记录状态名称
    public String investTime;    //转入时间
    public Number userRaiseInvestmentId;    //用户投资编号
    public String transferAmount;    //可转出金额
    public Number transferNo;
    @Override
    public String toString (){
        return "UserRaiseInvestment{" +
                "transferApplyTime='" + transferApplyTime + '\'' +
                ", interestRate='" + interestRate + '\'' +
                ", state='" + state + '\'' +
                ", userInvestmentNo='" + userInvestmentNo + '\'' +
                ", transferDay=" + transferDay +
                ", financialAmount='" + financialAmount + '\'' +
                ", userInvestAmount='" + userInvestAmount + '\'' +
                ", transferTime='" + transferTime + '\'' +
                ", stateName='" + stateName + '\'' +
                ", investTime='" + investTime + '\'' +
                ", userRaiseInvestmentId=" + userRaiseInvestmentId +
                ", transferAmount='" + transferAmount + '\'' +
                '}';
    }


}
