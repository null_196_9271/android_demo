/*
 *
 *  Page.java
 *
 *  Created by CodeRobot on 2016-12-22
 *  Copyright (c) 2016年 com.uzhu. All rights reserved.
 *
 */

package com.uzhu.project.service.models;


import com.uzhu.project.base.CommonConst;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Page implements Serializable {

    public Number pages;
    public Number totalRec;
    public Number pageIndex;
    public Number pageSize;
    public Page(){
        pages = 0;
        totalRec = 0;
        pageSize = CommonConst.PAGE_SIZE;
        pageIndex = CommonConst.PAGE_INDEX;
    }
    @Override
    public String toString (){
        return "Page{" +
                "pages=" + pages +
                ", totalRec=" + totalRec +
                ", pageIndex=" + pageIndex +
                ", pageSize=" + pageSize +
                '}';
    }


}
