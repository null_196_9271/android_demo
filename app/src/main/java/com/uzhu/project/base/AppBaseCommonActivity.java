package com.uzhu.project.base;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ScrollView;

import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.R;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.view.base.AppTitle;

import butterknife.ButterKnife;

/**
 * Created by John on 2016/12/15 0015.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 13:44
 * UpdateDescribe
 */

public abstract class AppBaseCommonActivity<T extends AppViewModel> extends AppBaseFragmentActivity {
    public String TAG;
    public T mViewModel;
    protected AppServiceMediator mApi;
    AppTitle topTitle;
    FrameLayout llContent;
    FrameLayout bottomView;
    protected SwipeRefreshLayout srlLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getName();
        beforeSetView(savedInstanceState);
        //// TODO: 2017/3/1 0001 封装界面加载
        if (!modContentView()) {
            setContentView(R.layout.common_activity);
            srlLayout = (SwipeRefreshLayout) findViewById(R.id.srl_layout);
            llContent = (FrameLayout) findViewById(R.id.ll_content);
            bottomView = (FrameLayout) findViewById(R.id.bottomView);
            topTitle = (AppTitle) findViewById(R.id.top_title);
            srlLayout.setEnabled(needRefresh());
            if (needRefresh()) {
                srlLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        onSRLayoutReFresh();
                    }
                });
            }
            initTitle();
            initBottomView();
            setShowView();
            if (isSetStateBar()) {
                setStateBar(topTitle.getBackgroundColor());
            }
        } else {
            setContentView(contentViewByLayoutId());
            ButterKnife.bind(this);
        }
        initID();
        initData();
        loadServiceData(AppServiceMediator.sharedInstance());
        initView();
        afterSetView(savedInstanceState);
        AppCollectCode.initPageCodeMap();

    }
    //if you want to use your's contentView, override it and retunr true
    protected boolean modContentView() {
        return false;
    }

    protected void initID() {
    }

    protected boolean isSetStateBar() {
        return true;
    }
    //仅仅在安卓5.1以上开启 适配状态栏和title栏一样颜色 (国产机型rom更改过大,不一定完全实现)
    private void setStateBar(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }

    private void setShowView() {
        View view = View.inflate(getContextByThis(), contentViewByLayoutId(), null);
        ButterKnife.bind(this, view);
        llContent.addView(view);
        //解决srlLayout 里包含scrollview造成的滑动冲突
        final ScrollView scrollView = checkScrollView(view);
        if (scrollView != null) {
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    if (srlLayout != null) {
                        srlLayout.setEnabled(scrollView.getScrollY() == 0);
                    }
                }
            });
        }
    }
    //找到子view或者自己的scrollView并返回
    private ScrollView checkScrollView(View view) {
        if (view instanceof ScrollView) {
            return (ScrollView) view;
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                ScrollView scrollView = checkScrollView(((ViewGroup) view).getChildAt(i));
                if (scrollView != null) {
                    return scrollView;
                }
            }
        }
        return null;
    }

    private void initBottomView() {
        if (getBottomView() != -1) {
            bottomView.addView(View.inflate(getContextByThis(), getBottomView(), null));
        }

    }

    protected int getBottomView() {
        return -1;
    }


    protected void onSRLayoutReFresh() {
        loadServiceData(mApi);
    }

    @Override
    public void refreshData(String fieldName) {
        super.refreshData(fieldName);
        stopSrlLayout();
    }

    @Override
    public void alertMessage(String fieldName, int errorCode, String errorMsg) {
        super.alertMessage(fieldName, errorCode, errorMsg);
        stopSrlLayout();
    }

    private void stopSrlLayout() {
        srlLayout.setRefreshing(false);
    }

    protected boolean needRefresh() {
        return false;
    }

    protected void loadServiceData(AppServiceMediator API) {
        if (mApi == null) {
            mApi = API;
        }
    }

    @Override
    public void alreadyBindBaseViewModel() {
        super.alreadyBindBaseViewModel();
        try {
            mViewModel = (T) baseViewModel;
        } catch (Exception e) {
            LogUtil.e(TAG, "bingViewModel is error:" + e.toString());
            e.printStackTrace();
        }
    }

    private void initTitle() {
        setAppTitle(topTitle);
    }

    protected void setAppTitle(AppTitle appTitle) {
        if (titleColor() != 0) {
            appTitle.setColor(titleColor());
        }
        if (!TextUtils.isEmpty(titleText())) {
            appTitle.setTitle(titleText());
        }
        if (isCanBack()) {
            appTitle.setBackIconListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    protected String titleText() {
        return null;
    }

    protected boolean isCanBack() {
        return true;
    }

    protected abstract int contentViewByLayoutId();


    protected abstract void initData();

    protected abstract void initView();


    protected void afterSetView(Bundle savedInstanceState) {
        // You can do something ...
    }

    protected boolean needRefreshInfo() {
        return false;
    }

    protected void beforeSetView(Bundle savedInstanceState) {

    }

    protected int titleColor() {
        return R.color.colorPrimaryDark;
    }

    @Override
    public void addFutureToMap(String fieldName, ServiceFuture<?> future) {
        if (!futureIsExist(fieldName)) {
            showProgress();
            super.addFutureToMap(fieldName, future);
        } else {
            LogUtil.d(TAG, fieldName + "is exist!!!!");
        }
    }

    public void addService(String fieldName, ServiceFuture<?> future) {
        showProgress();
        if (!futureIsExist(fieldName)) {
            ServiceFuture serviceFuture = mViewModel.addServiceMediatorAPI(future, fieldName);
            super.addFutureToMap(fieldName, serviceFuture);
        } else {
            LogUtil.d(TAG, fieldName + "is exist!!!!");
        }
    }
}
