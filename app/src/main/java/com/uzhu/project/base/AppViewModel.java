/*
 *
 *  ExampleViewModel.java
 *
 *  Created by CodeRobot on 2015-1-17
 *  Copyright (c) 2015 com.pingan.installment All rights reserved.
 *
 */

package com.uzhu.project.base;

import android.content.Context;

import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.util.LogUtil;
import com.mvvm.framework.viewModel.ViewModel;

public class AppViewModel extends ViewModel {
    public String TAG;
    public AppViewModel() {
        TAG = getClass().getName();
    }

    public ServiceFuture addServiceMediatorAPI(ServiceFuture<?> serviceFuture, String fildeName) {
        serviceFuture.addServiceListener(fildeName,this);
        return serviceFuture;
    }

    public void showErrorLog(String errorObject) {
        LogUtil.d(TAG, errorObject + " is null");
    }
    public Context getContext() {
        if (controller != null) {
            if (controller instanceof Context) {
                return (Context) controller;
            }
            LogUtil.d(TAG,"get Controller error");
        }
        return null;
    }
}
