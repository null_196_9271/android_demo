package com.uzhu.project.base;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mvvm.framework.common.NotificationCenter;
import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.R;
import com.uzhu.project.base.cache.UserCenter;
import com.uzhu.project.view.TopTitleBar;

import static android.webkit.WebSettings.LOAD_NO_CACHE;

/**
 * 加载网页的公共页面
 *
 * @author
 */
public class CommonBrowserActivity extends AppBaseFragmentActivity {

	public static final String TAG = "CommonBrowserActivity";

	public TopTitleBar topTitleBar;
	public static final String URL_KEY = "url";
	public static final String HTML_KEY = "html";
	public static final String TITLE_KEY = "title";
	protected String mUrl = "";// 当前访问的页面的url
	private String mHtml = "";// 当前访问的页面的内容
	private String mTitle = "";// 当前webview的title
	private WebView webView;
	private ProgressBar mProgressBar;
	private boolean alreadyCallBack;
	private String mSinaOperationType;
	private String mProjectName;
	private long mInvestAmount;
	private boolean needNotify = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_common_browser);
		initView();
		initData();
	}


	public void initData() {
//		mProjectName = getIntent().getStringExtra("ProjectName");
//		mInvestAmount = getIntent().getLongExtra("investAmount", 0);
//		mUrl = getIntent().getStringExtra(URL_KEY);
//		mHtml = getIntent().getStringExtra(HTML_KEY);
//		mTitle = getIntent().getStringExtra(TITLE_KEY);
		if (!TextUtils.isEmpty(mUrl)) {
			//加载url
			webView.loadUrl(mUrl);
		} else if (!TextUtils.isEmpty(mHtml)) {
			//加载html
			webView.loadData(mHtml, "text/html; charset=UTF-8", null);
		} else {
			throw new IllegalArgumentException("url or html can not be null !!!");
		}
	}

	public void initView() {
		mProjectName = getIntent().getStringExtra("ProjectName");
		mInvestAmount = getIntent().getLongExtra("investAmount", 0);
		mUrl = getIntent().getStringExtra(URL_KEY);
		mHtml = getIntent().getStringExtra(HTML_KEY);
		mTitle = getIntent().getStringExtra(TITLE_KEY);
		topTitleBar = (TopTitleBar) findViewById(R.id.top_title_layout);
		topTitleBar.setTitle(mTitle == null? "":mTitle);
		topTitleBar.setLeftViewOnClickListener(new leftBackClick());
		topTitleBar.setRightViewOnClickListener(new rightClick());
		webView = (WebView) this.findViewById(R.id.activity_browser_webview);
		mProgressBar = (ProgressBar) findViewById(R.id.activity_browser_progressbar);

		webView.getSettings().setUseWideViewPort(false); //设置此属性，可任意比例缩放
		webView.getSettings().setLoadWithOverviewMode(true);    //是否缩放至屏幕的大小
		webView.getSettings().setJavaScriptEnabled(true);   //是否支持js
//		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webView.getSettings().setSupportMultipleWindows(false);  //是否支持多窗口
		webView.getSettings().setBuiltInZoomControls(false);    //是否支持手势缩放
		webView.getSettings().setSupportZoom(false); //是否支持手势缩放
		webView.getSettings().setDomStorageEnabled(true);   //是否支持浏览器存储
		webView.getSettings().setCacheMode(LOAD_NO_CACHE);   //设置浏览器缓存模式
		webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				LogUtil.d(TAG, url);
				mSinaOperationType = null;
				if (url.contains("sina")) {
					needNotify = true;
				}
				if (url.startsWith("ishouzu://success?")) {
					if (!alreadyCallBack) {
						alreadyCallBack = true;
						//解析回调参数
						Bundle bundle = new Bundle();
						String[] paramsList = url.substring(18).split("&");
						for (int i = 0; i < paramsList.length; i++) {
							String param = paramsList[i];
							String[] paramList = param.split("=");
							bundle.putString(paramList[0], paramList[1]);
						}
						//根据解析出来的参数处理逻辑
						mSinaOperationType = bundle.getString("sinaOperationType");
						if (mSinaOperationType != null) {
							if (Integer.parseInt(mSinaOperationType) == CommonConst.kSinaOperationTypeInvest) {
								//投资成功跳转投资成功页
								if (!TextUtils.isEmpty(mProjectName)) {
									bundle.putCharSequence("ProjectName", mProjectName);
								}
								if (mInvestAmount != 0) {
									bundle.putLong("investAmount", mInvestAmount);
								}
//								Route.nextController(getContextByThis(), InvestSuccessActivity.class, bundle);
								finish();
							} else {
								Intent intent = new Intent();
								intent.putExtra("sinaOperationType", mSinaOperationType);
								CommonBrowserActivity.this.setResult(RESULT_OK, intent);
								CommonBrowserActivity.this.finish();
							}
						} else {
							LogUtil.e(TAG, "sinaOperationType不能为空");
//							view.loadUrl(url);
//							return true;
						}
					}
					return true;
				} else if (url.startsWith("ishouzu://")) {
					if (url.substring(10).equals("register")) {
						if (!UserCenter.instance().isLogin()) {
//							Route.nextController(getContextByThis(), QuickRegisterActivitySetup1.class);
						}
					}
					return true;
				} else {
					view.loadUrl(url);
					return false;
				}
			}

			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				mProgressBar.setVisibility(View.VISIBLE);
			}

			public void onPageFinished(WebView view, String url) {
				mProgressBar.setVisibility(View.INVISIBLE);
				if (url.endsWith("register.html") && UserCenter.instance().isLogin()) {
					webView.loadUrl("javascript:hideRegBtn()");
				}
			}
		});

		webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int newProgress) {
				mProgressBar.setProgress(newProgress);
			}

			public void onReceivedIcon(WebView view, Bitmap icon) {
			}

			public void onReceivedTitle(WebView view, String title) {
				if (TextUtils.isEmpty(mTitle)) {
					topTitleBar.setTitle(title);
				}
			}
		});

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			if (webView.canGoBack()) {
				webView.goBack();
			} else {

				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * titlebar左侧退出按钮点击事件，退出条件根据当前title的文本来决定。
	 *
	 * @author shaojun
	 */
	class leftBackClick implements OnClickListener {
		@Override
		public void onClick(View arg0) {
			if (webView.canGoBack()) {
				webView.goBack();
			} else {
				finish();
			}
		}

	}

	class rightClick implements OnClickListener {
		@Override
		public void onClick(View v) {
			webView.reload();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		if (webView != null) {
			webView.destroy();
			webView = null;
		}
		if (needNotify) {
			NotificationCenter.defaultCenter().postNotification("refreshMeInfo", null);
		}
		super.onDestroy();
	}
}
