/*
 * ExampleBaseActivity.java
 *
 * Created by: shard on 2014-9-27
 * Copyright (c) 2014 shard. All rights reserved.
 */
package com.uzhu.project.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;

import com.mvvm.framework.common.NotificationCenter;
import com.mvvm.framework.controller.BaseActivity;
import com.mvvm.framework.route.Route;
import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.util.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.uzhu.project.R;
import com.uzhu.project.controller.activity.common.LoginActivity;
import com.uzhu.project.util.AnimationUtil;
import com.uzhu.project.util.AppConfig;
import com.uzhu.project.widget.dialog.CustomDialog;
import com.uzhu.project.widget.dialog.CustomProgressDialog;

public abstract class AppBaseActivity extends BaseActivity {
    public int displayHeight;
    public int displayWidth;
    public float density;
    public int densityDpi;
    public Context context;
    private CustomProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // 设置竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //必须要在super.onCreate之前执行,因为父类里会读取ViewModel列表进行绑定,在之后会导致绑定失败
        ViewModelPlist.init();
        DisplayMetrics metric = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(metric);
        displayWidth = metric.widthPixels; // 屏幕宽度（像素）
        displayHeight = metric.heightPixels; // 屏幕高度（像素）
        density = metric.density; // 屏幕密度（0.75 / 1.0 / 1.5）
        densityDpi = metric.densityDpi; // 屏幕密度DPI（120 / 160 / 240）
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        String className = getClass().getName();
        if (AppCollectCode.pageCodeMap.containsKey(className)) {
            MobclickAgent.onPageStart((String) AppCollectCode.pageCodeMap.get(className));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        String className = getClass().getName();
        if (AppCollectCode.pageCodeMap.containsKey(className)) {
            MobclickAgent.onPageEnd((String) AppCollectCode.pageCodeMap.get(className));
        }
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            CustomProgressDialog.Builder customBuilder = new CustomProgressDialog.Builder(
                    this);
            // TODO: 2017/3/1 0001 更改内容
            //createNew()方法中已经去开启动画,避免重复加载动画,下面的开启动画注视掉
            //create(boolean b) 里面未开启动画,需要手动开启
//			progressDialog = customBuilder.create(false);
            progressDialog = customBuilder.createNew();
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (progressDialog.isShowing()) {
            return;
        }

//		ImageView loding_bg = (ImageView) progressDialog
//				.findViewById(R.id.loding_bg);
//		AnimationUtil.startRotateAnimation(loding_bg);
        progressDialog.show();
    }


    @Override
    public void showProgress(boolean hasCancle, final String fieldName) {
        if (progressDialog == null) {
            CustomProgressDialog.Builder customBuilder = new CustomProgressDialog.Builder(
                    this);
            progressDialog = customBuilder.create(hasCancle);
            progressDialog.setCanceledOnTouchOutside(false);
            //弹出框是否显示右上角的取消（插）
            if (hasCancle) {
                progressDialog
                        .setCustomProgressDialogListener(new CustomProgressDialog.CustomProgressDialogListener() {
                            @Override
                            public void onCancleClick(CustomProgressDialog dialog) {
                                ServiceFuture<?> serviceFuture = futureMap.get(fieldName);
                                if (serviceFuture != null) {
                                    serviceFuture.cancel();
                                    removeFutureFromMap(fieldName);
                                    dialog.dismiss();
                                } else {
                                    ToastUtil.show(AppBaseActivity.this, "serviceFuture is Not in the futureMap");
                                }
                            }
                        });
            }
            //点击返回键时，取消请求
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    ServiceFuture<?> serviceFuture = futureMap.get(fieldName);
                    if (serviceFuture != null) {
                        serviceFuture.cancel();
                        removeFutureFromMap(fieldName);
                        dialog.dismiss();
                    } else {
                        ToastUtil.show(AppBaseActivity.this, "serviceFuture is Not in the futureMap");
                    }
                }
            });
        }
        ImageView loding_bg = (ImageView) progressDialog
                .findViewById(R.id.loding_bg);
        AnimationUtil.startRotateAnimation(loding_bg);
        progressDialog.show();
    }

    /**
     * 取消ProgressView
     */
    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    Dialog promptDialog;

    @Override
    public void alertMessage(String fieldName, int errorCode, String errorMsg) {
        if (errorCode == CommonConst.ERROR_CODE_TOKEN_INVALID) {
            //用户登录失效后，先清空用户登录信息，弹出dialog提示用户去登录
            AppConfig.clearLoginState();
            NotificationCenter.defaultCenter().postNotification("logout", null);
            CustomDialog.Builder customBuilder = new CustomDialog.Builder(this);
            customBuilder
                    .setTitle("提示")
                    .setMessage(getString(R.string.token_invalid))
                    .setContentView(null)
                    .setNegativeButton("登录",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    Route.nextController(AppBaseActivity.this, LoginActivity.class);
                                    promptDialog.dismiss();
                                }
                            });
            if (promptDialog != null && promptDialog.isShowing()) {
                promptDialog.dismiss();
            }
            promptDialog = customBuilder.createDialog();
            // 点击周围是否可以取消
            promptDialog.setCancelable(false);
            promptDialog.setCanceledOnTouchOutside(false);
            promptDialog.show();
        } else {
            super.alertMessage(fieldName, errorCode, errorMsg);
        }
    }

}
