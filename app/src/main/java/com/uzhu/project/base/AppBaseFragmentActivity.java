/*
 * ExampleBaseFragmentActivity.java
 *
 * Created by: shard on 2014-9-27
 * Copyright (c) 2014 shard. All rights reserved.
 */
package com.uzhu.project.base;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.util.LogUtil;
import com.mvvm.framework.util.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.uzhu.project.R;
import com.uzhu.project.controller.inf.RefreshDataForFragment;
import com.uzhu.project.service.models.Version;
import com.uzhu.project.util.AnimationUtil;
import com.uzhu.project.util.UpdateUtil;
import com.uzhu.project.widget.dialog.CustomDialog;
import com.uzhu.project.widget.dialog.CustomProgressDialog;
import com.uzhu.project.widget.swipeback.app.SwipeBackActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class AppBaseFragmentActivity extends SwipeBackActivity {
    public int displayHeight;
    public int displayWidth;
    public float density;
    public int densityDpi;
    public Context context;
    private CustomProgressDialog progressDialog;
    private Dialog promptDialog;
    private boolean isForceUpdate;
    private String downloadUrl;
    public ProgressDialog progressDialog1;
    private boolean activityIsRuning = false;
    protected String mClassName;

    private void downloadAPK() {
        final UpdateUtil updateUtil = new UpdateUtil(this, "uzhu.apk");
        updateUtil.showNotification();
        progressDialog1 = new ProgressDialog(this);
        progressDialog1.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog1.setMax(100);
        progressDialog1.setTitle("软件更新");
        progressDialog1.setMessage("下载中...");
        progressDialog1.setIndeterminate(false);
        progressDialog1.setCanceledOnTouchOutside(false);
        progressDialog1.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (progressDialog1.getProgress() == 100) {
                    return false;
                }
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    return true;
                }
                return false;
            }
        });
        progressDialog1.show();
        new Thread() {
            public void run() {
                try {
                    updateUtil.downloadApkFile(downloadUrl);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
        if (!isForceUpdate) {
            promptDialog.dismiss();
        }
    }

    private View getVersionView(Version version) {
        TextView view = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 30, 20, 30);
        view.setLayoutParams(params);
        view.setLineSpacing(1f, 1.2f);
        view.setTextColor(this.getResources()
                .getColorStateList(R.color.gray_04));
        view.setTextSize(14);
        view.setText(version.intro);
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //application里自定义的activty管理map,可以方便的对activtiy进行相关操作
        AppApplication.getInstance().addActivity(this, getClass());
        activityIsRuning = true;
        mClassName = getClass().getName();
        // 设置竖屏
        //必须要在super.onCreate之前执行,因为父类里会读取ViewModel列表进行绑定,在之后会导致绑定失败
        ViewModelPlist.init();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        displayWidth = new DisplayMetrics().widthPixels; // 屏幕宽度（像素）
        displayHeight = new DisplayMetrics().heightPixels; // 屏幕高度（像素）
        density = new DisplayMetrics().density; // 屏幕密度（0.75 / 1.0 / 1.5）
        densityDpi = new DisplayMetrics().densityDpi; // 屏幕密度DPI（120 / 160 / 240）
        context = this;
        LogUtil.i("屏幕参数", "screenWidth=" + displayWidth + "; screenHeight="
                + displayHeight + ";density=" + density + ";densityDpi="
                + densityDpi);
        super.onCreate(savedInstanceState);
        // TODO: 2017/3/1 0001 增加内容
        // eventbug 事件传递,代替项目自身框架中的 NotificationCenter(在并行操作时候会有并发修改异常)
        EventBus.getDefault().register(this);
    }


    @Subscribe
    public void update(Version version) {
        if (version == null) {
            return;
        }
        if (!activityIsRuning) {
            return;
        }
        //如果activity 销毁了 ,返回true
        if (AppBaseFragmentActivity.this.isFinishing()) {
            return;
        }
        isForceUpdate = version.needUpdate.equals(CommonConst.FORCE_UPDATE);
        downloadUrl = version.downloadUrl;

        CustomDialog.Builder customBuilder = new CustomDialog.Builder(getContextByThis());
        customBuilder
                .setTitle(version.title)
                .setContentView(getVersionView(version))
                .setNegativeButton("立即更新",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // 下载更新apk包
                                if (AppApplication.checkPermission(AppBaseFragmentActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                    // 下载更新apk包
                                    downloadAPK();
                                } else {
                                    ActivityCompat.requestPermissions(AppBaseFragmentActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 99);
                                }

                            }
                        });
        if (isForceUpdate) {
            customBuilder.setPositiveButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    promptDialog.dismiss();
                }
            });
        }
        promptDialog = customBuilder.createDialog();
        promptDialog.setCanceledOnTouchOutside(false);
        if (promptDialog != null && promptDialog.isShowing()) {
            promptDialog.dismiss();
        }
        promptDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK
                        && event.getAction() == KeyEvent.ACTION_UP) {
                    if (isForceUpdate) {
                        AppApplication.getInstance().removeAllActivity();
                    } else {
                        promptDialog.dismiss();
                    }
                    return true;
                }
                return false;
            }
        });
        promptDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //activity 恢复时候会回调onstart,恢复
        activityIsRuning = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        if (clickCode()) {
            if (AppCollectCode.pageCodeMap.containsKey(mClassName)) {
                MobclickAgent.onPageStart((String) AppCollectCode.pageCodeMap.get(mClassName));
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityIsRuning = false;
        MobclickAgent.onPause(this);
        if (clickCode()) {
            if (AppCollectCode.pageCodeMap.containsKey(mClassName)) {
                MobclickAgent.onPageEnd((String) AppCollectCode.pageCodeMap.get(mClassName));
            }
        }
    }

    @Override
    protected void onDestroy() {
        AppApplication.getInstance().removeActivity(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    protected boolean clickCode() {
        return true;
    }

    @Override
    public void showProgress() {
        if (!activityIsRuning) {
            return;
        }
        if (progressDialog == null) {
            CustomProgressDialog.Builder customBuilder = new CustomProgressDialog.Builder(
                    this);
            // TODO: 2017/3/1 0001 更改内容
            //createNew()方法中已经去开启动画,避免重复加载动画,下面的开启动画删除掉
            //create(boolean b) 里面未开启动画,需要手动开启
            progressDialog = customBuilder.createNew();
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (progressDialog.isShowing()) {
            return;
        }
        progressDialog.show();
    }

    public void showProgress(String title) {
        if (!activityIsRuning) {
            return;
        }
        if (progressDialog == null) {
            CustomProgressDialog.Builder customBuilder = new CustomProgressDialog.Builder(
                    this);
            progressDialog = customBuilder.create(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (progressDialog.isShowing()) {
            return;
        }
        progressDialog.getBuilder().setTitle(title);
        ImageView loding_bg = (ImageView) progressDialog
                .findViewById(R.id.loding_bg);
        AnimationUtil.startRotateAnimation(loding_bg);
        progressDialog.show();
    }

    @Override
    public void showProgress(boolean hasCancle, final String fieldName) {
        if (!activityIsRuning) {
            return;
        }
        if (progressDialog == null) {
            CustomProgressDialog.Builder customBuilder = new CustomProgressDialog.Builder(
                    this);
            progressDialog = customBuilder.create(hasCancle);
            progressDialog.setCanceledOnTouchOutside(false);
            //弹出框是否显示右上角的取消（插）
            if (hasCancle) {
                progressDialog
                        .setCustomProgressDialogListener(new CustomProgressDialog.CustomProgressDialogListener() {
                            @Override
                            public void onCancleClick(CustomProgressDialog dialog) {
                                ServiceFuture<?> serviceFuture = futureMap.get(fieldName);
                                if (serviceFuture != null) {
                                    serviceFuture.cancel();
                                    removeFutureFromMap(fieldName);
                                    dialog.dismiss();
                                } else {
                                    ToastUtil.show(AppBaseFragmentActivity.this, "serviceFuture is Not in the futureMap");
                                }
                            }
                        });
            }
            //点击返回键时，取消请求
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    ServiceFuture<?> serviceFuture = futureMap.get(fieldName);
                    if (serviceFuture != null) {
                        serviceFuture.cancel();
                        removeFutureFromMap(fieldName);
                        dialog.dismiss();
                    } else {
                        ToastUtil.show(AppBaseFragmentActivity.this, "serviceFuture is Not in the futureMap");
                    }
                }
            });
        }
        if (progressDialog.isShowing()) {
            return;
        }
        ImageView loding_bg = (ImageView) progressDialog
                .findViewById(R.id.loding_bg);
        AnimationUtil.startRotateAnimation(loding_bg);
        progressDialog.show();
    }

    /**
     * 取消ProgressView
     */
    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void alreadyBindBaseViewModel() {
        //在需要绑定viewModel子类中重写该方法进行绑定
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                downloadAPK();
            }
        }
    }

    public void setProgressVal(int i) {
        LogUtil.d("Tag----------", "val-------------" + i);
        progressDialog1.setProgress(i);
    }

    //需要数据刷新的Fragment只需要实现RefreshDataForFragment接口并重写方法即可
    @Override
    public void refreshData(String fieldName) {
        super.refreshData(fieldName);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof RefreshDataForFragment) {
                    ((RefreshDataForFragment) fragment).refreshData(fieldName);
                }
            }
        }
        dismissProgress();
    }

    @Override
    public void alertMessage(String fieldName, int errorCode, String errorMsg) {
        super.alertMessage(fieldName, errorCode, errorMsg);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof RefreshDataForFragment) {
                    ((RefreshDataForFragment) fragment).alertMessage(fieldName);
                }
            }
        }
        dismissProgress();
    }

    public Activity getContextByThis() {
        return this;
    }


}
