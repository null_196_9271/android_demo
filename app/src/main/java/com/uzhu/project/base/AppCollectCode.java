/*
 * TenementCollectCode.java
 *
 *  Created by: shard on 2015-7-29
 *  Copyright (c) 2015 shard. All rights reserved.
 */
package com.uzhu.project.base;


import java.util.HashMap;

/**
 * 信息收集编号
 */
public class AppCollectCode {

    public static String kCollectNewerClick = "A001002";//首页点击新手标
    public static String kCollectMonthClick = "A001003";//首页点击月月升
    public static String kCollectInformationClick = "A001004";//首页点击资讯
    public static String kCollectLoanDetail = "A002001";//房租宝产品详情页进入x
    public static String kCollectLoanInvest = "A002002";//房租宝产品购买页进入x
    public static String kCollectRaiseDetail = "A002003";//月月升产品详情页进入x
    public static String kCollectRaiseInvest = "A002004";//月月升产品购买页进入x
    public static String kCollectNewerInvest = "A002005";//新手标产品购买页进入x
    public static String kCollectMessageList = "A003001";//站内信页面进入x
    public static String kCollectForgetPWD = "A003002";//忘记密码页面进入x
    public static String kCollectModifyPWD = "A003003";//修改密码面进入x
    public static HashMap pageCodeMap = new HashMap();

    public static class PageCode {
        public PageCode(String inCode, String outCode) {
            this.inCode = inCode;
            this.outCode = outCode;
        }

        public String inCode;
        public String outCode;
    }

    public static void initPageCodeMap() {
        if (!pageCodeMap.isEmpty()) {
            return;
        }
//        pageCodeMap.put(MainFragment.class.getName(), kCollectNewerClick);//首页点击新手标
//        pageCodeMap.put(MainFragment.class.getName(), kCollectMonthClick);//首页点击月月升
//        pageCodeMap.put(MainFragment.class.getName(), kCollectInformationClick);//首页点击资讯
    }
}
