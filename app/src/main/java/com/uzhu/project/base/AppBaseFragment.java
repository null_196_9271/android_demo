package com.uzhu.project.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.uzhu.project.R;
import com.uzhu.project.controller.inf.RefreshDataForFragment;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.view.base.AppTitle;

import java.util.ArrayList;
import java.util.Random;

import butterknife.ButterKnife;


@SuppressLint("NewApi")
public abstract class AppBaseFragment<T extends AppBaseCommonActivity> extends Fragment implements RefreshDataForFragment {
    private ArrayList<Integer> mBackgroundColors;
    private Random mRandom;
    protected T mActivity;
    protected static String TAG = "DefaultClassName";
    public AppServiceMediator mApi;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected AppTitle topTitle;
    protected FrameLayout llContent;
    protected FrameLayout bottomView;
    protected SwipeRefreshLayout srlLayout;
    protected AppViewModel mViewModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mActivity = (T) getActivity();
            mViewModel = mActivity.mViewModel;
        } catch (Exception e) {
            e.printStackTrace();
        }
        TAG = this.getClass().getName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mRandom = new Random();
        mBackgroundColors = new ArrayList<Integer>();
        mBackgroundColors.add(R.color.common_bg_color);
        mBackgroundColors.add(R.color.common_bg_color);
        mBackgroundColors.add(R.color.common_bg_color);
        mBackgroundColors.add(R.color.common_bg_color);
        mBackgroundColors.add(R.color.common_bg_color);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void alertMessage(String fieldName) {
        stopSrlLayout();
    }

    @Override
    public void refreshData(String fieldName) {
        stopSrlLayout();
    }

    private void stopSrlLayout() {
        srlLayout.setRefreshing(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        beforeCreateView();
        View view;
        if (!modContentView()) {
            view = View.inflate(mActivity, R.layout.common_activity, null);
            srlLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_layout);
            llContent = (FrameLayout) view.findViewById(R.id.ll_content);
            bottomView = (FrameLayout) view.findViewById(R.id.bottomView);
            topTitle = (AppTitle) view.findViewById(R.id.top_title);
            if (needRefresh()) {
                srlLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        onSRLayoutReFresh();
                    }
                });
            }
            initTitle();
            initBottomView();
            setShowView();
        } else {
            view = View.inflate(mActivity, createViewByLayoutId(), null);
            ButterKnife.bind(this, view);
        }
        if (isSetStateBar()) {
            setStateBar();
        }
        initID(view);
        initData();
        if (mApi == null) {
            mApi = AppServiceMediator.sharedInstance();
        }
        loadServiceData(mApi);
        initView(view);
        afterCreateView(view);
        return view;
    }

    private boolean modContentView() {
        return false;
    }

    protected boolean isSetStateBar() {
        return true;
    }

    //仅仅在安卓5.1以上开启 适配状态栏和title栏一样颜色 (国产机型rom更改过大,不一定完全实现)
    private void setStateBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mActivity.getWindow().setStatusBarColor(topTitle.getBackgroundColor());
        }
    }

    private void initBottomView() {
        if (getBottomView() != -1) {
            bottomView.addView(View.inflate(mActivity, getBottomView(), null));
        }

    }

    private void setShowView() {
        View view = View.inflate(mActivity, createViewByLayoutId(), null);
        ButterKnife.bind(this, view);
        llContent.addView(view);
        //解决srlLayout 里包含scrollview造成的滑动冲突
        final ScrollView scrollView = checkScrollView(view);
        if (scrollView != null) {
            scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged() {
                    if (srlLayout != null) {
                        srlLayout.setEnabled(scrollView.getScrollY() == 0);
                    }
                }
            });
        }
    }

    protected int getBottomView() {
        return -1;
    }

    private ScrollView checkScrollView(View view) {
        if (view instanceof ScrollView) {
            return (ScrollView) view;
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                ScrollView scrollView = checkScrollView(((ViewGroup) view).getChildAt(i));
                if (scrollView != null) {
                    return scrollView;
                }
            }
        }
        return null;
    }

    private void initTitle() {
        setAppTitle(topTitle);
    }

    protected void setAppTitle(AppTitle appTitle) {
        if (titleColor() != 0) {
            appTitle.setColor(titleColor());
        }
        if (!TextUtils.isEmpty(titleText())) {
            appTitle.setTitle(titleText());
        }
        if (isCanBack()) {
            appTitle.setBackIconListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
        }
    }

    protected int titleColor() {
        return R.color.colorPrimaryDark;
    }

    protected String titleText() {
        return null;
    }

    protected boolean isCanBack() {
        return true;
    }

    protected void onSRLayoutReFresh() {
        loadServiceData(mApi);
    }

    protected boolean needRefresh() {
        return false;
    }

    protected void loadServiceData(AppServiceMediator API) {

    }

    protected abstract void initView(View view);

    protected abstract void initData();

    protected void initID(View view) {

    }

    protected void afterCreateView(View view) {
    }

    protected void beforeCreateView() {
    }


    protected abstract int createViewByLayoutId();

    @Override
    public void onResume() {
        super.onResume();
        String className = getClass().getName();
        if (AppCollectCode.pageCodeMap.containsKey(className)) {
            MobclickAgent.onPageStart((String) AppCollectCode.pageCodeMap.get(className));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        String className = getClass().getName();
        if (AppCollectCode.pageCodeMap.containsKey(className)) {
            MobclickAgent.onPageEnd((String) AppCollectCode.pageCodeMap.get(className));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    /**
     * 随机背景色
     */
    public void randomBgColor(View layout) {
        int position = mRandom.nextInt(mBackgroundColors.size());
        // 随机颜色
        int backgroundIndex = position >= mBackgroundColors.size() ? position
                % mBackgroundColors.size() : position;
        layout.setBackgroundResource(mBackgroundColors.get(backgroundIndex));
    }

    /**
     * 长文本提示
     */
    public void showLongText(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    /**
     * 短文本提示
     */
    public void showShortText(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * 短文本提示
     */
    public void showShortText(int resId) {
        try {
            Toast.makeText(getActivity(), this.getResources().getString(resId),
                    Toast.LENGTH_SHORT).show();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 长文本提示
     */
    public void showLongText(int resId) {
        try {
            Toast.makeText(getActivity(), this.getResources().getString(resId),
                    Toast.LENGTH_LONG).show();
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Homeactivity中拦截下来的键盘事件，分发过来
     *
     * @param keyCode
     * @param event
     * @return
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }
}
