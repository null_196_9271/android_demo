/*
 * UserCenter.java
 *
 * Created by: shard on 2014-9-27
 * Copyright (c) 2014 shard. All rights reserved.
 */
package com.uzhu.project.base.cache;

import android.content.SharedPreferences.Editor;

import com.mvvm.framework.util.ENVConfig;
import com.mvvm.framework.util.LogUtil;
import com.mvvm.framework.util.PersistenceUtil;
import com.uzhu.project.service.models.User;

/**
 * manage user in usercenter
 */
public class UserCenter {

	// singleton
	private static UserCenter userCenter = null;
	// user info
	private static final String USER_INFO = "USER_INFO";
	// login account
	private static final String USER_ACCOUNT = "USER_ACCOUNT";
	// the user will modified
	private static final String MODIFY_USER = "MODIFY_USER";
	// address info
	private static final String ADDR_INFO = "ADDR_INFO";
	// is first login
	private static final String IS_FIRST_LOGIN = "IS_FIRST_LOGIN";
	// is shown guide page
	private static final String IS_SHOWN_GUIDE_PAGE = "IS_SHOWN_GUIDE_PAGE";

	// is null
	private static final String IS_NULL = "";

	private User user;
	private User modifyUser;
	// account
	private String userAccout;
	/** locationed city name */
	public String cityName;
	/** longitude */
	public double longitude;
	/** latitude */
	public double latitude;
	/** longitude of open app */
	public double openLongitude;
	/** latitude of open app */
	public double openLatitude;
	/** address of location */
	public String mapAddress;
	
	private UserCenter() {

	}

	public static UserCenter instance() {
		if (userCenter == null) {
			synchronized (UserCenter.class) {
				if (userCenter == null) {
					userCenter = new UserCenter();
				}
			}
		}
		return userCenter;
	}

	public boolean isLogin() {
		user = getUser();
		if (user == null || user.userToken == null || user.userToken.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public void setUser(User user) {
		this.user = user;
		if (null != user) {
			PersistenceUtil.saveObjectToSharePreferences(USER_INFO, user);
		} else {
			Editor editor = ENVConfig.sp.edit();
			editor.remove(USER_INFO);
			editor.commit();
		}
	}


	public User getUser() {
		if (user == null) {
			Object object = PersistenceUtil.getObjectFromSharePreferences(
					USER_INFO, User.class);
			if (object instanceof String) {
				String objString = (String) object;
				if (objString.equals(IS_NULL)) {
					LogUtil.d("userCenter", "获取用户信息为空！");
				} else {
					LogUtil.e("userCenter", "获取的用户信息为空！且有错误！空信息为：" + objString);
				}
				user = null;
			} else if (object instanceof User) {
				user = (User) object;
			}
		}
		return user;
	}

	public void setModifyUser(User user) {
		this.modifyUser = user;
		if (null != user) {
			PersistenceUtil.saveObjectToSharePreferences(MODIFY_USER, user);
		} else {
			Editor editor = ENVConfig.sp.edit();
			editor.remove(MODIFY_USER);
			editor.commit();
		}
	}

	public User getModifyUser() {
		if (modifyUser == null) {
			Object object = PersistenceUtil.getObjectFromSharePreferences(
					MODIFY_USER, User.class);
			if (object instanceof String) {
				String objString = (String) object;
				if (objString.equals(IS_NULL)) {
					LogUtil.d("userCenter", "获取用户信息为空！");
				} else {
					LogUtil.e("userCenter", "获取的用户信息为空！且有错误！空信息为：" + objString);
				}
				modifyUser = null;
			} else if (object instanceof User) {
				modifyUser = (User) object;
			}
		}
		return modifyUser;
	}

	public String getUserAccout() {
		if (userAccout == null) {
			String object = PersistenceUtil.getObjectFromSharePreferences(
					USER_ACCOUNT, String.class);
			if (object instanceof String) {
				userAccout = (String) object;
			}
		}
		return userAccout;
	}

	public void setUserAccount(String account) {
		this.userAccout = account;
		if (null != userAccout && userAccout.length() > 0) {
			PersistenceUtil.saveObjectToSharePreferences(USER_ACCOUNT, userAccout);
		} else {
			Editor editor = ENVConfig.sp.edit();
			editor.remove(USER_ACCOUNT);
			editor.commit();
		}
	}

	public static void setCityPosition(int position) {
		if (position >= 0) {
			Editor edit = ENVConfig.sp.edit();
			edit.putInt("cityPosition", position);
			edit.commit();
		} else {
			Editor edit = ENVConfig.sp.edit();
			edit.putInt("cityPosition", 0);
			edit.commit();
		}
	}

	public static int getCityPosition() {
		int position = 0;
		position = ENVConfig.sp.getInt("cityPosition", 0);
		return position;
	}

	public static void setCityId(int cityId) {
		if (cityId >= 0) {
			Editor edit = ENVConfig.sp.edit();
			edit.putInt("cityId", cityId);
			edit.commit();
		} else {
			Editor edit = ENVConfig.sp.edit();
			edit.putInt("cityId", 110100);
			edit.commit();
		}
	}

	public static int getCityId() {
		int position = 0;
		position = ENVConfig.sp.getInt("cityId", 110100);
		return position;
	}

	public void setAPPSize(long size) {
		if (size >= 1) {
			Editor edit = ENVConfig.sp.edit();
			edit.putLong("appSize", size);
			edit.commit();
		} else {
			Editor edit = ENVConfig.sp.edit();
			edit.putInt("appSize", 0);
			edit.commit();
		}
	}

	public static int getAppSize() {
		int size = 0;
		size = ENVConfig.sp.getInt("appSize", 0);
		return size;
	}

	public void setIsFirstLogin(boolean isFirstLogin) {
		Editor edit = ENVConfig.sp.edit();
		edit.putBoolean(IS_FIRST_LOGIN, isFirstLogin);
		edit.commit();
	}

	public static boolean getIsFirstLogin() {
		boolean hasOnceLogined = ENVConfig.sp.getBoolean(IS_FIRST_LOGIN, true);
		return hasOnceLogined;
	}

	public void setIsShownGuidePage(boolean isShownGuidePage) {
		Editor edit = ENVConfig.sp.edit();
		edit.putBoolean(IS_SHOWN_GUIDE_PAGE, isShownGuidePage);
		edit.commit();
	}

	public static boolean getIsShownGuidePage() {
		boolean hasOnceLogined = ENVConfig.sp.getBoolean(IS_SHOWN_GUIDE_PAGE,
				false);
		return hasOnceLogined;
	}

	public void reset() {
		this.setUser(null);
		this.setAPPSize(0);
		setCityId(-1);
		setCityPosition(-1);
	}

}
