package com.uzhu.project.base;

import android.content.Context;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.uzhu.project.R;


/**
 * Created by shard on 16/6/16.
 */
public class WXManager {
	private static WXManager INSTANCE;
	private static final int TIMELINE_SUPPORTED_VERSION = 0x21020001;
	private IWXAPI api;
	private Context mContext;

	private WXManager() {

	}

	public static WXManager getInstance() {
		if (INSTANCE == null) {
			synchronized (WXManager.class) {
				if (INSTANCE == null) {
					INSTANCE = new WXManager();
				}
			}
		}
		return INSTANCE;
	}

	public void init(Context context) {
		mContext = context;
		regToWX();
	}

	private void regToWX() {
		String APP_ID = getAPPID();
		//通过WXAPIFactory工厂，获取IWXAPI的实例
		api = WXAPIFactory.createWXAPI(AppApplication.getContext(), APP_ID);
		//将应用的appId注册到微信
		api.registerApp(APP_ID);
	}

	public String getAPPID() {
		return mContext.getResources().getString(R.string.WX_APPID);
	}

	public IWXAPI getApi() {
		return api;
	}

	public boolean isSupportTimeLine() {
		int wxSdkVersion = api.getWXAppSupportAPI();
		return (wxSdkVersion >= TIMELINE_SUPPORTED_VERSION);
	}
}
