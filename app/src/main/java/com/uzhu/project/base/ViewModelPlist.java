/*
 * ViewModelPlist.java
 *
 * Created by: shard on 2014-9-27
 * Copyright (c) 2014 shard. All rights reserved.
 */
package com.uzhu.project.base;

import com.mvvm.framework.viewModel.ViewModelManager;
import com.uzhu.project.controller.activity.TestCircularButtonActivity;
import com.uzhu.project.controller.activity.TestFragmentActivity;
import com.uzhu.project.controller.activity.TestListViewActivity;
import com.uzhu.project.controller.activity.TestRecyclerViewActivity;
import com.uzhu.project.controller.activity.common.LoginActivity;
import com.uzhu.project.controller.viewmodel.LoginViewModel;
import com.uzhu.project.controller.viewmodel.TestCircularButtonActivityViewModel;
import com.uzhu.project.controller.viewmodel.TestFragmentActivityViewModel;
import com.uzhu.project.controller.viewmodel.TestListViewActivityViewModel;
import com.uzhu.project.controller.viewmodel.TestRecyclerViewActivityViewModel;

import java.util.HashMap;

/**
 * create relationship of mViewModel and activity
 */
public class ViewModelPlist {
    public static HashMap<String, String> hashMap = new HashMap<String, String>();

    public static void init() {
        if (ViewModelManager.viewModelPlist.isEmpty()) {
            ViewModelManager.viewModelPlist.putAll(hashMap);
        }
    }

    static {
        hashMap.put(LoginActivity.class.getName(), LoginViewModel.class.getName());
        hashMap.put(TestListViewActivity.class.getName(), TestListViewActivityViewModel.class.getName());
        hashMap.put(TestFragmentActivity.class.getName(), TestFragmentActivityViewModel.class.getName());
        hashMap.put(TestRecyclerViewActivity.class.getName(), TestRecyclerViewActivityViewModel.class.getName());
        hashMap.put(TestCircularButtonActivity.class.getName(), TestCircularButtonActivityViewModel.class.getName());
    }
}
