package com.uzhu.project.base;

import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.R;
import com.uzhu.project.adapter.abslistview.base.CommonAdapter;
import com.uzhu.project.adapter.abslistview.base.ViewHolder;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.widget.xlistview.XListView;

import java.util.List;


/**
 * Created by John on 2016/12/15 0015.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 10:09
 * UpdateDescribe
 */
public abstract class AppBaseListActivity<T extends AppViewModel, V> extends AppBaseCommonActivity<T> {
    protected Page mPage;
    protected Params mParams;

    public XListView getListView() {
        return mLv_show;
    }

    protected XListView mLv_show;
    protected List<V> mData;
    protected LinearLayout mLl_null;
    protected ImageView mIv_base_list_null;
    protected TextView mTv_base_list_null_text;

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_base_list;
    }

    @Override
    protected void initData() {
        if (mData == null) {
            mData = mDataList(mViewModel);
        }
        mPage = new Page();
        mParams = new Params();
        setMyParams(mParams);
    }

    protected abstract List<V> mDataList(T viewModel);

    protected void setMyParams(Params params) {
    }

    @Override
    protected void loadServiceData(AppServiceMediator API) {
        super.loadServiceData(API);
        loadServiceAPI(API, mPage, mParams);
    }

    protected abstract void loadServiceAPI(AppServiceMediator appServiceMediator, Page page, Params params);

    @Override
    protected void initView() {
        mIv_base_list_null.setImageDrawable(getResources().getDrawable(getNullImageViewID()));
        mTv_base_list_null_text.setText(getNullText());
        mLl_null.setVisibility(View.GONE);
        mLv_show.setPullRefreshEnable(false);
        mLv_show.setPullLoadEnable(true);
        setHeadOrBottomListView(mLv_show);
        mLv_show.setAdapter(new CommonAdapter<V>(getContextByThis(), itemViewLayout(), mData) {
            @Override
            protected void convert(ViewHolder viewHolder, V item, int position) {
                setItemView(viewHolder, item, position);
            }
        });
        mLv_show.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
                mPage.pageIndex = mPage.pageIndex.intValue() + 1;
                loadServiceData(AppServiceMediator.sharedInstance());
                onListViewLoadMore();
            }
        });
        mLv_show.setOnScrollListener(new XListView.OnXScrollListener() {
            @Override
            public void onXScrolling(View view) {

            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                LogUtil.d(TAG, firstVisibleItem + "");
                srlLayout.setEnabled(firstVisibleItem == 0);

            }
        });

    }

    protected void onListViewLoadMore() {

    }

    @Override
    protected boolean needRefresh() {
        return true;
    }

    @Override
    protected void onSRLayoutReFresh() {
        super.onSRLayoutReFresh();
        initData();
        loadServiceData(AppServiceMediator.sharedInstance());
    }

    protected CharSequence getNullText() {
        return "暂时没有更多信息";
    }

    protected int getNullImageViewID() {
        return R.mipmap.null_record;
    }

    protected abstract void setItemView(ViewHolder viewHolder, V item, int position);

    protected abstract int itemViewLayout();

    protected void setHeadOrBottomListView(XListView lv_show) {

    }

    @Override
    public void refreshData(String fieldName) {
        super.refreshData(fieldName);
        if (mData.size() == 0) {
            mLl_null.setVisibility(View.VISIBLE);
        } else {
            mLl_null.setVisibility(View.GONE);
        }
        mLv_show.setPullLoadEnable(mData.size() >= CommonConst.PAGE_SIZE);
        mLv_show.getBaseAdapter().notifyDataSetChanged();
        mLv_show.stopLoadMore();
        mLv_show.stopRefresh();
    }

    @Override
    public void alertMessage(String fieldName, int errorCode, String errorMsg) {
        mLv_show.stopLoadMore();
        mLv_show.stopRefresh();
        super.alertMessage(fieldName, errorCode, errorMsg);
    }

    @Override
    protected void initID() {
        mLv_show = (XListView) findViewById(R.id.lv_msg);
        mLl_null = (LinearLayout) findViewById(R.id.ll_null);
        mIv_base_list_null = (ImageView) findViewById(R.id.iv_base_list_null);
        mTv_base_list_null_text = (TextView) findViewById(R.id.tv_base_list_null_text);
    }
}
