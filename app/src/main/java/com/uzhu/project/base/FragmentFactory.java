package com.uzhu.project.base;

import android.support.v4.app.Fragment;

import com.uzhu.project.controller.fragment.TestFragment;
import com.uzhu.project.controller.fragment.TestListFragment;

/**
 * Created by John on 2016/12/5 0005.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 17:03
 * UpdateDescribe
 */

public class FragmentFactory {
    public static Fragment createForMain(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:// 主页
                fragment = new TestFragment();
                break;
            case 1:// 主页
                fragment = new TestListFragment();
                break;
            case 2:// 主页
                fragment = new TestFragment();
                break;

        }
        return fragment;
    }


}
