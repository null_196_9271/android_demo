package com.uzhu.project.base;

import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.R;
import com.uzhu.project.adapter.abslistview.base.CommonAdapter;
import com.uzhu.project.adapter.abslistview.base.ViewHolder;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.widget.xlistview.XListView;

import java.util.List;

import butterknife.Bind;

/**
 * Created by John on 2017/2/10 0010.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 15:29
 * UpdateDescribe
 */
public abstract class AppBaseListFragment<T extends AppBaseCommonActivity<K>,K extends AppViewModel,V> extends AppBaseFragment<T> {
    protected Page mPage;
    protected Params mParams;

    public XListView getListView() {
        return mLv_show;
    }
    protected List<V> mData;
    @Bind(R.id.lv_msg)
    protected XListView mLv_show;
    @Bind(R.id.iv_base_list_null)
    protected ImageView mIv_base_list_null;
    @Bind(R.id.tv_base_list_null_text)
    protected TextView mTv_base_list_null_text;
    @Bind(R.id.ll_null)
    protected  LinearLayout mLl_null;



    @Override
    protected void initData() {
        if (mData == null) {
            mData = mDataList(mActivity.mViewModel);
        }
        mPage = new Page();
        mParams = new Params();
        setMyParams(mParams);
    }
    protected void setMyParams(Params params) {
    }
    protected abstract List<V> mDataList(K viewModel);
    @Override
    protected int createViewByLayoutId() {
        return R.layout.activity_base_list;
    }
    @Override
    protected void loadServiceData(AppServiceMediator API) {
        super.loadServiceData(API);
        loadServiceAPI(API, mPage, mParams);
    }

    protected abstract void loadServiceAPI(AppServiceMediator appServiceMediator, Page page, Params params);

    @Override
    protected void initView(View view) {
        mIv_base_list_null.setImageDrawable(getResources().getDrawable(getNullImageViewID()));
        mTv_base_list_null_text.setText(getNullText());
        mLl_null.setVisibility(View.GONE);
        mLv_show.setPullRefreshEnable(false);
        mLv_show.setPullLoadEnable(true);
        setHeadOrBottomListView(mLv_show);
        mLv_show.setAdapter(new CommonAdapter<V>(mActivity, itemViewLayout(), mData) {
            @Override
            protected void convert(ViewHolder viewHolder, V item, int position) {
                setItemView(viewHolder, item, position);
            }
        });
        mLv_show.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh() {

            }

            @Override
            public void onLoadMore() {
                mPage.pageIndex = mPage.pageIndex.intValue() + 1;
                loadServiceData(AppServiceMediator.sharedInstance());
                onListViewLoadMore();
            }
        });
        mLv_show.setOnScrollListener(new XListView.OnXScrollListener() {
            @Override
            public void onXScrolling(View view) {

            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                LogUtil.d(TAG, firstVisibleItem + "");
                srlLayout.setEnabled(firstVisibleItem == 0);

            }
        });
    }
    protected void onListViewLoadMore() {

    }
    @Override
    protected boolean needRefresh() {
        return true;
    }

    @Override
    protected void onSRLayoutReFresh() {
        super.onSRLayoutReFresh();
        initData();
        loadServiceData(AppServiceMediator.sharedInstance());
        onListViewRefresh();
    }

    private void onListViewRefresh() {
    }

    protected CharSequence getNullText() {
        return "暂时没有更多信息";
    }

    protected int getNullImageViewID() {
        return R.mipmap.null_record;
    }

    protected abstract void setItemView(ViewHolder viewHolder, V item, int position);

    protected abstract int itemViewLayout();

    protected void setHeadOrBottomListView(XListView lv_show) {

    }
}
