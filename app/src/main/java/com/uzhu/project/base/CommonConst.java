package com.uzhu.project.base;

public interface CommonConst {
	public static final String INTENT_ACTION_MIPUSH = "myintent.action.mipush";

	public static final int ERROR_CODE_TOKEN_INVALID = 15030003;

	public static final int REGISTER = 1000; // 注册
	public static final int REGISTER_SUCCESS = 1001; // 注册成功
	public static final int REGISTER_FAILED = 1002; // 注册失败
	public static final int LOGIN = 2000; // 登录
	public static final int LOGIN_SUCCESS = 2001; // 登录成功
	public static final int LOGIN_FAILED = 2002; // 登录失败
	public static final int RESET_PWD = 3000; // 重置密码
	public static final int RESET_PWD_SUCCESS = 3001; // 重置密码成功
	public static final int RESET_PWD_FAILED = 3002; // 重置密码失败
	public static final int CHANGE_INFO = 4001; // 修改信息
	public static final int LOGOUT = 5001; // 登出
	public static final int ACCEPT = 6001; // 同意协议

	public static final int BILL_DETAIL = 4000;//账单详情

	/* 本地文件配置 */
	public static final String ABOUT_UZHU = "file:///android_asset/about.html"; // 关于优住
	public static final String REGISTER_PROTOCOL = "file:///android_asset/register_protocol.html"; // 注册协议
	public static final String AGREEMENT = "file:///android_asset/agreement.html"; // 关于优住

	/**
	 * 默认分页数据
	 */
	public static final int PAGE_SIZE = 10;
	public static final int PAGE_INDEX = 1;

	/* 网页配置 */
	public static final String PUBLICIZE_URL = "http://www.uzhujf.com/publicitypage/about.html"; // 优住宣传页
	public static final String ABOUT_URL = "http://x.eqxiu.com/s/KLv3ODHN"; // 关于优住
	public static final String CREDIT_POINT_URL = "http://q.eqxiu.com/s/8c2Erths"; // 信用分说明

	/* 头像名称 */
	public static final String PHOTO_FILE_NAME = "temp_photo.jpg";
	public static final int PHOTO_REQUEST_CAMERA = 100;// 拍照
	public static final int PHOTO_REQUEST_GALLERY = 200;// 从相册中选择
	public static final int PHOTO_REQUEST_CUT = 300;// 结果

	public static final String FORCE_UPDATE = "1";

	/**
	 * 验证码类型
	 */
	public static final String SMSCodeTypeRegister = "RT"; //注册
	public static final String SMSCodeTypeRegister1 = "NC"; //注册
	public static final String SMSCodeTypeResetPwsd = "RP"; //重置密码
	public static final String SMSCodeTypeVerifyMobile = "VM"; //校验原手机号
	public static final String SMSCodeTypeChangeMobile = "CM"; //修改手机号
	public static final String SMSCodeTypeLogin = "LT"; //登录

	/**
	 * 支付类型
	 */
	public static final String PayTypeAlipay = "alipay"; //支付宝支付
	public static final String PayTypeWX = "weixin"; //微信支付

	/**
	 * 业主状态
	 */
	public static final String OwnerStatusDefault = "0"; //未留资
	public static final String OwnerStatusUnApplied = "1"; //已留资
	public static final String OwnerStatusApplied = "2"; //已贷款

	/**
	 * 房间绑定状态
	 */
	public static final String UnbindRoom = "0"; //未绑定
	public static final String BoundRoom = "1"; //已绑定

	/**
	 * 合同状态
	 */
	public static final String ContractStatusALL = "ALL"; //全部
	public static final String ContractStatusCKD = "CKD"; //入住中
	public static final String ContractStatusRET = "RET"; //中途退租
	public static final String ContractStatusDON = "DON"; //正常退租
	public static final String ContractStatusREL = "REL"; //续租退租

	public static final String RentalStatusPAY = "PAY"; //已支付
	public static final String RentalStatusUNPAY = "UNPAY"; //未支付
	public static final String RentalStatusDUE = "DUE"; //拖欠
	public static final String RentalStatusDBL = "DBL"; //已关闭

	/**
	 * 修改用户信息类型
	 */
	public static final String ModTypeAvatar = "1";		//改头像
	public static final String ModTypeNickName = "2";	//改昵称
	public static final String ModTypeIdCard = "3";	//改证件号

	/**
	 * 订单详细类型
	 */
	public static final String OrderDetailTypeTenement = "TENEMENT";	//租金订单
	public static final String OrderDetailTypeDeposit = "DEPOSIT";	//押金订单
	public static final String OrderDetailTypeUtilities = "UTILITIES";	//水电订单
	public static final String OrderDetailTypeManage = "MANAGE";	//管理费订单
	public static final String OrderDetailTypeSundries = "SUNDRIES";	//杂费订单
	public static final String OrderDetailTypeElectric = "ELECTRIC";	//电费订单
	public static final String OrderDetailTypeWater = "WATER";	//水费订单
	public static final String OrderDetailTypeParking = "PARKING";	//停车费订单
	public static final String OrderDetailTypeHotWater = "HOTWATER";	//热水费订单

	/**
	 * 订单状态
	 */
	public static final String OrderStatusAll = "ALL";	//所有订单
	public static final String OrderStatusPay = "PAY";	//已支付订单
	public static final String OrderStatusUnpay = "UNPAY";	//未支付订单

	/**
	 * 期限类型
	 */
	public static final String PERIOD_TYPE_DAY = "DAY";
	public static final String PERIOD_TYPE_WEEK = "WEEK";
	public static final String PERIOD_TYPE_MONTH = "MONTH";
	public static final String PERIOD_TYPE_YEAR = "YEAR";

	/**
	 * 协议状态
	 */
	public static final String AGREEMENT_AGREE = "AGREE";
	public static final String AGREEMENT_REJECT = "REJECT";

	/**
	 *  新浪操作类型
	 */
	public static final Integer kSinaOperationTypeInvest = 1; //新浪投资
	public static final Integer kSinaOperationTypeRecharge = 2; //新浪充值
	public static final Integer kSinaOperationTypeWithdraw = 3; //新浪提现
	public static final Integer kSinaOperationTypeSetPwd = 4; //新浪设置密码
	public static final Integer kSinaOperationTypeModifyPwd = 5; //新浪修改密码
}
