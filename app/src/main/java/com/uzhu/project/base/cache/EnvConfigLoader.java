package com.uzhu.project.base.cache;


import android.content.Context;
import android.util.Xml;

import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.service.models.EnvConfing;
import com.uzhu.project.service.models.Environment;
import com.uzhu.project.util.SaxEnvConfigXml;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class EnvConfigLoader {
    public static String TAG = "EnvConfigLoader";
    public static String FileName = "environment_config.xml";

    private static Properties prop;
    public static Context mContext;


    public static Environment init(Context context) {
        EnvConfigLoader.mContext = context;
        copyConfig(context, FileName);
        return readEnvConfigXML(context);
    }

    /**
     * 拷贝资产目录下的数据库文件
     */
    private static void copyConfig(Context context, final String filename) {
        File file = new File(EnvConfigLoader.mContext.getFilesDir(), filename);
        InputStream is = null;
        FileOutputStream fos = null;
        if (file.exists()) { // zcj 每次都复制
            // if (false ){
            LogUtil.i(TAG, "配置文件已经存在,无需拷贝");
            return;
        } else {
            LogUtil.i(TAG, "读取默认位置配置文件");
            try {
//                is = ClientLoader.class.getClassLoader().getResourceAsStream(
//                        filename);
                is = context.getResources().getAssets().open(filename);
                File config = new File(EnvConfigLoader.mContext.getFilesDir(),
                        filename);

                fos = new FileOutputStream(config);
                byte[] buffer = new byte[1024];
                int len = 0;
                while ((len = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static Environment readEnvConfigXML(Context context) {
        //创建sax解析
        SAXParserFactory saxParseFac;
        SaxEnvConfigXml handler;
        Environment environment = new Environment();
        try {
//            InputStream input = context.getResources().openRawResource(R.raw.environment_config);
            InputStream input = EnvConfigLoader.mContext.openFileInput(FileName);
//            File destFile = new File(path + "/environment_config");
//            InputStream fis = new FileInputStream(destFile);
            saxParseFac = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParseFac.newSAXParser();
            handler = new SaxEnvConfigXml();
            //解析xml文件
            saxParser.parse(input, handler);
            input.close();
            environment = handler.getEnvironment();
        } catch (Exception e) {
            LogUtil.e(TAG, "xml解析异常！-- " + e.getMessage());
//            context.showToast("xml解析异常！-- "+e.getMessage());
        }
        return environment;
    }


    public static boolean writeEnvConfigXml(Environment environment) {
        boolean falg = false;
        final String enter = System.getProperty("line.separator");//换行
        try {
            File file = new File(EnvConfigLoader.mContext.getFilesDir(), FileName);
            FileOutputStream fos = new FileOutputStream(file);
            XmlSerializer serializer = Xml.newSerializer();
            serializer.setOutput(fos, "utf-8");
            serializer.startDocument("utf-8", true);
            changeLine(serializer, enter);
            serializer.startTag(null, "xml");
            changeLine(serializer, enter);
            //当前环境
            serializer.startTag(null, "Environment");
            serializer.text(environment.getCurrEnvironment());
            serializer.endTag(null, "Environment");
            changeLine(serializer, enter);
            //ServerURL
            serializer.startTag(null, "ServerURL");
            changeLine(serializer, enter);
            for (EnvConfing confing : environment.getServerURLs()) {
                serializer.startTag(null, confing.getConfigTitle());
                serializer.text(confing.getConfigValue());
                serializer.endTag(null, confing.getConfigTitle());
                changeLine(serializer, enter);
            }
            serializer.endTag(null, "ServerURL");
            changeLine(serializer, enter);
            //SecurityURL
            serializer.startTag(null, "SecurityURL");
            changeLine(serializer, enter);
            for (EnvConfing confing : environment.getSecurityURLs()) {
                serializer.startTag(null, confing.getConfigTitle());
                serializer.text(confing.getConfigValue());
                serializer.endTag(null, confing.getConfigTitle());
                changeLine(serializer, enter);
            }
            serializer.endTag(null, "SecurityURL");
            changeLine(serializer, enter);
            //ImageURL
            serializer.startTag(null, "ImageURL");
            changeLine(serializer, enter);
            for (EnvConfing confing : environment.getImageURLs()) {
                serializer.startTag(null, confing.getConfigTitle());
                serializer.text(confing.getConfigValue());
                serializer.endTag(null, confing.getConfigTitle());
                changeLine(serializer, enter);
            }
            serializer.endTag(null, "ImageURL");
            changeLine(serializer, enter);
            //MessageCollectorServerURL
            serializer.startTag(null, "MessageCollectorServerURL");
            changeLine(serializer, enter);
            for (EnvConfing confing : environment.getMessageCollectorServerURLs()) {
                serializer.startTag(null, confing.getConfigTitle());
                serializer.text(confing.getConfigValue());
                serializer.endTag(null, confing.getConfigTitle());
                changeLine(serializer, enter);
            }
            serializer.endTag(null, "MessageCollectorServerURL");
            changeLine(serializer, enter);
            serializer.endTag(null, "xml");
            changeLine(serializer, enter);
            serializer.endDocument();
            fos.close();
            falg = true;
        } catch (Exception e) {
            e.printStackTrace();
            falg = false;
        }

        return falg;
    }


    public static void changeLine(XmlSerializer serializer, String enter) {
        try {
            serializer.text(enter);
        } catch (IOException e) {
            LogUtil.e(TAG, e.getMessage());
        }
    }
}
