package com.uzhu.project.base;

import android.content.Context;

import com.uzhu.project.util.NetworkUtil;


/**
 * Created by hainiao on 2015/12/3.
 */
public class DataManager {
	private static DataManager INSTANCE;

	private DataManager() {

	}

	public static DataManager getInstance() {
		if (INSTANCE == null) {
			synchronized (DataManager.class) {
				if (INSTANCE == null) {
					INSTANCE = new DataManager();
				}
			}
		}
		return INSTANCE;
	}

	public void initData(Context context) {
		if (NetworkUtil.checkNetwork(context)) {
		}
	}
}
