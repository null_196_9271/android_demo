/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: TenementApplication.java
 * Author:   jeremy
 * Date:     2015-10-15 09:58:11
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-15 09:58:11> <version>   <desc>
 */
package com.uzhu.project.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.alipay.euler.andfix.patch.PatchManager;
import com.lzy.okhttputils.OkHttpUtils;
import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.service.task.NoBlockExecuteListener;
import com.squareup.leakcanary.LeakCanary;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.Version;
import com.uzhu.project.util.AppConfig;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class AppApplication extends Application {
    private static AppApplication application;
    private static Context mContext;
    //开发（开关）[异常收集、环境配置、log日志输出]
    // 【true=开发测试】【false=上线发布】
    private boolean isDeveloping = true;
    public static final String CURRENT_ENV = "SIT7"; //当前模式
    public AppConfig mExampleConfig;
    public static PatchManager mPatchManager;
    public Version mVersion;

    public static AppApplication getInstance() {
        if (null == application) {
            application = new AppApplication();
        }
        return application;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        mExampleConfig = new AppConfig(
                mContext);
        mExampleConfig.projectConfig();
        System.out.println(getDeviceInfo(mContext));
        //下载splash图片
        getSplashImage();
        // TODO: 2017/3/1 0001 新添加内容

        //okhttpUtils初始化(用于网络下载,等特定情况)
        OkHttpUtils.init(this);
        //开发模式开启内存泄漏检测
        if (isDeveloping()) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);
        }
        //初始化热修复相关(可以实现线上紧急修改bug类,仅仅支持修改,不能增加或删除,需要服务端配置地址)
        initPatch();
        //初始化异常捕捉(可以实现程序异常崩溃时候记录日志到sd卡,后续提示或者自动发送日志到服务器&(未设置服务器,未确定提示界面))
        //NewCrashHandler.getInstance().init(this);
    }

    private void initPatch() {
        // 初始化patch管理类
        mPatchManager = new PatchManager(this);
        // 初始化patch版本
        String appversion = null;
        try {
            appversion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        mPatchManager.init(appversion);
        // 加载已经添加到PatchManager中的patch
        mPatchManager.loadPatch();
    }

    private void getSplashImage() {
        DisplayMetrics metric = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metric);
        int widthPixels = metric.widthPixels;
        int heightPixels = metric.heightPixels;
        ServiceFuture serviceFuture = AppServiceMediator.sharedInstance().splashImage(widthPixels, heightPixels);
        serviceFuture.addServiceListener("splashimage", new NoBlockExecuteListener() {
            @Override
            public void onExecuteSuccess(String tag, Object result) {
                final String imagerUrl = (String) result;
                TimerTask task = new TimerTask() {
                    public void run() {
                        downLoadSplashImage(imagerUrl);
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, 3000);

            }
        });
    }

    public static Context getContext() {
        return mContext;
    }

    public static Resources gotResources() {
        return mContext.getResources();
    }

    public void applicationExit() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public boolean isDeveloping() {
        return isDeveloping;
    }

    public void setIsDeveloping(boolean isDeveloping) {
        this.isDeveloping = isDeveloping;
    }

    /**
     * 获取当前渠道号
     */
    public String getCurrentChannel() {
        String channel = "";
        try {
            PackageManager packageManager = mContext.getPackageManager();
            ApplicationInfo applicationInfo = packageManager
                    .getApplicationInfo(mContext.getPackageName(),
                            PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            if (bundle != null) {
                channel = bundle.getString("CURRENT_CHANNEL");
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return channel;
    }

    private boolean checked = false;

    /**
     * 检查版本升级，判断如果已经检查过就不再检查
     */
    public void checkVersion() {
        if (checked) {
            return;
        }
        checked = true;
        ServiceFuture serviceFuture = AppServiceMediator.sharedInstance().isVersionUpdate();
        serviceFuture.addServiceListener("", new NoBlockExecuteListener() {
            @Override
            public void onExecuteSuccess(String s, Object o) {
                if (o == null || !(o instanceof Version)) {
                    return;
                }
                mVersion = (Version) o;
                EventBus.getDefault().post(mVersion);
            }
        });
    }


    public static boolean checkPermission(Context context, String permission) {
        boolean result = false;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                Class clazz = Class.forName("android.content.Context");
                Method method = clazz.getMethod("checkSelfPermission", String.class);
                int rest = (Integer) method.invoke(context, permission);
                if (rest == PackageManager.PERMISSION_GRANTED) {
                    result = true;
                } else {
                    result = false;
                }
            } catch (Exception e) {
                result = false;
            }
        } else {
            PackageManager pm = context.getPackageManager();
            if (pm.checkPermission(permission, context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
                result = true;
            }
        }
        return result;
    }

    public static String getDeviceInfo(Context context) {
        try {
            org.json.JSONObject json = new org.json.JSONObject();
            android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String device_id = null;
            if (checkPermission(context, Manifest.permission.READ_PHONE_STATE)) {
                device_id = tm.getDeviceId();
            }
            String mac = null;
            FileReader fstream = null;
            try {
                fstream = new FileReader("/sys/class/net/wlan0/address");
            } catch (FileNotFoundException e) {
                fstream = new FileReader("/sys/class/net/eth0/address");
            }
            BufferedReader in = null;
            if (fstream != null) {
                try {
                    in = new BufferedReader(fstream, 1024);
                    mac = in.readLine();
                } catch (IOException e) {
                } finally {
                    if (fstream != null) {
                        try {
                            fstream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            json.put("mac", mac);
            if (TextUtils.isEmpty(device_id)) {
                device_id = mac;
            }
            if (TextUtils.isEmpty(device_id)) {
                device_id = android.provider.Settings.Secure.getString(context.getContentResolver(),
                        android.provider.Settings.Secure.ANDROID_ID);
            }
            json.put("device_id", device_id);
            return json.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void downLoadSplashImage(String splash) {
        try {
            download(splash);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载方法
     *
     * @param path
     * @throws Exception
     */
    public void download(String path) throws Exception {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            int total = conn.getContentLength();
            // 获取到文件的大小
            InputStream is = conn.getInputStream();
            File dir = creatSDDir("ishouzu");
            File file = new File(dir,
                    "splash.png");
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(file);
            BufferedInputStream bis = new BufferedInputStream(is);
            byte[] buffer = new byte[1024 * 10];
            int len;
            int temp = 0;
            while ((len = bis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
                temp += len;
                final int i = (temp * 100) / total;

            }
            fos.close();
            bis.close();
            is.close();

        }
    }

    /*
* 在SD卡上创建目录
*/
    public File creatSDDir(String dirName) {
        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + dirName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    /**
     * 存放activity的列表
     */
    public HashMap<Class<?>, Activity> activities = new LinkedHashMap<>();

    /**
     * 添加Activity
     *
     * @param activity
     */
    public void addActivity(Activity activity, Class<?> clz) {
        activities.put(clz, activity);
    }

    /**
     * 判断一个Activity 是否存在
     *
     * @param clz
     * @return
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public <T extends Activity> boolean isActivityExist(Class<T> clz) {
        boolean res;
        Activity activity = getActivity(clz);
        if (activity == null) {
            res = false;
        } else {
            if (activity.isFinishing() || activity.isDestroyed()) {
                res = false;
            } else {
                res = true;
            }
        }

        return res;
    }

    /**
     * 获得指定activity实例
     *
     * @param clazz Activity 的类对象
     * @return
     */
    public <T extends Activity> T getActivity(Class<T> clazz) {
        return (T) activities.get(clazz);
    }

    /**
     * 移除activity,代替finish
     *
     * @param activity
     */
    public void removeActivity(Activity activity) {
        if (activities.containsValue(activity)) {
            activities.remove(activity.getClass());
        }
    }

    /**
     * 移除所有的Activity
     */
    public void removeAllActivity() {
        if (activities != null && activities.size() > 0) {
            Set<Map.Entry<Class<?>, Activity>> sets = activities.entrySet();
            for (Map.Entry<Class<?>, Activity> s : sets) {
                if (!s.getValue().isFinishing()) {
                    s.getValue().finish();
                }
            }
        }
        activities.clear();
    }
}
