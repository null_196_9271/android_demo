package com.uzhu.project.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.uzhu.project.widget.dialog.CustomDialog;

import java.util.List;


public class UiUtil {

	/**
	 * dip-->px sualizerView.setLayoutParams(new ViewGroup.LayoutParams(
	 * ViewGroup.LayoutParams.FILL_PARENT, (int) (VISUALIZER_HEIGHT_DIP *
	 * getResources() .getDisplayMetrics().density)));
	 *
	 * @param context
	 * @param dp
	 * @return
	 */
//	public static int dip2px(Context context, float dip) {
//		final float scale = context.getResources().getDisplayMetrics().density;
//		return (int) (dip * scale + 0.5f);
//	}

	public static int dp2px(Context context, float dp) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
	}

	public static int px2sp(Context context, float pxValue) {
		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (pxValue / fontScale + 0.5f);
	}

	public static int sp2px(Context context, float spValue) {
//		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spValue, displayMetrics);
//		return (int) (spValue * fontScale + 0.5f);
	}

	/**
	 * 关闭键盘事件
	 *
	 * @author shimiso
	 * @update 2012-7-4 下午2:34:34
	 */
	public static void closeInput(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (inputMethodManager != null && activity.getCurrentFocus() != null) {
			inputMethodManager.hideSoftInputFromWindow(activity
							.getCurrentFocus().getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	public static void showInput(EditText editText) {
		InputMethodManager imm = (InputMethodManager) editText.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
	}

	// 旋转动画
	public static void startRotateAnimation(final View view) {
		RotateAnimation rotateAnimation = new RotateAnimation(0f, 360f,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f);
		rotateAnimation.setDuration(3000);
		rotateAnimation.setRepeatCount(-1);
		LinearInterpolator lin = new LinearInterpolator();
		rotateAnimation.setInterpolator(lin);
		view.startAnimation(rotateAnimation);
	}

	// 旋转动画
	public static void stopRotateAnimation(final View view) {
		if (view.getAnimation() != null) {
			view.getAnimation().cancel();
		}
	}

	public static boolean isAppInForeground(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				return appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
			}
		}
		return false;
	}


	private static Dialog confirmDialog;

	/**
	 * 定位开关关闭状态提示是否开启
	 */
	public static void promptOpenProvider(final Activity activity) {
		CustomDialog.Builder customBuilder = new CustomDialog.Builder(activity);
		customBuilder
				.setTitle("打开定位开关")
				.setMessage("定位服务未开启,请进入系统【设置】>【位置】中打开开关,并允许城家公寓使用定位功能。")
				.setContentView(null)
				.setNegativeButton("确定",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
												int which) {
								Intent i = new Intent();
								i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								activity.startActivity(i);
								confirmDialog.dismiss();
							}
						})
				.setPositiveButton("取消",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
												int which) {
								confirmDialog.dismiss();
							}
						});
		if (confirmDialog != null && confirmDialog.isShowing()) {
			confirmDialog.dismiss();
		}
		confirmDialog = customBuilder.createDialog();
		// 点击周围是否可以取消
		confirmDialog.setCanceledOnTouchOutside(false);
		confirmDialog.show();

	}
}
