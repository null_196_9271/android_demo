package com.uzhu.project.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    public static boolean checkNetwork(Context context) {
        return getNetworkType(context) != -1;
    }

    /**
     * Return -1 when there is no network available
     *
     * @return ConnectivityManager.TYPE_* or -1
     */
    public static int getNetworkType(Context context) {
        final ConnectivityManager manager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        try {
            networkInfo = manager.getActiveNetworkInfo();
        }catch (Exception e) {
            e.printStackTrace();
        }

        return (networkInfo == null || !networkInfo.isConnected()) ? -1 : networkInfo.getType();
    }
}
