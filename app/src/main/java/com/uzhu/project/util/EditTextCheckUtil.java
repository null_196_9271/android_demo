package com.uzhu.project.util;

import android.app.Activity;
import android.widget.EditText;

import com.mvvm.framework.util.ToastUtil;
import com.uzhu.project.R;

public class EditTextCheckUtil {

    public static final int CORRECT = 0;
    public static final int WRONG = 1;
    public static final String NULL = "Parameter is null";
    private static final String MOBILE_NOT_RIGHT = "请输入正确的手机号";
    private static final String PWD_NOT_RIGHT = "密码需要6-20位的字母和数字组合";
    private static final String NAME_NOT_RIGHT = "姓名应该在2—10个中英文";
    private static final String ENTERPRISE_NAME_NOT_RIGHT = "企业名称应该在8—60个中英文";
    private static final String LOCK_PWD_NOT_RIGHT = "请输入6位数字密码";
    public static final String CODE_NOT_RIGHT = "短信验证码错误";
    public static final String TEXT_TOO_LONG = "文本长度需要大于";
    public static final String TEXT_TOO_SHORT = "文本长度需要小于";

    public static final String BANK_CARD_BRANCH_NOT_RIGHT = "开户行支行名称不得超过25个字符";
    public static final String BANK_CARD_USERNAME_NOT_RIGHT = "开户人姓名应该在2—10个中英文";
    public static final String BANK_CARD_NO_NOT_RIGHT = "银行卡号应该输入10-30个数字";
    public static final String AMOUNT_NOT_RIGHT = "请输入正确的金额";

    /**
     * 验证手机号码是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：手机号长度不足或未以13,14,15,17,18开头
     */
    public static int checkMobileNumEdit(Activity context, EditText edit) {
        String checkMobleNum = VerifyUtils.checkMobileNO(edit.getText()
                .toString());
        if (checkMobleNum.equals(VerifyUtils.MOBILE_ERROR)) {
            checkMobleNum = MOBILE_NOT_RIGHT;
        }
        if (checkMobleNum.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, context.getResources().getString(R.string.empty_mobile));
            return WRONG;
        } else if (!checkMobleNum.equals(VerifyUtils.CORRECT)) {
            ToastUtil.show(context, checkMobleNum);
            return WRONG;
        } else {
            return CORRECT;
        }

    }

    /**
     * 验证密码是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：密码输入长度小于6位或大于20位
     */

    public static int checkPasswordEdit(Activity context, EditText edit) {
        String checkPassword = VerifyUtils.checkNumAndLetter(edit.getText()
                .toString(), 6, 20);
        if (checkPassword.equals(VerifyUtils.NUMANDLETTER_SHORT)) {
            checkPassword = PWD_NOT_RIGHT;
        } else if (checkPassword.equals(VerifyUtils.NUMANDLETTER_LONG)) {
            checkPassword = PWD_NOT_RIGHT;
        } else if (checkPassword.equals(VerifyUtils.NUMANDLETTER_ERROR)) {
            checkPassword = PWD_NOT_RIGHT;
        }
        if (checkPassword.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, context.getResources().getString(R.string.empty_password));
            return WRONG;
        } else if (checkPassword.equals(PWD_NOT_RIGHT)) {
            ToastUtil.show(context, checkPassword);
            return WRONG;
        } else {
            return CORRECT;
        }

    }

    /**
     * 验证姓名是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：姓名输入长度在2-10个字符
     */
    public static int checkNameEdit(Activity context, EditText edit) {
        String checkName = VerifyUtils.checkChineseAndEnglish(edit.getText()
                .toString(), 2, 10);
        if (checkName.equals(VerifyUtils.CHINESEANDENGLISH_SHORT)) {
            checkName = NAME_NOT_RIGHT;
        } else if (checkName.equals(VerifyUtils.CHINESEANDENGLISH_LONG)) {
            checkName = NAME_NOT_RIGHT;
        } else if (checkName.equals(VerifyUtils.CHINESEANDENGLISH_ERROR)) {
            checkName = NAME_NOT_RIGHT;
        }
        if (checkName.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, R.string.empty_name);
            return WRONG;
        } else if (checkName.equals(NAME_NOT_RIGHT)) {
            ToastUtil.show(context, checkName);
            return WRONG;
        } else {
            return CORRECT;
        }

    }

    /**
     * 验证验证码是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：输入等于4
     */
    public static int checkCodeEdit(Activity context, EditText edit) {
        String checkCode = VerifyUtils.checkIntLenth(edit.getText().toString(),
                0, 10000);
        if (edit.getText().toString().length() != 4) {
            checkCode = CODE_NOT_RIGHT;
        }
        if (checkCode.equals(VerifyUtils.NUM_MIN)) {
            checkCode = CODE_NOT_RIGHT;
        } else if (checkCode.equals(VerifyUtils.NUM_MAX)) {
            checkCode = CODE_NOT_RIGHT;
        } else if (checkCode.equals(VerifyUtils.NUM_ERROR)) {
            checkCode = CODE_NOT_RIGHT;
        }
        if (checkCode.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, R.string.empty_code);
            return WRONG;
        } else if (checkCode.equals(CODE_NOT_RIGHT)) {
            ToastUtil.show(context, checkCode);
            return WRONG;
        } else {
            return CORRECT;
        }

    }


    /**
     * 校验银行卡支行输入是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：不超过25个汉字
     */
    public static int checkBankCardBranchEdit(Activity context, EditText edit) {
        String checkCode = VerifyUtils.checkChinese(edit.getText().toString(),
                1, 25);
        if (checkCode.equals(VerifyUtils.CHINESE_SHORT)) {
            checkCode = BANK_CARD_BRANCH_NOT_RIGHT;

        } else if (checkCode.equals(VerifyUtils.CHINESE_LONG)) {

            checkCode = BANK_CARD_BRANCH_NOT_RIGHT;
        } else if (checkCode.equals(VerifyUtils.CHINESE_ERROR)) {

            checkCode = BANK_CARD_BRANCH_NOT_RIGHT;
        }

        if (checkCode.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, "请输入开户行支行");
            return WRONG;
        } else if (checkCode.equals(BANK_CARD_BRANCH_NOT_RIGHT)) {
            ToastUtil.show(context, checkCode);
            return WRONG;
        } else {
            return CORRECT;
        }
    }

    public static int checkBankCardUserNameEdit(Activity context, EditText edit) {
        String checkCode = VerifyUtils.checkChineseAndEnglish(edit.getText().toString(),
                2, 10);
        if (checkCode.equals(VerifyUtils.CHINESEANDENGLISH_SHORT)) {
            checkCode = BANK_CARD_USERNAME_NOT_RIGHT;
        } else if (checkCode.equals(VerifyUtils.CHINESEANDENGLISH_LONG)) {
            checkCode = BANK_CARD_USERNAME_NOT_RIGHT;
        } else if (checkCode.equals(VerifyUtils.CHINESEANDENGLISH_ERROR)) {
            checkCode = BANK_CARD_USERNAME_NOT_RIGHT;
        }

        if (checkCode.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, "请输入开户人姓名");
            return WRONG;
        } else if (checkCode.equals(BANK_CARD_USERNAME_NOT_RIGHT)) {
            ToastUtil.show(context, checkCode);
            return WRONG;
        } else {
            return CORRECT;
        }
    }

    public static int checkBankCardNoEdit(Activity context, EditText edit) {
        String editContent = edit.getText().toString().trim();
        if (editContent.isEmpty()) {
//            edit.requestFocus();
//            edit.setError("请输入银行卡号");
            ToastUtil.show(context, "请输入银行卡号");
            return WRONG;
        } else {
            if (editContent.length() < 10) {
                ToastUtil.show(context, BANK_CARD_NO_NOT_RIGHT);
                return WRONG;
            } else if (editContent.length() > 30) {
                ToastUtil.show(context, BANK_CARD_NO_NOT_RIGHT);
                return WRONG;
            } else {
                return CORRECT;
            }
        }
    }

    /**
     * 校验输入的金额是否正确，若不正确返回错误提示,若正确则返回 CORRECT
     */
    public static int checkAmount(Activity context, EditText edit) {
        String checkCode = VerifyUtils.checkAmount(edit.getText().toString());
        if (checkCode.equals(VerifyUtils.AMOUNT_ERROR)) {
            checkCode = AMOUNT_NOT_RIGHT;
        }
        if (checkCode.equals(VerifyUtils.NULL)) {
            ToastUtil.show(context, checkCode);
            return WRONG;
        }else if (checkCode.equals(VerifyUtils.AMOUNT_ERROR)) {
            ToastUtil.show(context, checkCode);
            return WRONG;
        }else {
            return CORRECT;
        }
    }
}
