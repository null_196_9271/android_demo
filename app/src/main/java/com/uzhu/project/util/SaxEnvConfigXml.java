package com.uzhu.project.util;

import com.mvvm.framework.util.LogUtil;
import com.uzhu.project.service.models.EnvConfing;
import com.uzhu.project.service.models.Environment;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

/*
 *  Sax的事件处理类必须实现ContentHandler接口，但我们在这个例子中
 *  不需要使用到ContentHandler接口的所有方法，我们仅需要其中的3个方法。
 *  所以Sax为我们提供了一个没有进行任何操作的ContentHandler实现类DefaultHandler。
 *  我们直接继承DefaultHandler类，并重写我们需要的方法即可。
 * */
public class SaxEnvConfigXml extends DefaultHandler {


	private Environment environment;

	private String currEnvironment;

	private ArrayList<EnvConfing> serverURLs;

	private ArrayList<EnvConfing> securityURLs;

	private ArrayList<EnvConfing> h5URLs;

	private EnvConfing tempEnvConfing;
	private String tagName;// 标签名称
	private String parentTagName;

	public Environment getEnvironment() {
		return environment;
	}

	/**
	 * 接受文档的开始通知
	 */
	@Override
	public void startDocument() throws SAXException {
		environment = new Environment();
	}

	/**
	 * 接受字符数据的通知
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if (tagName != null) {
			String data = new String(ch, start, length);
			LogUtil.d("SAX", "characters:" + data);
			if (tagName.equals("Environment")) {
				currEnvironment = data;
			} else if (tagName.equals("DEV")) {
				tempEnvConfing = new EnvConfing();
				tempEnvConfing.configTitle = ("DEV");
				tempEnvConfing.configValue = (data);
			} else if (tagName.equals("SIT")) {
				tempEnvConfing = new EnvConfing();
				tempEnvConfing.configTitle = ("SIT");
				tempEnvConfing.configValue = (data);

			} else if (tagName.equals("PRE")) {
				tempEnvConfing = new EnvConfing();
				tempEnvConfing.configTitle = ("PRE");
				tempEnvConfing.configValue = (data);
			} else if (tagName.equals("PRD")) {
				tempEnvConfing = new EnvConfing();
				tempEnvConfing.configTitle = ("PRD");
				tempEnvConfing.configValue = (data);
			}
		}

	}

	/**
	 * 接收元素开始的通知。 namespaceURI:元素的命名空间 localName:元素的本地名称（不带前缀） qName:元素的限定名（带前缀）
	 * atts:元素的属性集合
	 */

	@Override
	public void startElement(String uri, String localName, String name,
							 Attributes attributes) throws SAXException {


		LogUtil.d("SAX", "startElement:" + localName);
		if (localName.equals("Environment")) {
			parentTagName = localName;
		} else if (localName.equals("ServerURL")) {
			serverURLs = new ArrayList<EnvConfing>();
			parentTagName = localName;
		} else if (localName.equals("SecurityURL")) {
			securityURLs = new ArrayList<EnvConfing>();
			parentTagName = localName;
		} else if (localName.equals("H5URL")) {
			h5URLs = new ArrayList<EnvConfing>();
			parentTagName = localName;
		}
		this.tagName = localName;
	}

	/**
	 * 接收文档的结尾的通知。 uri:元素的命名空间 localName:元素的本地名称（不带前缀） name:元素的限定名（带前缀）
	 */

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		LogUtil.d("SAX", "endElement:" + localName);
		if (localName.equals("Environment")) {
			environment.setCurrEnvironment(currEnvironment);
			parentTagName = null;
		} else if (localName.equals("ServerURL")) {
			environment.setServerURLs(serverURLs);
			parentTagName = null;
		} else if (localName.equals("SecurityURL")) {
			environment.setSecurityURLs(securityURLs);
			parentTagName = null;
		}  else if (localName.equals("H5URL")) {
			environment.setSecurityURLs(h5URLs);
			parentTagName = null;
		} else {
			if (parentTagName != null && this.tempEnvConfing != null) {
				if (parentTagName.equals("ServerURL")) {
					serverURLs.add(this.tempEnvConfing);
				} else if (parentTagName.equals("SecurityURL")) {
					securityURLs.add(this.tempEnvConfing);
				} else if (parentTagName.equals("H5URL")) {
					h5URLs.add(this.tempEnvConfing);
				}
			}
		}
		this.tempEnvConfing = null;
		this.tagName = null;
	}
}
