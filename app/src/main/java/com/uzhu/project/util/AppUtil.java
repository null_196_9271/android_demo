package com.uzhu.project.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import com.uzhu.project.base.CommonBrowserActivity;
import com.uzhu.project.base.CommonConst;

import java.util.concurrent.atomic.AtomicInteger;

public class AppUtil {
    private static final String TAG = "AppUtil";

    public static boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 剪切图片
     *
     * @param uri
     * @function:
     * @author:Jerry
     * @date:2013-12-30
     */
    public static void crop(Uri uri, Activity activity) {
        // 裁剪图片意图
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        // 裁剪框的比例，1：1
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // 裁剪后输出图片的尺寸大小
        intent.putExtra("outputX", 250);
        intent.putExtra("outputY", 250);
        // 图片格式
        intent.putExtra("outputFormat", "JPEG");
        intent.putExtra("noFaceDetection", true);// 取消人脸识别
        intent.putExtra("return-data", true);// true:不返回uri，false：返回uri
        activity.startActivityForResult(intent, CommonConst.PHOTO_REQUEST_CUT);
    }

    /**
     * 根据资源图片的名获取资源drawale
     *
     * @param context
     * @param resName
     * @return
     */
    public static Drawable getDrawableByResName(Context context, String resName) {
        Drawable drawable = null;
        int resId = context.getResources().getIdentifier(resName.toLowerCase(), "drawable",
                context.getPackageName());
        if (resId > 0) {
            drawable = context.getResources().getDrawable(resId);
            // 这一步必须要做,否则不会显示.
            drawable.setBounds(0, 0, drawable.getMinimumWidth(),
                    drawable.getMinimumHeight());
            return drawable;
        } else {
            return drawable;
        }
    }

    /**
     * 控制textview最大显示长度,超过长度显示“...”
     *
     * @param textview
     * @param maxLength
     */
    public static void setMaxlength(TextView textview, int maxLength) {
        String str = textview.getText().toString().trim();
        if (str.length() > maxLength + 1) {
            String subStr = str.substring(0, maxLength + 1);
            textview.setText(subStr + "...");
        }
    }

    /**
     * 使用公共webview页面加载本地url
     *
     * @param activity
     * @param pageTitle
     * @param pageUrl
     */
    public static void startCommonBrowserForPage(Activity activity, String pageTitle, String pageUrl) {
        Intent intent = new Intent(activity, CommonBrowserActivity.class);
        intent.putExtra(CommonBrowserActivity.TITLE_KEY, pageTitle);
        intent.putExtra(CommonBrowserActivity.URL_KEY, pageUrl);
        activity.startActivity(intent);
    }

    /**
     * 判断GPS是否开启，GPS或者AGPS开启一个就认为是开启的
     *
     * @param context
     * @return true 表示开启
     */
    public static final boolean locationProviderIsOpen(final Context context) {
        LocationManager locationManager
                = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }
        return false;
    }

    public static boolean appProviderIsOpen(final Context context) {
        PackageManager pm = context.getPackageManager();
        boolean permission1 = (PackageManager.PERMISSION_GRANTED ==
                pm.checkPermission("android.permission.ACCESS_COARSE_LOCATION", context.getPackageName()));
        boolean permission2 = (PackageManager.PERMISSION_GRANTED ==
                pm.checkPermission("android.permission.ACCESS_FINE_LOCATION", context.getPackageName()));
        return permission1 && permission2;
    }

    /**
     * 调用intent启动拨打电话
     *
     * @param context
     * @param phone
     */
    public static void dial(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void openUrl(Context context, String url) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        Uri content_url = Uri.parse(url);
        intent.setData(content_url);
        context.startActivity(intent);
    }

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    /**
     * 代码布局时动态生成View ID
     * API LEVEL 17 以上View.generateViewId()生成
     * API LEVEL 17 以下需要手动生成
     */
    public static int generateViewId() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            for (; ; ) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) {
                    return result;
                }
            }
        } else {
            return View.generateViewId();
        }
    }
}
