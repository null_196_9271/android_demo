package com.uzhu.project.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;

/**
 * 文件处理
 * 
 */
public class FileHandler {

	/**
	 * sd卡的根目录
	 */
	private static String mSdRootPath = Environment
			.getExternalStorageDirectory().getPath();
	/**
	 * 手机的缓存根目录
	 */
	private static String mDataRootPath = null;
	/**
	 * 保存Image的目录名
	 */
	private final static String FOLDER_NAME = "/uzhu/image";

	public FileHandler(Context context) {
		mDataRootPath = context.getCacheDir().getPath();
	}

	/**
	 * 判断文件是否存在
	 * 
	 * @param fileName
	 * @return
	 */
	public boolean isFileExists(String fileName) {
		return new File(getStorageDirectory() + File.separator + fileName)
				.exists();
	}

	/**
	 * 获取储存Image的目录
	 * 
	 * @return
	 */
	private String getStorageDirectory() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED) ? mSdRootPath + FOLDER_NAME
				: mDataRootPath + FOLDER_NAME;
	}

	/**
	 * 获取文件的大小
	 * 
	 * @param fileName
	 * @return
	 */
	public long getFileSize(String fileName) {
		return new File(getStorageDirectory() + File.separator + fileName)
				.length();
	}

	/**
	 * 创建文件到下载目录
	 * 
	 * @param fileName
	 *            文件名
	 * @return
	 */
	public File createEmptyFileToDownloadDirectory(String fileName) {

		String downloadPath = getDownloadPath();
		File file = new File(downloadPath, fileName);

		File resultFile = FileHandler.createEmptyFileOrDirectory(file);

		return resultFile;
	}

	/**
	 * 创建文件到下载目录
	 * 
	 * @param fileName
	 *            文件名
	 * @return
	 */
	public File createEmptyFileToImageDirectory(String fileName) {

		String downloadPath = getImagePath();
		File file = new File(downloadPath, fileName);

		File resultFile = FileHandler.createEmptyFileOrDirectory(file);

		return resultFile;
	}

	// 判断文件是否存在，若不存在则创建
	private static File createEmptyFileOrDirectory(File file) {
		// 判断文件是否存在，若不存在则创建
		if (!file.exists()) {
			try {
				// 这里要取到父目录，否则会把文件当作目录来创建
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// 如果存在的话，还要判断是否是目录（因为linux系统中目录也会当作文件处理）
		else if (file.isDirectory()) {
			file.delete();
		}

		return file;
	}

	/**
	 * 在指定目录查找文件
	 * 
	 * @param fileNameString
	 *            文件名
	 * @param path
	 *            目录名
	 * @return
	 */
	public File findFileByName(String fileNameString, String path) {
		File resultFile = null;
		if (fileNameString == null) {
			return null;
		}

		File file = new File(path);
		if (!file.isDirectory()) {
			System.out.println("目录有错误，请重新设置");
			return null;
		}

		// 遍历该目录下的所有文件
		for (File fileItem : file.listFiles()) {
			if (fileItem.getName().equalsIgnoreCase(fileNameString)) {
				resultFile = fileItem;
			}
		}
		return resultFile;
	}

	/**
	 * 取得图片目录
	 * 
	 * @return
	 */
	public String getImagePath() {
		return getStorageDirectory();
	}

	/**
	 * 取得图片目录
	 * 
	 * @return
	 */
	public String getDownloadPath() {
		return getStorageDirectory();
	}

	/**
	 * 通过Uri返回File文件
	 * 注意：通过相机的是类似content://media/external/images/media/97596
	 * 通过相册选择的：file:///storage/sdcard0/DCIM/Camera/IMG_20150423_161955.jpg
	 * 通过查询获取实际的地址
	 *
	 * @param uri
	 * @return
	 */
	public static File getFileByUri(Uri uri, Context context) {
		String path = null;
		if ("file".equals(uri.getScheme())) {
			path = uri.getEncodedPath();
			if (path != null) {
				path = Uri.decode(path);
				ContentResolver cr = context.getContentResolver();
				StringBuffer buff = new StringBuffer();
				buff.append("(").append(MediaStore.Images.ImageColumns.DATA).append("=").append("'" + path + "'").append(")");
				Cursor cur = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA}, buff.toString(), null, null);
				int index = 0;
				int dataIdx = 0;
				for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
					index = cur.getColumnIndex(MediaStore.Images.ImageColumns._ID);
					index = cur.getInt(index);
					dataIdx = cur.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
					path = cur.getString(dataIdx);
				}
				cur.close();
				if (index == 0) {
				} else {
					Uri u = Uri.parse("content://media/external/images/media/" + index);
					System.out.println("temp uri is :" + u);
				}
			}
			if (path != null) {
				return new File(path);
			}
		} else if ("content".equals(uri.getScheme())) {
			// 4.2.2以后
			String[] proj = {MediaStore.Images.Media.DATA};
			Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
			if (cursor.moveToFirst()) {
				int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				path = cursor.getString(columnIndex);
			}
			cursor.close();

			return new File(path);
		}
		return null;
	}
}
