package com.uzhu.project.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

public class AnimationUtil {

	public static void startRotateAnimation(final View view) {
		RotateAnimation rotateAnimation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);
		rotateAnimation.setDuration(3000);
		rotateAnimation.setRepeatCount(-1);
		LinearInterpolator lin = new LinearInterpolator();
		rotateAnimation.setInterpolator(lin);  
		view.startAnimation(rotateAnimation);
	}
}
