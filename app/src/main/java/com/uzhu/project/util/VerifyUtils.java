package com.uzhu.project.util;

import com.mvvm.framework.util.LogUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyUtils {
    private static String TAG = "VerifyUtils";
    public static final String CORRECT = "correct";
    public static final String NULL = "Parameter is null";
    public static final String MOBILE_ERROR= "mobileError";
    public static final String EMAIL_ERROR = "emailError";
    public static final String AGE_ERROR = "ageError";
    public static final String AGE_YOUNG = "young";
    public static final String AGE_OLD = "old";
    public static final String CHINESE_ERROR = "chineseError";
    public static final String CHINESE_LONG = "long3";
    public static final String CHINESE_SHORT = "short3";
    public static final String CHINESEANDENGLISH_ERROR = "chineseAndEnglishError";
    public static final String CHINESEANDENGLISH_LONG = "long2";
    public static final String CHINESEANDENGLISH_SHORT = "short2";
    public static final String NUMANDLETTER_ERROR = "numAndLetterError";
    public static final String NUMANDLETTER_LONG = "long1";
    public static final String NUMANDLETTER_SHORT = "short1";
    public static final String TEXT_ERROR = "textError";
    public static final String TEXT_LONG = "long";
    public static final String TEXT_SHORT = "short";
    public static final String NUM_ERROR = "numError";
    public static final String NUM_MAX = "max";
    public static final String NUM_MIN = "min";
    public static final String AMOUNT_ERROR = "amountError";

    /**
     * 验证手机号码是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：手机号长度不足或未以1开头
     */
    public static String checkMobileNO(String mobileNo) {
        if (null == mobileNo || "".equals(mobileNo)) {
            return NULL;
        }
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^1\\d{10}$");
            Matcher m = p.matcher(mobileNo);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }

        if (flag) {
            checkResult = CORRECT;
        } else {
            checkResult = MOBILE_ERROR;
        }

        return checkResult;
    }

    /**
     * 验证数字+大小写字母，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：text不合法，或者长度过长，过短
     */
    public static String checkNumAndLetter(String text, int minLength, int maxLength) {
        if (null == text || "".equals(text)) {
            return NULL;
        }
        text = text.replace(" ","");
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^(?![0-9]+$)(?![A-Za-z]+$)[a-zA-Z0-9]{6,20}$");
            Matcher m = p.matcher(text);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }

        if (flag) {
            int length = text.length();
            if(length<minLength)
                checkResult = NUMANDLETTER_SHORT;
            else if(length>maxLength)
                checkResult = NUMANDLETTER_LONG;
            else
                checkResult = CORRECT;
        } else {
            checkResult = NUMANDLETTER_ERROR;
        }

        return checkResult;
    }

    /**
     * 验证中文是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：text不合法，或者长度过长，过短
     */
    public static String checkChinese(String text, int minLength, int maxLength) {
        if (null == text || "".equals(text)) {
            return NULL;
        }
//        text = text.replace(" ","");
        text = text.trim();
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^([一-龥]*)$");
            Matcher m = p.matcher(text);
            flag = m.matches();
        } catch (Exception e) {
            LogUtil.e(TAG, e.getMessage());
            flag = false;
        }

        if (flag) {
            int length = text.length();
            if(length<minLength)
                checkResult = CHINESE_SHORT;
            else if(length>maxLength)
                checkResult = CHINESE_LONG;
            else
                checkResult = CORRECT;
        } else {
            checkResult = CHINESE_ERROR;
        }

        return checkResult;
    }

    /**
     * 验证中文+英文是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：text不合法，或者长度过长，过短
     */
    public static String checkChineseAndEnglish(String text, int minLength, int maxLength) {
        if (null == text || "".equals(text)) {
            return NULL;
        }
        text = text.replace(" ", "");
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^([a-zA-Z一-龥]*)$");  //|([一-龥]
            Matcher m = p.matcher(text);
            flag = m.matches();
        } catch (Exception e) {
            LogUtil.e(TAG, e.getMessage());
            flag = false;
        }

        if (flag) {
            int length = text.length();
            if(length<minLength)
                checkResult = CHINESEANDENGLISH_SHORT;
            else if(length>maxLength)
                checkResult = CHINESEANDENGLISH_LONG;
            else
                checkResult = CORRECT;
        } else {
            checkResult = CHINESEANDENGLISH_ERROR;
        }

        return checkResult;
    }

    /**
     * 验证邮箱是否正确，若不正确返回错误提示,若正确则返回 CORRECT 验证规则：正确邮箱格式
     */
    public static String checkEmail(String email) {
        if (null == email || "".equals(email)) {
            return NULL;
        }
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\\.[a-zA-Z0-9_-]{2,3}){1,2})$");
            Matcher m = p.matcher(email);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }

        if (flag) {
            checkResult = CORRECT;
        } else {
            checkResult = EMAIL_ERROR;
        }
        return checkResult;
    }


    /**
     * 验证单位为年的年龄是否正确，若不正确返回错误提示,若正确则返回 CORRECT
     */
    public static String checkYearAge(String age, int minAge, int maxAge) {
        if (null == age || "".equals(age)) {
            return NULL;
        }
        boolean flag = false;
        try {
            Pattern p = Pattern
                    .compile("^([1-9]+[0-9]{0,2})$");
            Matcher m = p.matcher(age);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }
        if (flag) {
            int intValue = new Integer(age).intValue();
            if(intValue<minAge)
                return AGE_YOUNG;
            else if(intValue>maxAge)
                return AGE_OLD;
            else
                return CORRECT;
        } else {
            return AGE_ERROR;
        }
    }


    /**
     * 验证单位为月的年龄是否正确，若不正确返回错误提示,若正确则返回 CORRECT
     */
    public static String checkMonthAge(String age, int minAge, int maxAge) {
        if (null == age || "".equals(age)) {
            return NULL;
        }
        boolean flag = false;
        try {
            Pattern p = Pattern
                    .compile("^([1-9]+[0-9]{0,2})$");
            Matcher m = p.matcher(age);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }
        if (flag) {
            int intValue = new Integer(age).intValue();
            if(intValue<minAge)
                return AGE_YOUNG;
            else if(intValue>maxAge)
                return AGE_OLD;
            else
                return CORRECT;
        } else {
            return AGE_ERROR;
        }
    }
    /**
     * 验证输入文本长度，若不正确返回错误提示,若正确则返回 CORRECT 字符数大于等于min，小于等于max
     */
    public static String checkTextLenth(String text, int minLength, int maxLength) {
        if (null == text || "".equals(text)) {
            return NULL;
        }
        int length = text.length();
        if(length<minLength)
            return TEXT_SHORT;
        else if(length>maxLength)
            return TEXT_LONG;
        else
            return CORRECT;
    }

    /**
     * 验证输入整数数字大小，若不正确返回错误提示,若正确则返回 CORRECT 整数大于等于min，小于等于max
     */
    public static String checkIntLenth(String num, int min, int max) {
        if (null == num || "".equals(num)) {
            return NULL;
        }
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^[0-9]*$");
            Matcher m = p.matcher(num);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }

        if (flag) {
            int intValue = new Integer(num);
            if(intValue<min)
                checkResult = NUM_MIN;
            else if(intValue>max)
                checkResult = NUM_MAX;
            else
                checkResult = CORRECT;
        } else {
            checkResult = NUM_ERROR;
        }

        return checkResult;
    }

    /**
     * 验证输入的金额，若不正确返回错误提示，若正确返回 CORRECT
     */
    public static String checkAmount(String amount)
    {
        if (null == amount || "".equals(amount)) {
            return NULL;
        }
        boolean flag = false;
        String checkResult;
        try {
            Pattern p = Pattern
                    .compile("^(([0-9]+)|([0-9]+.[0-9]{1,2}))$");
            Matcher m = p.matcher(amount);
            flag = m.matches();
        } catch (Exception e) {
            flag = false;
        }
        if (flag) {
            checkResult = CORRECT;
        }else {
            checkResult = AMOUNT_ERROR;
        }
        return checkResult;
    }
}