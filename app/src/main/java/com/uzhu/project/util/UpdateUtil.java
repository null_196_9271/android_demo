package com.uzhu.project.util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import com.uzhu.project.R;
import com.uzhu.project.base.AppBaseFragmentActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * app升级类
 */
public class UpdateUtil {

    private static final int NOTIFICATION = 10001;
    private String apkName = "update.apk";

    private Context context;

    private NotificationCompat.Builder progressNotification;

    private NotificationManager notificationManager;

    public UpdateUtil(Context context, String apkName) {
        super();
        this.context = context;
        this.apkName = apkName;
    }

    /**
     * 下载方法
     *
     * @param path
     * @throws Exception
     */
    public void downloadApkFile(String path) throws Exception {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            int total = conn.getContentLength();
            // 获取到文件的大小
            InputStream is = conn.getInputStream();
            File file = new File(Environment.getExternalStorageDirectory(),
                    apkName);
            if (file.exists()) {
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(file);
            BufferedInputStream bis = new BufferedInputStream(is);
            byte[] buffer = new byte[1024 * 10];
            int len;
            int temp = 0;
            while ((len = bis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
                temp += len;
                final int i = (temp * 100) / total;
                ((AppBaseFragmentActivity) context).setProgressVal(i);
                progressNotification.setProgress(total, temp, false);
                notificationManager.notify(NOTIFICATION,
                        progressNotification.build());
            }
            fos.close();
            bis.close();
            is.close();
            installApk(file);
            hideNotification();
        }
    }

    /**
     * 通知栏
     */
    public void showNotification() {
        progressNotification = new NotificationCompat.Builder(context);
        // 下面三个参数是必须要设定的
        progressNotification.setSmallIcon(R.mipmap.ic_launcher);
        progressNotification.setContentTitle("版本更新");
        progressNotification.setContentText("");
        // 将AutoCancel设为true后，当你点击通知栏的notification后，它会自动被取消消失
        // notifyBuilder.setAutoCancel(true);
        // 将Ongoing设为true 那么notification将不能滑动删除
        // notifyBuilder.setOngoing(true);
        // 从Android4.1开始，可以通过以下方法，设置notification的优先级，优先级越高的，通知排的越靠前，优先级低的，不会在手机最顶部的状态栏显示图标
        progressNotification.setPriority(NotificationCompat.PRIORITY_MAX);
        progressNotification.setOngoing(false);
        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        progressNotification.setProgress(100, 0, false);
        notificationManager.notify(NOTIFICATION, progressNotification.build());
    }

    public void hideNotification() {
        notificationManager.cancel(NOTIFICATION);
    }

    private void setPendIntent(Intent intent) {
        int requestCode = (int) SystemClock.uptimeMillis();
        PendingIntent pendIntent = PendingIntent.getActivity(context,
                requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        progressNotification.setContentIntent(pendIntent);
        notificationManager.notify(NOTIFICATION, progressNotification.build());
    }

    // 安装apk
    protected void installApk(File file) {
        Intent intent = new Intent();
        // 执行动作
        intent.setAction(Intent.ACTION_VIEW);
        // 执行的数据类型
        intent.setDataAndType(Uri.fromFile(file),
                "application/vnd.android.package-archive");
        setPendIntent(intent);

        context.startActivity(intent);
    }
}
