package com.uzhu.project.util;

import android.text.TextUtils;

import com.mvvm.framework.util.DateUtil;
import com.mvvm.framework.util.LogUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StrUtils {
    private static final String TAG = "StrUtils";

    /**
     * 判断 字符串是否是空的
     *
     * @param string
     * @return true 空字符串，false 不为空
     */
    public static boolean stringIsEmpty(String string) {
        if (string == null || ("").equals(string.trim())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 得到字符串的非空指针
     *
     * @param str 要转换的字符串
     * @return
     */
    public static String getString(String str) {
        if (str == null || str.equals("null") || str.equals("")) {
            return "";
        } else {
            return str.trim();
        }
    }

    /**
     * 判断是否是数字
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        if (str == null || "".equals(str.trim())) {
            return false;
        } else {
            Pattern pattern = Pattern.compile("[0-9]*");
            return pattern.matcher(str.trim()).matches();
        }
    }

    /**
     * 取整（四舍五入）
     *
     * @param num 待转换的价格
     * @return 转换后的价格
     */
    public static String formatInt(String num) {
        int xx = Math.round(Float.parseFloat(num));
        return xx + "";
    }

    /**
     * 价格转换
     *
     * @param price 待转换的价格
     * @return 转换后的价格
     */
    public static String formatPrice(long price) {
//		DecimalFormat myformat = new DecimalFormat();
//		if (price % 100 == 0) {
//			myformat.applyPattern("##0");
//		} else {
//			myformat.applyPattern("##0.00");
//		}
//		return myformat.format(price / 100.0);

        DecimalFormat myformat = new DecimalFormat();
        myformat.applyPattern("##,##0.00");
        return myformat.format(price / 100.0);
    }

    /**
     * 价格转换
     *
     * @param price 待转换的价格
     * @return 转换后的价格
     */
    public static String formatTenThousandPrice(long price) {
        DecimalFormat myformat = new DecimalFormat();
        if (price % 1000000 == 0) {
            myformat.applyPattern("##0");
        } else {
            myformat.applyPattern("##0.0");
        }
        return myformat.format(price / 1000000.0);
    }

    /**
     * 将金额格式化为100,000,000.00
     *
     * @param price
     * @return
     */
    public static String formatShowCash(long price) {
        DecimalFormat myformat = new DecimalFormat();
        myformat.applyPattern("##,##0.00");
        return myformat.format(price / 100.0);
    }

    private static DecimalFormat dfs = null;

    public static DecimalFormat getCashformat(String pattern) {
        if (dfs == null) {
            dfs = new DecimalFormat();
        }
        dfs.setRoundingMode(RoundingMode.FLOOR);
        dfs.applyPattern(pattern);
        return dfs;
    }

    /**
     * 距离单位换算
     *
     * @param distance
     * @return
     */
    public static String formatDistanceUnit(float distance) {
        String result = "";
        if (distance > 1000) {
            DecimalFormat fnum = new DecimalFormat("##0.0");
            result = fnum.format(distance / 1000.0) + "公里";

        } else {
            DecimalFormat fnum = new DecimalFormat("##0");
            result = fnum.format(distance) + "米";
        }

        return result;
    }

//    /**
//     * 将价格转换为保留2位小数
//     *
//     * @param price 待转换的价格
//     * @return 转换后的价格
//     */
//    public static String formatCashPrice(long price) {
//        //金额取整
//        return formatInt(price / 100 + "");
//    }

    public static String getPeriod(long times) {
        String period = null;
        switch ((int) times) {
            case 1:
                period = "一　　期";
                break;
            case 2:
                period = "二　　期";
                break;
            case 3:
                period = "三　　期";
                break;
            case 4:
                period = "四　　期";
                break;
            case 5:
                period = "五　　期";
                break;
            case 6:
                period = "六　　期";
                break;
            case 7:
                period = "七　　期";
                break;
            case 8:
                period = "八　　期";
                break;
            case 9:
                period = "九　　期";
                break;
            case 10:
                period = "十　　期";
                break;
            case 11:
                period = "十  一  期";
                break;
            case 12:
                period = "十  二  期";
                break;

            default:
                break;
        }
        return period;
    }

    public static String getBankTailNo(String bankCardNo) {
        if (!stringIsEmpty(bankCardNo) && bankCardNo.length() >= 4) {
            return "尾号" + bankCardNo.substring(bankCardNo.length() - 4, bankCardNo.length());
        } else {
            return "";
        }
    }

    /**
     * 时间格式转换
     *
     * @param date ”201509101200“
     * @return ”2015/09/10 上午“
     */
    public static String convertDateString(String date) {
        String formatString = "";
        String hour = "";
        try {
            formatString = DateUtil.formatStringFrom14String(date,
                    "yyyy/MM/dd HH:mm:ss");
            hour = DateUtil.formatStringFrom14String(date, "HH");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (Integer.parseInt(hour) < 12) {
            formatString = formatString.substring(0, 11) + "上午";
        } else if (Integer.parseInt(hour) < 18) {
            formatString = formatString.substring(0, 11) + "下午";
        } else {
            formatString = formatString.substring(0, 11) + "晚上";
        }
//		formatString = formatString.replace("00:00:00", "上午");
//		formatString = formatString.replace("12:00:00", "下午");
//		formatString = formatString.replace("18:00:00", "晚上");
        return formatString;
    }

    /**
     * 判断字符串是否是整数
     */
    public static boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 判断字符串是否是浮点数
     */
    public static boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            if (!value.contains("."))
                return false;
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 验证输入的金额，若不正确返回错误提示，若正确返回 CORRECT
     */
    public static boolean checkAmount(String amount) {
        if (amount.split("\\.").length == 1 && amount.endsWith(".")) {
            amount = amount.substring(0, amount.length() - 1);
            LogUtil.d(TAG, amount);
        }
        if (isInteger(amount)) {
            return true;
        }
        if (isDouble(amount)) {
            Pattern p = Pattern
                    .compile("^(([0-9]+)|([0-9]+.[0-9]{1,2}))$");
            Matcher m = p.matcher(amount);
            return m.matches();
        }
        return false;

    }

    public static String formatDecimal(Long price, double unit) throws ArithmeticException {
        if (price == null) {
            price = 0L;
        }
        if (unit == 0) {
            throw new ArithmeticException("divide zero unit is not supported!");
        }
        DecimalFormat myformat = new DecimalFormat();
        //小数位最多保留2位
        myformat.setMaximumFractionDigits(2);
        myformat.setGroupingSize(0);
        BigDecimal priceValue = BigDecimal.valueOf(price);
        BigDecimal unitValue = BigDecimal.valueOf(unit);
        return myformat.format(priceValue.divide(unitValue, 2, BigDecimal.ROUND_HALF_UP));
    }


    public static String hidePhoneNum(String PhoneNum) {
        if (TextUtils.isEmpty(PhoneNum)) {
            return "";
        }
        StringBuilder sb  =new StringBuilder();
        for (int i = 0; i < PhoneNum.length(); i++) {
            char c = PhoneNum.charAt(i);
            if (i >= 3 && i <= 6) {
                sb.append("*");
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String hideIDNum(String PhoneNum) {
        if (TextUtils.isEmpty(PhoneNum)) {
            return "";
        }
        StringBuilder sb  =new StringBuilder();
        for (int i = 0; i < PhoneNum.length(); i++) {
            char c = PhoneNum.charAt(i);
            if (i >= 6 && i < 14) {
                sb.append("*");
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String hideName(String name) {
        if (TextUtils.isEmpty(name)) {
            return "";
        }
        StringBuilder sb  =new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            char c = name.charAt(i);
            if (i >= 1) {
                sb.append("*");
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String getNumByChinese(int num) {
        String period = null;
        switch ( num+1) {
            case 1:
                period = "1期";
                break;
            case 2:
                period = "2期";
                break;
            case 3:
                period = "3期";
                break;
            case 4:
                period = "4期";
                break;
            case 5:
                period = "5期";
                break;
            case 6:
                period = "6期";
                break;
            case 7:
                period = "7期";
                break;
            case 8:
                period = "8期";
                break;
            case 9:
                period = "9期";
                break;
            case 10:
                period = "10期";
                break;
            case 11:
                period = "11期";
                break;
            case 12:
                period = "12期";
                break;

            default:
                break;
        }
        return period;
    }

    public static boolean checkPassword(String password) {
        return password.matches("^[0-9A-Za-z]{6,20}$");
    }

    public static boolean checkPhoneNum(String phoneNum) {
        return phoneNum.matches("0?(13|14|15|18)[0-9]{9}");
    }
}
