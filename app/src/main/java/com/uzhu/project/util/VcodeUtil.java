package com.uzhu.project.util;

import android.os.Handler;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 验证码
 */
public class VcodeUtil {

	private static VcodeUtil vCodeUitl;
	private TextView view;

	private Timer timer;
	private static final int TIME_COUNT_DOWN = 1;
	private int timeCount;
	private static final int TIMER_COUNT_DOWN = 60;

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case TIME_COUNT_DOWN:
				if (timeCount > 0) {
					view.setEnabled(false);
					view.setText(timeCount + "秒后\n重新发送");
				} else {
					view.setText("获取验证码");
					view.setEnabled(true);
					timer.cancel();
				}
				break;

			default:
				break;
			}
		};
	};

	public static VcodeUtil instance() {
		synchronized (VcodeUtil.class) {
			if (null == vCodeUitl) {
				vCodeUitl = new VcodeUtil();
			}
		}
		return vCodeUitl;
	}

	public void setViewToVcode(TextView view) {
		this.view = view;
		startTimeCount();
	}

	public void startTimeCount() {
		timer = new Timer();
		timeCount = TIMER_COUNT_DOWN;
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				timeCount -= 1;
				handler.sendMessage(handler.obtainMessage(TIME_COUNT_DOWN));
			}
		}, 0, 1000);
	}

	public void stopTimeCount(/* int viewId */) {
		// if(view.getId() == viewId){
		view.setText("获取验证码");
		view.setEnabled(true);
		timer.cancel();
		// }
	}

}
