/*
 * Copyright (C),2015-2015. 城家酒店管理有限公司
 * FileName: TenementConfig.java
 * Author:   jeremy
 * Date:     2015-10-15 10:01:27
 * Description: //模块目的、功能描述
 * History: //修改记录 修改人姓名 修改时间 版本号 描述
 * <jeremy>  <2015-10-15 10:01:27> <version>   <desc>
 */

package com.uzhu.project.util;

import android.app.ActivityManager;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.mvvm.framework.service.MobileHead;
import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.service.task.NoBlockExecuteListener;
import com.mvvm.framework.util.ENVConfig;
import com.mvvm.framework.util.LogUtil;
import com.mvvm.framework.util.PersistenceUtil;
import com.umeng.analytics.MobclickAgent;
import com.uzhu.project.R;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.base.AppCollectCode;
import com.uzhu.project.base.WXManager;
import com.uzhu.project.base.cache.EnvConfigLoader;
import com.uzhu.project.base.cache.UserCenter;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.Environment;
import com.xiaomi.channel.commonutils.logger.LoggerInterface;
import com.xiaomi.mipush.sdk.Logger;
import com.xiaomi.mipush.sdk.MiPushClient;

import java.util.List;

public class AppConfig extends ENVConfig {
    private static final String TAG = "AppConfig";
    public static final String SERVERURL_PRD = "http://mobile.ishouzu.cn";
    public static final String SECURITYSERVERURL_PRD = "http://mobile.ishouzu.cn";
    public static final String H5URL_PRD = "https://ishouzu.cn";
    private Context mContext;
    public static Environment env;

    public AppConfig(Context mContext) {
        this.mContext = mContext;
    }

    public void projectConfig() {
        initConfig();
        initServiceMediator();
        initDevice();
        WXManager.getInstance().init(mContext);
        initMessageCollect();
        LogUtil.setDebug(AppApplication.getInstance().isDeveloping());
    }

    private void initDevice() {
        String cid = PersistenceUtil.getObjectFromSharePreferences(AppServiceMediator.CLIENT_ID,
                String.class);
        if (!TextUtils.isEmpty(cid)) {
            MobileHead.getMobileHead().clientId = cid;
            initMsgPush();

        } else {
            ServiceFuture future = AppServiceMediator.sharedInstance().doClientRegister(mContext);
            future.addServiceListener("registerClient", new NoBlockExecuteListener() {
                @Override
                public void onExecuteSuccess(String tag, Object result) {
                    String clientId = (String) result;
                    if (!TextUtils.isEmpty(clientId)) {
                        MobileHead.getMobileHead().clientId = clientId;
                        PersistenceUtil.saveObjectToSharePreferences(AppServiceMediator.CLIENT_ID, clientId);
                        // 若已经注册过设备，直接开启推送功能
                        initMsgPush();
                    }
                }
            });
        }
    }

    /**
     * 初始化本地配置类
     */
    private void initConfig() {
        ENVConfig.configurationEnvironment(mContext);
        env = EnvConfigLoader.init(mContext);
    }

    /**
     * 初始化消息推送
     */
    public void initMsgPush() {
        initMiPush();
    }

    /**
     * 初始化ServiceMediator服务
     */
    private void initServiceMediator() {
        AppServiceMediator.sharedInstance();
        if (UserCenter.instance().isLogin()) {
            setUserToken(UserCenter.instance().getUser().userToken);
        }
    }

    /**
     * 初始化信息搜集服务
     */
    private void initMessageCollect() {
        AppCollectCode.initPageCodeMap();
        String APP_KEY = mContext.getResources().getString(R.string.UMENG_APPKEY_SIT);
        if (!AppApplication.getInstance().isDeveloping()) {
            APP_KEY = mContext.getResources().getString(R.string.UMENG_APPKEY_PRD);
        }
        MobclickAgent.UMAnalyticsConfig config = new MobclickAgent.UMAnalyticsConfig(mContext, APP_KEY, AppServiceMediator.SOURCE_CODE, MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.startWithConfigure(config);
        //友盟统计开关
        MobclickAgent.setDebugMode(AppApplication.getInstance().isDeveloping());
        MobclickAgent.setCatchUncaughtExceptions(!AppApplication.getInstance().isDeveloping());
        //加密
        MobclickAgent.enableEncrypt(!AppApplication.getInstance().isDeveloping());
        //不自动统计页面
        MobclickAgent.openActivityDurationTrack(false);

    }



    /**
     * 清除用户登录状态
     */
    public static void clearLoginState() {
        UserCenter.instance().setUser(null);
        setUserToken("");
    }

    private  void initMiPush() {
        //初始化push推送服务
        if (shouldInit(mContext)) {
            String appId = mContext.getString(R.string.MIPUSH_APPID);
            String appKey =  mContext.getString(R.string.MIPUSH_APPKEY);
            MiPushClient.registerPush(mContext, appId, appKey);
        }
        //打开Log
        LoggerInterface newLogger = new LoggerInterface() {

            @Override
            public void setTag(String tag) {
                // ignore
            }

            @Override
            public void log(String content, Throwable t) {
                Log.d(TAG, content, t);
            }

            @Override
            public void log(String content) {
                Log.d(TAG, content);
            }
        };
        Logger.setLogger(mContext, newLogger);
    }

    private static boolean shouldInit(Context context) {
        ActivityManager am = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = context.getPackageName();
        int myPid = android.os.Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }
}
