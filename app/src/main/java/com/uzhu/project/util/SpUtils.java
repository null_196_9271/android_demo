package com.uzhu.project.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SpUtils {
	

	private  static SharedPreferences getSp(Context context){
		SharedPreferences sp = context.getSharedPreferences("config", Context.MODE_PRIVATE);
		return sp;
	}
	
	public static void putInt(Context context, String key, Integer value){
		getSp(context).edit().putInt(key, value).commit();
	}
	public static void putBoolean(Context context, String key, Boolean value){
		getSp(context).edit().putBoolean(key, value).commit();
	}
	public static void putString(Context context, String key, String value){
		getSp(context).edit().putString(key, value).commit();
	}
	public  static void putLong(Context context, String key, Long value){
		getSp(context).edit().putLong(key,value).commit();
	}
	
	public static Boolean getBoolean(Context context, String key){
		boolean value = getSp(context).getBoolean(key, false);
		return value;
	}
	public static Long getLong(Context context, String key){
		Long value = getSp(context).getLong(key, 0);
		return value;
	}
	
	
	public static String getString(Context context, String key){
		String value = getSp(context).getString(key, "");
		return value;
	}
	public static Integer getInt(Context context, String key){
		
		Integer value = getSp(context).getInt(key, 0);
		return value;
	}

	
	
}
