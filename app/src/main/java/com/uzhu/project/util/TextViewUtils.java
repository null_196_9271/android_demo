package com.uzhu.project.util;

import android.util.TypedValue;
import android.widget.TextView;

/**
 * Created by John on 2016/12/20 0020.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 16:06
 * UpdateDescribe
 */

public class TextViewUtils {
    public static void setBigTextSize(TextView textView) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
    }

    public static void setMidTextSize(TextView textView) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
    }

    public static void setSmallTextSize(TextView textView) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    }
}
