package com.uzhu.project.view.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by John on 2016/12/16 0016.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 15:33
 * UpdateDescribe
 */

public abstract class BaseLinearLayout extends LinearLayout {
    public BaseLinearLayout(Context context) {
        this(context, null);
    }

    public BaseLinearLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View.inflate(context, getLayout(), this);
        initView();
    }

    protected abstract void initView();

    protected abstract int getLayout();
}
