package com.uzhu.project.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uzhu.project.R;
import com.uzhu.project.util.UiUtil;


/**
 * Created by hainiao on 2015/10/8.
 */
public class TopTitleBar extends RelativeLayout {

	private View mRootView;
	private TextView mLeftView;
	private TextView mLeftCloseTextView;
	private TextView mCenterView;
	private TextView mRightView;
	private ImageView mBadge;
	private int width10dp, width15dp;

	public TopTitleBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);

		TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TopTitleBar);
		try {
			String title = array.getString(R.styleable.TopTitleBar_titleText);
			if (!TextUtils.isEmpty(title)) {
				mCenterView.setText(title);
			}
			width10dp = UiUtil.dp2px(context, 10);
			String leftText = array.getString(R.styleable.TopTitleBar_leftText);
			if (!TextUtils.isEmpty(leftText)) {
				mLeftView.setText(leftText);
				mLeftView.setPadding(width10dp, 0, width10dp, 0);
			}
			String leftCloseText = array.getString(R.styleable.TopTitleBar_leftCloseText);
			if (!TextUtils.isEmpty(leftCloseText)) {
				mLeftCloseTextView.setText(leftCloseText);
			}
			width15dp = UiUtil.dp2px(context, 15);
			String rightText = array.getString(R.styleable.TopTitleBar_rightText);
			if (!TextUtils.isEmpty(rightText)) {
				mRightView.setText(rightText);
				mRightView.setPadding(width15dp, 0, width15dp, 0);
			}
			int leftId = array.getResourceId(R.styleable.TopTitleBar_leftDrawable, 0);
			if (leftId > 0) {
				mLeftView.setCompoundDrawablesWithIntrinsicBounds(leftId, 0, 0, 0);
				mLeftView.setPadding(0, 0, width10dp, 0);
			}
			int rightId = array.getResourceId(R.styleable.TopTitleBar_rightDrawable, 0);
			if (rightId > 0) {
				mRightView.setCompoundDrawablesWithIntrinsicBounds(0, 0, rightId, 0);
				mRightView.setPadding(width15dp, 0, 0, 0);
			}
		} finally {
			array.recycle();
		}

	}

	private void initView(final Context context) {
		mRootView = LayoutInflater.from(context).inflate(R.layout.common_top_titlebar, this, true);
		mLeftView = (TextView) mRootView.findViewById(R.id.top_left_tv);
		mLeftView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				((Activity) context).finish();
			}
		});
		mLeftCloseTextView = (TextView) mRootView.findViewById(R.id.top_left_close_tv);
		mLeftCloseTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((Activity) context).finish();
			}
		});
		mCenterView = (TextView) mRootView.findViewById(R.id.top_title_tv);
		mRightView = (TextView) mRootView.findViewById(R.id.top_right_tv);
		mBadge = (ImageView) mRootView.findViewById(R.id.badge);
	}

	public TextView getLeftView() {
		return mLeftView;
	}

	public void setTitle(int resId) {
		mCenterView.setText(resId);
	}

	public void setTitle(CharSequence title) {
		mCenterView.setText(title);
	}

	public CharSequence getTitle() {
		if (mCenterView != null) {
			return mCenterView.getText();
		}
		return "";
	}

	public void setTitleOnClickListener(OnClickListener listener) {
		mCenterView.setOnClickListener(listener);
	}

	public void setLeftView(int resId) {
		mLeftView.setText(resId);
	}

	public void setLeftViewCompoundDrawablesWithIntrinsicBounds(int l, int t, int r, int b) {
		mLeftView.setCompoundDrawablesWithIntrinsicBounds(l, t, r, b);
	}

	public void setLeftViewPadding(int left, int top, int right, int bottom) {
		mLeftView.setPadding(left, top, right, bottom);
	}

	public void setLeftView(CharSequence charSequence) {
		mLeftView.setText(charSequence);
	}

	public void setLeftViewOnClickListener(OnClickListener listener) {
		mLeftView.setOnClickListener(listener);
	}

	public void setLeftViewDrawablePadding(int padding) {
		mLeftView.setCompoundDrawablePadding(padding);
	}

	public TextView getRightView() {
		return mRightView;
	}

	public void setRightView(int resId) {
		mRightView.setText(resId);
	}

	public void setRightViewCompoundDrawablesWithIntrinsicBounds(int l, int t, int r, int b) {
		mRightView.setCompoundDrawablesWithIntrinsicBounds(l, t, r, b);
	}

	public void setRightViewPadding(int left, int top, int right, int bottom) {
		mRightView.setPadding(left, top, right, bottom);
	}

	public void setRightView(CharSequence charSequence) {
		mRightView.setText(charSequence);
		mRightView.setPadding(width15dp, 0, width15dp, 0);
	}

	public void setRightViewDrawablePadding(int padding) {
		mRightView.setCompoundDrawablePadding(padding);
	}

	public void setRightViewOnClickListener(OnClickListener listener) {
		mRightView.setOnClickListener(listener);
	}

	public void setRightViewEnabled(boolean isEnabled) {
		mRightView.setEnabled(isEnabled);
		if (isEnabled) {
			mRightView.setAlpha(1.0f);
		} else {
			mRightView.setAlpha(0.6f);
		}
	}

	public void showBadge() {
		mBadge.setVisibility(VISIBLE);
	}

	public void hideBadge() {
		mBadge.setVisibility(GONE);
	}
}
