package com.uzhu.project.view.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uzhu.project.R;


/**
 * Created by John on 2016/12/15 0015.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 10:38
 * UpdateDescribe
 */

public class AppTitle extends RelativeLayout {

    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    private int mBackgroundColor;

    public FrameLayout getFrameLayout() {
        return mFrameLayout;
    }



    private FrameLayout mFrameLayout;

    public TextView getTv_next() {
        return mTv_next;
    }

    public TextView mTv_next;

    public ImageView getIv_next() {
        return mIv_next;
    }

    public ImageView getTv_back() {
        return mTv_back;
    }

    public TextView getTv_title() {
        return mTv_title;
    }

    private ImageView mTv_back;
    private ImageView mIv_next;
    private TextView mTv_title;

    public AppTitle(Context context) {
        this(context, null);
    }

    public AppTitle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AppTitle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View.inflate(context, R.layout.layout_head_title, this);
        mTv_title = (TextView) findViewById(R.id.tv_app_title);
        mTv_next = (TextView) findViewById(R.id.tv_app_next);
        mTv_back = (ImageView) findViewById(R.id.iv_app_back);
        mIv_next = (ImageView) findViewById(R.id.iv_app_next);
        mFrameLayout = (FrameLayout) findViewById(R.id.mid_layout);

    }

    public void setTitle(String title) {
        mTv_title.setText(title);
    }

    public void setColor(int color) {
        mBackgroundColor = getResources().getColor(color);
        setBackgroundColor(mBackgroundColor);
    }

    public void setBackIconListener(OnClickListener listener) {
        mTv_back.setOnClickListener(listener);
    }
}
