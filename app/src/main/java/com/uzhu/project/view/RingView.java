package com.uzhu.project.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by John on 2016/12/8 0008.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 12:22
 * UpdateDescribe
 */
public class RingView extends View {

    private float mPercent = 0;

    public RingView(Context context) {
        this(context, null);
    }

    public RingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setPercentColor(int percentColor) {
        this.percentColor = percentColor;
    }

    public void setRingColor(int ringColor) {
        this.ringColor = ringColor;
    }

    /**
     * 进度的颜色
     */
    private int percentColor = Color.BLUE;

    /**
     * 环的颜色
     */
    private int ringColor = Color.GREEN;

    @Override
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    private int backgroundColor;

    /**
     * 画笔
     */
    private Paint paint;
    /**
     * 是否第一次
     */
    private boolean init = false;
    private float content_X;
    /**
     * 扇形中心点Y轴
     */
    private float content_Y;
    /**
     * 环形外半径
     */
    private float bigRadius;
    /**
     * 环形内半径
     */
    private float smallRadius;
    private int width;
    private int height;

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (!init) {
            initPaint();
        }
    }

    private void initPaint() {
        setPadding(0, 0, 0, 0);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        width = getMeasuredWidth();
        height = getMeasuredHeight();
        bigRadius = ((float) width / 2);
        smallRadius = bigRadius - dip2px(getContext(), 3f);
        content_X = (float) width / 2;
        content_Y = (float) height / 2;
        init = true;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setColor(ringColor);//ring的颜色
        canvas.drawCircle(content_X, content_Y, bigRadius, paint);
        RectF rectF = new RectF(content_X - bigRadius, content_Y - bigRadius, content_X + bigRadius,
                content_Y + bigRadius);
        paint.setColor(percentColor);
        canvas.drawArc(rectF, -90, mPercent, true, paint);
        paint.setColor(backgroundColor);
        canvas.drawCircle(content_X, content_Y, smallRadius, paint);
    }


    /**
     * @param startAngle 百分比
     */
    public void setAngle(float startAngle) {
        mPercent = (float) (startAngle * 3.6);
    }


    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}