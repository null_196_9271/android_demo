package com.uzhu.project.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.uzhu.project.R;
import com.uzhu.project.view.base.BaseLinearLayout;


/**
 * Created by John on 2016/12/21 0021.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 17:10
 * UpdateDescribe
 */

public class RingPercentItem extends BaseLinearLayout {

    private TextView mTv_state;
    private TextView mTv_percent;
    private RingView mRingView;
    public final static String TYPE_RED = "type_white";
    public final static String TYPE_NORMAL = "type_normal";
    public final static String TYPE_BLUE = "type_blue";
    private int mColor;

    public RingPercentItem(Context context) {
        super(context);
    }

    public RingPercentItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RingPercentItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mColor = ((ColorDrawable) getBackground()).getColor();

    }

    @Override
    protected void initView() {
        mTv_state = (TextView) findViewById(R.id.tv_state);
        mTv_percent = (TextView) findViewById(R.id.tv_percent);
        mRingView = (RingView) findViewById(R.id.rv_show);
        mRingView.setBackgroundColor(mColor);
    }


    public void setPercent(float value) {
        mTv_percent.setText(((int) (value+0.5)) + "%");
        mRingView.setAngle(value);
        mRingView.invalidate();
    }



    public void setData(float value, String state) {
        setPercent(value);
        mTv_state.setText(state);
    }

    public void setType(String type) {
        switch (type) {
            case TYPE_RED:
                mRingView.setBackgroundColor(getResources().getColor(R.color.app_red));
                int white = getResources().getColor(R.color.white);
                mTv_state.setTextColor(white);
                mTv_percent.setTextColor(white);
                mRingView.setRingColor(getResources().getColor(R.color.deep_red));
                mRingView.setPercentColor(white);
                break;
            case TYPE_BLUE:
                mRingView.setBackgroundColor(getResources().getColor(R.color.app_blue));
//                mRingView.setBackgroundColor(((ColorDrawable) getBackground()).getColor());
                int color = getResources().getColor(R.color.white);
                mTv_state.setTextColor(color);
                mTv_percent.setTextColor(color);
                mRingView.setRingColor(getResources().getColor(R.color.deep_blue));
                mRingView.setPercentColor(color);
                break;
            case TYPE_NORMAL:
                mRingView.setBackgroundColor(getResources().getColor(R.color.white));
                mTv_percent.setTextColor(getResources().getColor(R.color.gray));
                mTv_state.setTextColor(getResources().getColor(R.color.orange_08));
                mRingView.setPercentColor(getResources().getColor(R.color.light_green));
                mRingView.setRingColor(getResources().getColor(R.color.light_gray));
                break;
        }

    }

    @Override
    protected int getLayout() {
        return R.layout.item_ring_percent;
    }
}
