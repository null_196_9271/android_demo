package com.uzhu.project.view.ios;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.uzhu.project.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shard on 15/12/2.
 */
public class IOSActionSheet extends Dialog {
	private ListView mMenuItems;
	private ArrayList<String> mSheetItem = new ArrayList<>();
	private BaseAdapter mAdapter;
	private View mRootView;
	private TextView mTitle, cancelTv;
	private View titleLine, space;
	private Context context;

	private Animation mShowAnim;
	private Animation mDismissAnim;

	private boolean isDismissing;
	private MenuListener mMenuListener;

	public MenuListener getMenuListener() {
		return mMenuListener;
	}

	public void setMenuListener(MenuListener menuListener) {
		mMenuListener = menuListener;
	}

	public IOSActionSheet(Context context) {
		super(context, R.style.ActionSheetDialog);
		this.context = context;
		getWindow().setGravity(Gravity.BOTTOM);
		initView();
		initAnim(context);
	}

	public IOSActionSheet(Context context, String title, String cancelButtonTitle, String destructiveButtonTitle, List<String> otherButtonTitles) {
		super(context, R.style.ActionSheetDialog);
		this.context = context;
		getWindow().setGravity(Gravity.BOTTOM);
		initView(title, cancelButtonTitle, destructiveButtonTitle, otherButtonTitles);
		initAnim(context);
	}

	private void initView() {
		initView(null, null, null, null);
	}

	private void initView(String title, String cancelButtonTitle, final String destructiveButtonTitle, List<String> otherButtonTitles) {
		mRootView = View.inflate(context, R.layout.layout_action_sheet, null);
		mTitle = (TextView) mRootView.findViewById(R.id.tv_title);
		titleLine = mRootView.findViewById(R.id.line_title);
		space = mRootView.findViewById(R.id.space);
		cancelTv = (TextView) mRootView.findViewById(R.id.tv_cancel);
		if (cancelButtonTitle == null ||
				(title == null && destructiveButtonTitle == null &&
						(otherButtonTitles == null || otherButtonTitles.size() == 0))) {
			//上面或下面有一部分缺失，则去除中间空隙
			space.setVisibility(View.GONE);
		}
		if (cancelButtonTitle == null) {
			//不需要取消按钮
			cancelTv.setVisibility(View.GONE);
		} else {
			cancelTv.setText(cancelButtonTitle);
		}
		if (title == null) {
			//无标题
			mTitle.setVisibility(View.GONE);
			titleLine.setVisibility(View.GONE);
		} else if (destructiveButtonTitle == null && (otherButtonTitles == null || otherButtonTitles.size() == 0)) {
			//只有标题
			titleLine.setVisibility(View.GONE);
		} else {
			mTitle.setText(title);
		}
		mMenuItems = (ListView) mRootView.findViewById(R.id.lv_content);
//		mAdapter = new ArrayAdapter<>(context, R.layout.item_action_sheet);
		if (destructiveButtonTitle != null) {
			mSheetItem.add(destructiveButtonTitle);
		}
		if (otherButtonTitles != null)
			mSheetItem.addAll(otherButtonTitles);
		BaseAdapter adapter = new BaseAdapter() {
			@Override
			public int getCount() {
				return mSheetItem.size();
			}

			@Override
			public Object getItem(int position) {
				return mSheetItem.get(position);
			}

			@Override
			public long getItemId(int position) {
				return 0;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				LayoutInflater mInflater = (LayoutInflater) context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.item_action_sheet, null);
				TextView contentView = (TextView) convertView;
				if (destructiveButtonTitle != null && position == 0)
					contentView.setTextColor(context.getResources().getColor(R.color.app_red));
				contentView.setText(mSheetItem.get(position));
				return convertView;
			}
		};
		setListViewAdapter(adapter);
		this.setContentView(mRootView);
		cancelTv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if (mMenuListener != null) {
					mMenuListener.onCancel();
				}
			}
		});
		setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				dismiss();
				if (mMenuListener != null) {
					mMenuListener.onCancel();
				}
			}
		});
		mMenuItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				dismiss();
				if (mMenuListener != null) {
					if (mAdapter != null) {
						if (position < mSheetItem.size()) {
							//选中listview的内容
							mMenuListener.onItemSelected(position, mAdapter.getItem(position));
						}
					}
				}
			}
		});
		//dialog填充屏幕宽度
		WindowManager.LayoutParams wmlp = getWindow().getAttributes();
		wmlp.width = WindowManager.LayoutParams.MATCH_PARENT;
	}

	public void setListViewAdapter(BaseAdapter adapter) {
		this.mAdapter = adapter;
		mMenuItems.setAdapter(mAdapter);
	}

	public void setTitle(CharSequence charSequence) {
		if (mTitle != null) {
			mTitle.setText(charSequence);
		}
	}

	public void setTitle(int res) {
		if (mTitle != null) {
			mTitle.setText(res);
		}
	}

	private void initAnim(Context context) {
		mShowAnim = AnimationUtils.loadAnimation(context, R.anim.push_bottom_in);
		mDismissAnim = AnimationUtils.loadAnimation(context, R.anim.push_bottom_out);
		mDismissAnim.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				dismissMe();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
	}

	public void setActionSheetTitle(String title) {
		mTitle.setText(title);
	}

//	public IOSActionSheet addMenuItem(String items) {
//		mAdapter.add(items);
//		return this;
//	}

	public void toggle() {
		if (isShowing()) {
			dismiss();
		} else {
			show();
		}
	}

	@Override
	public void show() {
		mAdapter.notifyDataSetChanged();
		super.show();
		mRootView.startAnimation(mShowAnim);
	}

	@Override
	public void dismiss() {
		if (isDismissing) {
			return;
		}
		isDismissing = true;
		mRootView.startAnimation(mDismissAnim);
	}

	private void dismissMe() {
		super.dismiss();
		isDismissing = false;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			dismiss();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public interface MenuListener {
		void onItemSelected(int position, Object item);

		void onCancel();
	}
}
