package com.uzhu.project.view.ios;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uzhu.project.R;

/**
 * Created by John on 2017/2/13 0013.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 14:39
 * UpdateDescribe
 */

public class IOSCommonDialog extends Dialog {
    TextView mTvTitle;
    LinearLayout mFlTitle;
    TextView mTvShow;
    TextView mTvCancel;
    TextView mTvCommit;
    FrameLayout mFlContent;
    private CommonListener mListener;
    private String mTilte;
    private String mDesc;



    public void setDesc(String desc) {
        mDesc = desc;
    }


    public IOSCommonDialog(Context context, CommonListener listener) {
        this(context, null, null, listener);
    }

    public IOSCommonDialog(Context context, String title, String desc, CommonListener listener) {
        super(context);
        mTilte = title;
        mDesc = desc;
        mListener = listener;
        init(context);
    }

    private void init(Context context) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = View.inflate(context, R.layout.app_common_dialog, null);
        mTvTitle = (TextView) view.findViewById(R.id.tv_title);
        mFlTitle = (LinearLayout) view.findViewById(R.id.fl_title);
        mTvShow = (TextView) view.findViewById(R.id.tv_show);
        mTvCancel = (TextView) view.findViewById(R.id.tv_cancel);
        mTvCommit = (TextView) view.findViewById(R.id.tv_commit);
        mFlContent = (FrameLayout) view.findViewById(R.id.fl_content);
        if (mTilte != null) {
            mTvTitle.setText(mTilte);
        }
        mFlTitle.setVisibility(isTitleVisible() ? View.VISIBLE : View.GONE);
        if (setShowViewById() != -1) {
            mFlContent.removeAllViews();
            mFlContent.addView(View.inflate(context, setShowViewById(), null));
        } else {
            if (mDesc != null) {
                mTvShow.setText(mDesc);
            }
        }
        mTvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onDialogCommit();
                    if (commitNotDismiss()) {
                        return;
                    }
                }
                dismiss();
            }
        });

        if (!hasCancle()) {
            mTvCancel.setVisibility(View.GONE);
            view.findViewById(R.id.tv_line).setVisibility(View.GONE);
            mTvCommit.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.select_dialog_button));
        }
        mTvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onDialogCance();
                }
                dismiss();
            }
        });
        setContentView(view);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    protected boolean hasCancle() {
        return true;
    }

    //if  you set it  is true ,don't forget dissmiss
    protected boolean commitNotDismiss() {
        return false;
    }

    protected boolean isTitleVisible() {
        return true;
    }


    //子类继承重写
    protected int setShowViewById() {
        return -1;
    }


    private boolean commitNotDismiss = false;

    public interface CommonListener {

        void onDialogCommit();

        void onDialogCance();
    }
}
