package com.uzhu.project.controller.fragment;

import android.view.View;

import com.uzhu.project.R;
import com.uzhu.project.base.AppBaseFragment;
import com.uzhu.project.controller.activity.TestFragmentActivity;
import com.uzhu.project.service.AppServiceMediator;

/**
 * Created by John on 2017/2/10 0010.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 15:16
 * UpdateDescribe
 */
public class TestFragment extends AppBaseFragment<TestFragmentActivity> {
    @Override
    protected void initView(View view) {

    }

    @Override
    protected boolean needRefresh() {
        return true;
    }

    @Override
    protected void loadServiceData(AppServiceMediator API) {
        super.loadServiceData(API);
        refreshData("ss");
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int createViewByLayoutId() {
        return R.layout.activity_scrollview;
    }
}
