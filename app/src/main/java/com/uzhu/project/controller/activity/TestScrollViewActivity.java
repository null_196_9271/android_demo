package com.uzhu.project.controller.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.uzhu.project.R;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.base.AppBaseCommonActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by John on 2017/2/9 0009.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 17:13
 * UpdateDescribe
 */
public class TestScrollViewActivity extends AppBaseCommonActivity {
    @Bind(R.id.bt1)
    Button mBt1;

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_scrollview;
    }

    @Override
    protected void initID() {

    }

    @Override
    protected boolean needRefresh() {
        return true;
    }

    @Override
    protected int getBottomView() {
        return R.layout.login_bottom;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        mBt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppApplication.getInstance().checkVersion();
            }
        });
    }

    @Override
    protected void onSRLayoutReFresh() {
        super.onSRLayoutReFresh();
        refreshData("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
}
