package com.uzhu.project.controller.activity;

import android.widget.TextView;

import com.uzhu.project.R;
import com.uzhu.project.adapter.abslistview.base.ViewHolder;
import com.uzhu.project.base.AppBaseListActivity;
import com.uzhu.project.controller.viewmodel.TestListViewActivityViewModel;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.widget.xlistview.XListView;

import java.util.List;

/**
 * Created by John on 2017/2/10 0010.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 10:28
 * UpdateDescribe
 */

public class TestListViewActivity extends AppBaseListActivity<TestListViewActivityViewModel,String> {
    @Override
    protected List mDataList(TestListViewActivityViewModel viewModel) {
        for (int i = 0; i < 10; i++) {
            mViewModel.data.add("abc" + mViewModel.data.size());
        }
        return mViewModel.data;
    }

    @Override
    protected void loadServiceAPI(AppServiceMediator appServiceMediator, Page page, Params params) {

    }

    @Override
    protected void onSRLayoutReFresh() {
        super.onSRLayoutReFresh();
        mViewModel.data.clear();
        for (int i = 0; i < 10; i++) {
            mViewModel.data.add("abc" + mViewModel.data.size());
        }
        mLv_show.getBaseAdapter().notifyDataSetChanged();
        refreshData("sss");
    }

    @Override
    protected void onListViewLoadMore() {
        super.onListViewLoadMore();
        mLv_show.stopLoadMore();
        for (int i = 0; i < 2; i++) {
            mViewModel.data.add("abc" + mViewModel.data.size());
        }
        mLv_show.getBaseAdapter().notifyDataSetChanged();
    }

    @Override
    protected void setItemView(ViewHolder viewHolder, String item, int position) {
        viewHolder.setText(R.id.tv_show, item);
    }

    @Override
    protected int itemViewLayout() {
        return R.layout.item_test_list;
    }

    @Override
    protected void setHeadOrBottomListView(XListView lv_show) {
        super.setHeadOrBottomListView(lv_show);
        TextView v = new TextView(getContextByThis());
        v.setText("我是头部");
        lv_show.addHeaderView(v);
    }
}
