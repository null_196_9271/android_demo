package com.uzhu.project.controller.inf;

/**
 * Created by John on 2016/12/5 0005.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 18:02
 * UpdateDescribe
 */
public interface RefreshDataForFragment {
    void refreshData(String fieldName);
    void alertMessage(String fieldName);
}
