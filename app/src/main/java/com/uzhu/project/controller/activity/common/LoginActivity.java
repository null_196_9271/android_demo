package com.uzhu.project.controller.activity.common;

import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mvvm.framework.common.NotificationCenter;
import com.mvvm.framework.common.NotificationListener;
import com.mvvm.framework.route.Route;
import com.mvvm.framework.util.CommonUtil;
import com.mvvm.framework.util.LogUtil;
import com.mvvm.framework.util.ToastUtil;
import com.umeng.analytics.MobclickAgent;
import com.uzhu.project.R;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.base.AppBaseCommonActivity;
import com.uzhu.project.base.cache.UserCenter;
import com.uzhu.project.controller.viewmodel.LoginViewModel;
import com.uzhu.project.util.StrUtils;
import com.uzhu.project.view.base.AppTitle;
import com.uzhu.project.widget.ClearEditText;

public class LoginActivity extends AppBaseCommonActivity<LoginViewModel> implements
		OnClickListener {
	private Button envConfigBtn;
	private TextView loginBtn;
	private ClearEditText loginAccount;
	private ClearEditText loginPassword;
	private CheckBox pwdSwitch;

//	private LoginViewModel viewModel;

	@Override
	protected boolean isSetStateBar() {
		return false;
	}

	private TextWatcher watcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		}

		@Override
		public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

		}

		@Override
		public void afterTextChanged(Editable editable) {
			if (loginAccount.getText().length() > 0 && loginPassword.getText().length() > 0) {
				loginBtn.setEnabled(true);
			} else {
				loginBtn.setEnabled(false);
			}
		}
	};
	private NotificationListener mObserver;

	@Override
	protected int getBottomView() {
		return R.layout.login_bottom;
	}

	@Override
	protected int contentViewByLayoutId() {
		return R.layout.activity_login;
	}

	@Override
	protected void initID() {
		initLoginView();
	}

	@Override
	protected void setAppTitle(AppTitle appTitle) {
		appTitle.setVisibility(View.GONE);
	}

	@Override
	protected void initData() {
		mObserver = new NotificationListener() {
			@Override
			public void onNotificationReceived(NotificationCenter.Notification notification) {
				finish();
			}
		};
		NotificationCenter.defaultCenter().addObserver(mObserver,"registFinish",null);
	}

	@Override
	protected void initView() {
		setView();
	}

	/**
	 * 初始化控件
	 */
	public void initLoginView() {
		envConfigBtn = (Button) findViewById(R.id.btn_env_config);
		envConfigBtn.setOnClickListener(this);

		loginBtn = (TextView) findViewById(R.id.btn_login);
		loginBtn.setEnabled(false);

		loginAccount = (ClearEditText) findViewById(R.id.login_mobile_phone);//账号
		loginPassword = (ClearEditText) findViewById(R.id.login_password);//密码

		loginAccount.addTextChangedListener(watcher);
		loginPassword.addTextChangedListener(watcher);
		//显示隐藏密码
		findViewById(R.id.btn_env_config).setVisibility(AppApplication.getInstance().isDeveloping()? View.VISIBLE: View.GONE);
		pwdSwitch = (CheckBox) findViewById(R.id.cb_hideshow);

		pwdSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					//如果选中，显示密码
					loginPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

				} else {
					//否则隐藏密码
					loginPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
				}
//				pwdSwitch.setChecked(!pwdSwitch.isChecked());

			}
		});
		//检查版本更新
		AppApplication.getInstance().checkVersion();
	}

	private void setView() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			getWindow().setStatusBarColor(getResources().getColor(R.color.default_line_color));
		}
		// 若登录过则默认填充账号,而且是记住密码的话,那就需要直接设置进去,
		// 如果没有设置密码, 那么就需要输入密码来得到
		String userAccount = UserCenter.instance().getUserAccout();
		if (userAccount != null && userAccount.length() > 0) {
			loginAccount.setText(userAccount);
		}
		//默认隐藏密码
		loginPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
		setSwipeBackEnable(false);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void refreshData(String fieldName) {
		super.refreshData(fieldName);
		dismissProgress();
		switch (fieldName) {
			case LoginViewModel.FIELDNAME_USER:
				LogUtil.d(mViewModel.user.toString());
				//登录后进行账号统计
				MobclickAgent.onProfileSignIn(loginAccount.getText().toString());
				setResult(RESULT_OK,null);
				finish();
				break;

			default:
				break;
		}
	}

	@Override
	public void alertMessage(String fieldName, int errorCode, String errorMsg) {
		super.alertMessage(fieldName, errorCode, errorMsg);
		dismissProgress();
	}

	@Override
	protected void onDestroy() {
		NotificationCenter.defaultCenter().removeObserver(mObserver);
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		CommonUtil.keyBoardCancel(LoginActivity.this);
		switch (v.getId()) {
			case R.id.btn_login:
				if (StrUtils.stringIsEmpty(loginAccount.getText().toString())) {
					ToastUtil.show(context, "请输入账号");
					return;
				}
				if (StrUtils.stringIsEmpty(loginPassword.getText().toString())) {
					ToastUtil.show(context, context.getResources().getString(R.string.empty_password));
					return;
				}

				//				if (!futureIsExist(fieldName)) {
//					showProgress();
//					addFutureToMap(fieldName,viewModel.addServiceMediatorAPI(AppServiceMediator.sharedInstance().login(loginAccount.getText().toString().trim(),loginPassword.getText().toString().trim()),fieldName));
//				}
				addService(LoginViewModel.FIELDNAME_USER,mApi.login(loginAccount.getText().toString().trim(),loginPassword.getText().toString().trim()));
				break;
			case R.id.btn_env_config:
				//环境配置
				if (AppApplication.getInstance().isDeveloping()) {
					Route.nextController(LoginActivity.this, EnvironmentConfigActivity.class);
				}
				break;
			default:
				break;
		}
	}


}
