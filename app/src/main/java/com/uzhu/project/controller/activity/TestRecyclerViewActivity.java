package com.uzhu.project.controller.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.uzhu.project.R;
import com.uzhu.project.adapter.recyclerview.CommonAdapter;
import com.uzhu.project.adapter.recyclerview.base.ViewHolder;
import com.uzhu.project.adapter.recyclerview.wrapper.LoadMoreWrapper;
import com.uzhu.project.base.AppBaseCommonActivity;
import com.uzhu.project.controller.viewmodel.TestRecyclerViewActivityViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by John on 2017/2/17 0017.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 11:16
 * UpdateDescribe
 */
public class TestRecyclerViewActivity extends AppBaseCommonActivity<TestRecyclerViewActivityViewModel> {
    @Bind(R.id.rv_1)
    RecyclerView mRecyclerView;
    private List<String> mData;

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_recyclerview;
    }

    @Override
    protected void initData() {
        mData = new ArrayList<>();
        mData.add("abc");
        mData.add("abc");
        mData.add("abc");
        mData.add("abc");
        mData.add("abc");
    }

    @Override
    protected void initView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new LoadMoreWrapper<String>(new CommonAdapter<String>(this,R.layout.item_test_list,mData) {

            @Override
            protected void convert(ViewHolder holder, String s, int position) {
                holder.setText(R.id.tv_show, s+position);
            }
        }));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
}
