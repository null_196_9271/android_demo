package com.uzhu.project.controller.fragment;

import android.widget.TextView;

import com.uzhu.project.R;
import com.uzhu.project.adapter.abslistview.base.ViewHolder;
import com.uzhu.project.base.AppBaseListFragment;
import com.uzhu.project.controller.activity.TestFragmentActivity;
import com.uzhu.project.controller.viewmodel.TestFragmentActivityViewModel;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.Page;
import com.uzhu.project.service.models.Params;
import com.uzhu.project.widget.xlistview.XListView;

import java.util.List;

/**
 * Created by John on 2017/2/10 0010.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 16:05
 * UpdateDescribe
 */
public class TestListFragment extends AppBaseListFragment<TestFragmentActivity,TestFragmentActivityViewModel,String>{
    @Override
    protected List<String> mDataList(TestFragmentActivityViewModel viewModel) {
        for (int i = 0; i < 10; i++) {
            viewModel.data.add("abc" + viewModel.data.size());
        }
        return viewModel.data;
    }

    @Override
    protected void loadServiceAPI(AppServiceMediator appServiceMediator, Page page, Params params) {

    }


    @Override
    protected void onListViewLoadMore() {
        super.onListViewLoadMore();
        mLv_show.stopLoadMore();
        for (int i = 0; i < 2; i++) {
            mActivity.mViewModel.data.add("abc" +  mActivity.mViewModel.data.size());
        }
        mLv_show.getBaseAdapter().notifyDataSetChanged();
    }
    @Override
    protected void onSRLayoutReFresh() {
        super.onSRLayoutReFresh();
        mActivity.mViewModel.data.clear();
        for (int i = 0; i < 10; i++) {
            mActivity.mViewModel.data.add("abc" +mActivity. mViewModel.data.size());
        }
        mLv_show.getBaseAdapter().notifyDataSetChanged();
        refreshData("abc");
    }

    @Override
    protected void setItemView(ViewHolder viewHolder, String item, int position) {
        viewHolder.setText(R.id.tv_show, item);
    }

    @Override
    protected int itemViewLayout() {
        return R.layout.item_test_list;
    }

    @Override
    protected void setHeadOrBottomListView(XListView lv_show) {
        super.setHeadOrBottomListView(lv_show);
        TextView v = new TextView(mActivity);
        v.setText("我是头部");
        lv_show.addHeaderView(v);
    }
}
