package com.uzhu.project.controller.activity.common;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.LinearLayout;

import com.mvvm.framework.route.Route;
import com.mvvm.framework.util.PersistenceUtil;
import com.mvvm.framework.util.ToastUtil;
import com.uzhu.project.R;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.base.AppBaseCommonActivity;
import com.uzhu.project.base.DataManager;
import com.uzhu.project.controller.activity.MainActivity;
import com.uzhu.project.view.base.AppTitle;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppBaseCommonActivity {
    private final static int DEFAULT_GUIDE = 0; //若要显示引导页，每次该值请设置为versionCode-1,初始为0
    private static final int HAND_FINISH = 101;
    private LinearLayout mLinearLayout;


    @Override
    protected boolean needRefresh() {
        return false;
    }

    @Override
    protected void setAppTitle(AppTitle appTitle) {
        super.setAppTitle(appTitle);
        appTitle.setVisibility(View.GONE);
    }

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initID() {
        mLinearLayout = (LinearLayout) findViewById(R.id.ll_splash);
    }

    @Override
    protected void initData() {
        DataManager.getInstance().initData(this);
    }


    @Override
    protected void initView() {
        if (AppApplication.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            setNewSplash();
            fade();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
    }

    private void setNewSplash() {
        try {
            File dir = creatSDDir("ishouzu");
            File imageFile = new File(dir,
                    "splash.png");
            InputStream inputStream = new FileInputStream(imageFile);
            long length = imageFile.length();
            byte[] bytes = new byte[(int) length];
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length &&
                    (numRead = inputStream.read(bytes, offset, bytes.length - offset)) >= 0) {
                offset += numRead;
            }
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            BitmapDrawable bd = new BitmapDrawable(this.getResources(), bitmap);
            mLinearLayout.setBackgroundDrawable(bd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            setNewSplash();
            fade();
        } else {
            ToastUtil.show(getContextByThis(), "权限错误!部分功能将无法正常运行");
//            finish();
            setNewSplash();
            fade();
        }
    }

    private void fade() {
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                int lastVersion = PersistenceUtil.getInt("versionCode", DEFAULT_GUIDE);
                if (lastVersion <= DEFAULT_GUIDE) {
                    Route.nextController(SplashActivity.this, GuideActivity.class);
                    try {
                        PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        lastVersion = packageInfo.versionCode;
                        PersistenceUtil.saveInt("versionCode", lastVersion);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    Route.nextController(SplashActivity.this, MainActivity.class);
                }
                mHandler.sendEmptyMessageDelayed(HAND_FINISH, 50);
            }
        }, 1000);
    }

    protected Handler mHandler = new MyHandler(this);

    /*
* 在SD卡上创建目录
*/
    public File creatSDDir(String dirName) {
        File dir = new File(Environment.getExternalStorageDirectory() + File.separator + dirName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }


    private static class MyHandler extends Handler {
        WeakReference<Activity> activity;

        MyHandler(Activity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Activity activity = this.activity.get();
            if (activity != null) {
                activity.finish();
            }
        }
    }
}
