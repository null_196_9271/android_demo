package com.uzhu.project.controller.viewmodel;

import com.mvvm.framework.service.ServiceFuture;
import com.uzhu.project.base.AppViewModel;
import com.uzhu.project.base.cache.UserCenter;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.User;
import com.uzhu.project.util.AppConfig;

public class LoginViewModel extends AppViewModel {
	public static final String FIELDNAME_SMSCODE = "smsCode";
	public static final String FIELDNAME_USER = "user";
	public User user;

	@Override
	public void requestSuccess(String fieldName) {
		if (fieldName.equals(FIELDNAME_USER)) {
			// 数据处理
			if (user != null) {
				setUserConfig();
			} else {
				showErrorLog("user");
			}
		}
		super.requestSuccess(fieldName);
	}

	private void setUserConfig() {
		UserCenter.instance().setUser(user);
		AppConfig.setUserToken(user.userToken);
		UserCenter.instance().setUserAccount(user.accountName);
//        getTenantInfo();
	}

//	public ServiceFuture login(String accountName, String loginPassword) {
//		ServiceFuture serviceFuture = AppServiceMediator.sharedInstance().login(accountName, loginPassword);
//		serviceFuture.addServiceListener(FIELDNAME_USER, this);
//		return serviceFuture;
//	}

	//获取验证码
	public ServiceFuture getSMSCode(String mobile, String smsType) {
		ServiceFuture<?> serviceFuture = AppServiceMediator.sharedInstance().getSMSCode(mobile, smsType);
		serviceFuture.addServiceListener(FIELDNAME_SMSCODE, this);
		return serviceFuture;
	}
}
