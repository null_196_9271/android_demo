package com.uzhu.project.controller.activity;

import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mvvm.framework.route.Route;
import com.mvvm.framework.util.ToastUtil;
import com.uzhu.project.R;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.base.AppBaseCommonActivity;
import com.uzhu.project.controller.activity.common.LoginActivity;
import com.uzhu.project.view.base.AppTitle;
import com.uzhu.project.view.ios.IOSCommonDialog;

import junit.framework.Test;

import java.io.IOException;

import butterknife.Bind;

public class MainActivity extends AppBaseCommonActivity {


    @Bind(R.id.bt1)
    Button bt1;
    @Bind(R.id.bt2)
    Button bt2;
    @Bind(R.id.bt3)
    Button bt3;
    @Bind(R.id.bt4)
    Button bt4;
    @Bind(R.id.bt5)
    Button mBt5;
//    @Bind(R.id.sw_1)
//    SwitchButton mSw1;
    @Bind(R.id.bt6)
    Button mBt6;
    @Bind(R.id.bt7)
    Button mBt7;
    @Bind(R.id.bt8)
    Button mBt8;
    @Bind(R.id.bt9)
    Button mBt9;
    @Bind(R.id.bt10)
    Button mBt10;
    @Bind(R.id.bt11)
    Button mBt11;
    @Bind(R.id.bt12)
    Button mBt12;
    @Bind(R.id.activity_main)
    LinearLayout mActivityMain;

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initID() {

    }

    @Override
    protected void onSRLayoutReFresh() {
        super.onSRLayoutReFresh();
        refreshData("");
    }

    @Override
    protected boolean needRefresh() {
        return true;
    }

    @Override
    protected void initData() {
//        AppApplication.getInstance().checkVersion();
    }

    @Override
    protected void initView() {
        setSwipeBackEnable(false);
        bt1.setText("LoginActivity");
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route.nextController(getContextByThis(), LoginActivity.class);
            }
        });
        bt2.setText("TestScrollViewActivity");
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route.nextController(getContextByThis(), TestScrollViewActivity.class);
            }
        });
        bt3.setText("TestListViewActivity");
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route.nextController(getContextByThis(), TestListViewActivity.class);
            }
        });
        bt4.setText("TestFragmentActivity");
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route.nextController(getContextByThis(), TestFragmentActivity.class);
            }
        });
        mBt5.setText("IOSCommonDialog");
        mBt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IOSCommonDialog dialog = new IOSCommonDialog(getContextByThis(), "Abc", "Desc", new IOSCommonDialog.CommonListener() {
                    @Override
                    public void onDialogCommit() {
                        ToastUtil.show(getContextByThis(), "提交了");
                    }

                    @Override
                    public void onDialogCance() {
                        ToastUtil.show(getContextByThis(), "取消了");
                    }

                });

                dialog.show();
            }
        });
        mBt6.setText("热修复技术");
        mBt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updata();
            }
        });
        mBt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.show(getContextByThis(), "123");
            }
        });
        mBt8.setText("checkVersion");
        mBt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppApplication.getInstance().checkVersion();
            }
        });
        mBt9.setText("TestRecyclerViewActivity");
        mBt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route.nextController(getContextByThis(), TestRecyclerViewActivity.class);
            }
        });
        mBt10.setText("测试程序崩溃");
        mBt10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = 2 / 0;
            }
        });
//        mSw1.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
//                ToastUtil.show(getContextByThis(), isChecked ? "checked" : "No");
//            }
//        });
        mBt11.setText("Android Circular Progress Button ");
        mBt11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Route.nextController(getContextByThis(), TestCircularButtonActivity.class);
            }
        });
    }

    private static final String APATCH_PATH = "/fix.apatch";

    private void updata() {
        String patchFileStr = Environment.getExternalStorageDirectory().getAbsolutePath() + APATCH_PATH;
        try {
            AppApplication.mPatchManager.addPatch(patchFileStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setAppTitle(AppTitle appTitle) {
        super.setAppTitle(appTitle);
        appTitle.setColor(R.color.theme_color);
    }


}
