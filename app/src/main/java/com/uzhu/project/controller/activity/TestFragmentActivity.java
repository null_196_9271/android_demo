package com.uzhu.project.controller.activity;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.uzhu.project.R;
import com.uzhu.project.adapter.MainPagerAdapter;
import com.uzhu.project.base.AppBaseCommonActivity;
import com.uzhu.project.controller.viewmodel.TestFragmentActivityViewModel;
import com.uzhu.project.view.NoScrollViewPager;
import com.uzhu.project.view.base.AppTitle;

import butterknife.Bind;

/**
 * Created by John on 2017/2/10 0010.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 14:48
 * UpdateDescribe
 */
public class TestFragmentActivity extends AppBaseCommonActivity<TestFragmentActivityViewModel> {


    @Bind(R.id.vp_main)
    NoScrollViewPager mVpMain;
    @Bind(R.id.rb_main)
    RadioButton mRbMain;
    @Bind(R.id.rb_finance)
    RadioButton mRbFinance;
    @Bind(R.id.rb_me)
    RadioButton mRbMe;
    @Bind(R.id.radio_group)
    RadioGroup mRadioGroup;

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_app_main;
    }

    @Override
    protected void initData() {

    }


    @Override
    protected void initView() {
        mVpMain.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));
        mVpMain.setOffscreenPageLimit(5);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_main:
                        mVpMain.setCurrentItem(0, false);
                        break;
                    case R.id.rb_finance:
                        mVpMain.setCurrentItem(1, false);
                        break;
                    case R.id.rb_me:
                        mVpMain.setCurrentItem(2, false);
//						logout();
                        break;
                }
            }
        });
    }

    @Override
    protected boolean needRefresh() {
        return false;
    }

    @Override
    protected void setAppTitle(AppTitle appTitle) {
        super.setAppTitle(appTitle);
        appTitle.setVisibility(View.GONE);
    }
}
