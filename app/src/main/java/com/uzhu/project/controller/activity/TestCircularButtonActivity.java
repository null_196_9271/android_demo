package com.uzhu.project.controller.activity;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.dd.CircularProgressButton;
import com.uzhu.project.R;
import com.uzhu.project.base.AppBaseCommonActivity;
import com.uzhu.project.controller.viewmodel.TestCircularButtonActivityViewModel;

import butterknife.Bind;

/**
 * Created by John on 2017/5/18 0018.
 * Describe
 * <p>
 * Updater John
 * UpdateTime 15:57
 * UpdateDescribe
 */

public class TestCircularButtonActivity extends AppBaseCommonActivity<TestCircularButtonActivityViewModel> {


    @Bind(R.id.circularButton1)
    CircularProgressButton mCircularButton1;
    @Bind(R.id.circularButton2)
    CircularProgressButton mCircularButton2;
    @Bind(R.id.circularButton3)
    CircularProgressButton mCircularButton3;
    @Bind(R.id.circularButton4)
    CircularProgressButton mCircularButton4;
    @Bind(R.id.circularButton5)
    CircularProgressButton mCircularButton5;
    @Bind(R.id.circularButton6)
    CircularProgressButton mCircularButton6;
    @Bind(R.id.circularButton7)
    CircularProgressButton mCircularButton7;
    @Bind(R.id.circularButton8)
    CircularProgressButton mCircularButton8;
    @Bind(R.id.circularButton9)
    CircularProgressButton mCircularButton9;

    @Override
    protected int contentViewByLayoutId() {
        return R.layout.activity_circular_button;
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        mCircularButton1.setText("button1");
        mCircularButton2.setText("button2");
        mCircularButton3.setText("button3");
        mCircularButton4.setText("button4");
        mCircularButton5.setText("button5");
        mCircularButton6.setText("button6");
        mCircularButton7.setText("button7");
        mCircularButton8.setText("button8");
        mCircularButton9.setText("button9");
        mCircularButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCircularButton1.getProgress() == 0) {
                    simulateSuccessProgress(mCircularButton1);
                } else {
                    mCircularButton1.setProgress(0);
                }
            }
        });
    }

    private void simulateSuccessProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }
}
