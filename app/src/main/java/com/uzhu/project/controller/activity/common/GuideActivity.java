package com.uzhu.project.controller.activity.common;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mvvm.framework.route.Route;
import com.uzhu.project.R;
import com.uzhu.project.base.AppBaseFragmentActivity;
import com.uzhu.project.controller.activity.MainActivity;
import com.uzhu.project.util.UiUtil;
import com.uzhu.project.widget.BaseButton;

/**
 * Created by chenhainiao on 15/12/31.
 */
public class GuideActivity extends AppBaseFragmentActivity {
    private ViewPager mGuideViewPager;
    private RadioGroup mGuideRadioGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        initActivityData();
        initActivityView();
    }
    protected void initActivityView() {
        mGuideViewPager = (ViewPager) findViewById(R.id.viewpager_guide);
        mGuideViewPager.setAdapter(new GuideAdapter());
        mGuideViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mGuideRadioGroup.check(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mGuideRadioGroup = (RadioGroup) findViewById(R.id.rg_guide);
        mGuideRadioGroup.setEnabled(false);
        int size = images.length;
        for (int i = 0;i < size;i++) {
            RadioButton radioButton = new RadioButton(GuideActivity.this);
            int margin = UiUtil.dp2px(this, 7);
            int width6dp = UiUtil.dp2px(this, 14);
            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(width6dp, width6dp);
            if (i != 0) {
                layoutParams.setMargins(margin, 0, 0, 0);
            }
            radioButton.setLayoutParams(layoutParams);
            radioButton.setBackgroundResource(R.drawable.selector_guide_dot);
            radioButton.setButtonDrawable(new ColorDrawable(Color.TRANSPARENT));
            radioButton.setId(i);
            mGuideRadioGroup.addView(radioButton);
        }
        mGuideRadioGroup.check(0);
        setSwipeBackEnable(false);
    }
    private int[] images;
    private void initActivityData() {
        images = new int[]{R.mipmap.guide1, R.mipmap.guide2, R.mipmap.guide3,R.mipmap.guide4};
    }
    private class GuideAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(GuideActivity.this).inflate(R.layout.guide_viewpager_item, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
            BaseButton button = (BaseButton) view.findViewById(R.id.btn_start);
            imageView.setImageResource(images[position]);
            if (position == images.length - 1) {
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Route.nextController(GuideActivity.this, MainActivity.class);
                        GuideActivity.this.finish();
                    }
                });
            } else {
                button.setVisibility(View.GONE);
            }
            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            ImageView imageView = (ImageView) view.findViewById(R.id.imageview);
            imageView.setImageBitmap(null);
            container.removeView(view);
        }
    }
}
