package com.uzhu.project.controller.activity.common;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mvvm.framework.service.MobileHead;
import com.mvvm.framework.service.ServiceFuture;
import com.mvvm.framework.service.task.OnExecuteListener;
import com.mvvm.framework.util.PersistenceUtil;
import com.mvvm.framework.util.ToastUtil;
import com.uzhu.project.R;
import com.uzhu.project.base.AppApplication;
import com.uzhu.project.base.AppBaseFragmentActivity;
import com.uzhu.project.base.cache.EnvConfigLoader;
import com.uzhu.project.service.AppServiceMediator;
import com.uzhu.project.service.models.EnvConfing;
import com.uzhu.project.service.models.Environment;
import com.uzhu.project.util.AppConfig;
import com.uzhu.project.widget.dialog.CustomDialog;

public class EnvironmentConfigActivity extends AppBaseFragmentActivity {
	private Environment environment;
	private ListView environmentConfigList;
	private EnvConfigListAdapter envConfigListAdapter;

	private Dialog configDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_environment_config);
		setSwipeBackEnable(true);
		initData();
		initView();
	}

	public void initData() {
		environment = AppConfig.env;
	}

	public void initView() {
		environmentConfigList = (ListView) findViewById(R.id.activity_environment_config_list);
		envConfigListAdapter = new EnvConfigListAdapter(this, environment);
		environmentConfigList.setAdapter(envConfigListAdapter);

		environmentConfigList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				showProgress();
				EnvConfing envConfing = (EnvConfing) adapterView.getAdapter().getItem(i);
				if (AppConfig.env.getCurrEnvironment().equals(envConfing.configTitle)) {
					dismissProgress();
					return;
				}
				environment.setCurrEnvironment(envConfing.configTitle);
				changeEnvionment();

			}
		});

		environmentConfigList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
				ToastUtil.show(EnvironmentConfigActivity.this, "onItemLongClick");
				EnvConfing envConfing = (EnvConfing) adapterView.getAdapter().getItem(i);
				if (envConfing.configTitle.equals("DEV")) {
					changeEnvConfig(envConfing);
				} else {
					ToastUtil.show(EnvironmentConfigActivity.this, "选择的配置不能修改，只有dev的配置可以修改！");
				}
				return true;
			}
		});

	}

	public void setView() {

	}

	public Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case 0:
					dismissProgress();
					envConfigListAdapter = new EnvConfigListAdapter(EnvironmentConfigActivity.this, environment);
					environmentConfigList.setAdapter(envConfigListAdapter);
					//注销掉当前登录状态
					AppConfig.clearLoginState();
					//重设环境
					AppConfig.env = environment;
					AppServiceMediator.sharedInstance().configAddress();
					//清空clientId并重新注册设备和开启推送
					MobileHead.getMobileHead().clientId = "";
					PersistenceUtil.saveObjectToSharePreferences(AppServiceMediator.CLIENT_ID, "");
					//重新注册设备
					ServiceFuture serviceFuture = AppServiceMediator.sharedInstance().doClientRegister(EnvironmentConfigActivity.this);
					serviceFuture.addServiceListener("", new OnExecuteListener() {
						@Override
						public void onPreExecute() {

						}

						@Override
						public void onExecuteSuccess(String s, Object o) {
							String clientId = (String) o;
							MobileHead.getMobileHead().clientId = clientId;
							PersistenceUtil.saveObjectToSharePreferences(AppServiceMediator.CLIENT_ID, clientId);
							// 若已经注册过设备，直接开启推送功能
							AppApplication.getInstance().mExampleConfig.initMsgPush();
						}

						@Override
						public void onExecuteFailed(String s, int i, String s1) {

						}
					});
					finish();
					break;
				default:
					break;
			}

			super.handleMessage(msg);
		}
	};

	public class EnvConfigListAdapter extends BaseAdapter {
		private Context mContext;
		private Environment environment;
		private LayoutInflater inflater;
		private String currEnvConfig;

		public EnvConfigListAdapter(Context mContext, Environment environment) {
			this.mContext = mContext;
			this.environment = environment;
			this.currEnvConfig = environment.getCurrEnvironment();
			inflater = LayoutInflater.from(this.mContext);
		}

		public void changeEnvironment(Environment environment) {
			this.environment = environment;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return environment.getServerURLs().size();
		}

		@Override
		public Object getItem(int i) {
			return environment.getServerURLs().get(i);
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getView(int i, View view, ViewGroup viewGroup) {
			ViewHodel viewHodel;
			if (view == null) {
				viewHodel = new ViewHodel();
				view = inflater.inflate(R.layout.item_listview_environment_config, null);
				viewHodel.configLayout = (LinearLayout) view.findViewById(R.id.item_config_layout);
				viewHodel.configTitle = (TextView) view.findViewById(R.id.item_config_title);
				viewHodel.configValue = (TextView) view.findViewById(R.id.item_config_value);
				viewHodel.isChecked = (ImageView) view.findViewById(R.id.item_config_isChecked);
				view.setTag(viewHodel);
			} else {
				viewHodel = (ViewHodel) view.getTag();
			}

			EnvConfing envConfing = (EnvConfing) getItem(i);
			viewHodel.configTitle.setText(envConfing.configTitle);
			viewHodel.configValue.setText(envConfing.configValue);

			viewHodel.configLayout.setBackgroundResource(R.color.white);
			viewHodel.configTitle.setTextColor(mContext.getResources().getColor(R.color.default_gary06_pressed_white_color));
			viewHodel.configValue.setTextColor(mContext.getResources().getColor(R.color.default_gary08_pressed_white_color));


			if (currEnvConfig.equals(envConfing.configTitle)) {
				viewHodel.isChecked.setVisibility(View.VISIBLE);
			} else {
				viewHodel.isChecked.setVisibility(View.GONE);
			}

			return view;
		}
	}

	static class ViewHodel {
		TextView configTitle;
		TextView configValue;
		LinearLayout configLayout;
		ImageView isChecked;
	}


	@Override
	public void onBackPressed() {
		finish();
	}


	public void changeEnvConfig(EnvConfing envConfing) {
		View configView = LayoutInflater.from(EnvironmentConfigActivity.this).inflate(
				R.layout.view_dialog_change_environment, null);
		TextView config = (TextView) configView.findViewById(R.id.config);
		final EditText ipEdit = (EditText) configView.findViewById(R.id.change_config_ip);
		final EditText portEdit = (EditText) configView.findViewById(R.id.change_config_port);
		config.setText(envConfing.configTitle + ":" + envConfing.configValue);
		String url = envConfing.configValue;
		String ip = url.substring(0, url.lastIndexOf(":"));
		String port = url.substring(url.lastIndexOf(":") + 1);
		ipEdit.setText(ip);
		portEdit.setText(port);
		CustomDialog.Builder customBuilder = new CustomDialog.Builder(EnvironmentConfigActivity.this);
		customBuilder
				.setTitle("修改配置")
				.setContentView(configView)
				.setNegativeButton("否",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
												int which) {
								configDialog.dismiss();
							}
						})
				.setPositiveButton("是",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
												int which) {
								if (ipEdit.getText().toString().trim().isEmpty()) {
									ToastUtil.show(EnvironmentConfigActivity.this, "请输入ip地址");
									return;
								}
								String port = "";
								if (!portEdit.getText().toString().trim().isEmpty()) {
									port = ":" + portEdit.getText().toString().trim();
								}
								String newDev = ipEdit.getText().toString().trim() + port;

								for (EnvConfing item :
										environment.getServerURLs()) {
									if (item.getConfigTitle().equals("DEV")) {
										item.setConfigValue(newDev);
										break;
									}
								}

								for (EnvConfing item :
										environment.getSecurityURLs()) {
									if (item.getConfigTitle().equals("DEV")) {
										item.setConfigValue(newDev);
										break;
									}
								}

								changeEnvionment();
								configDialog.dismiss();


							}
						});
		if (configDialog != null && configDialog.isShowing()) {
			configDialog.dismiss();
		}
		configDialog = customBuilder.createDialog();
		// 点击周围是否可以取消
		configDialog.setCanceledOnTouchOutside(false);
		configDialog.show();
	}


	public void changeEnvionment() {
		boolean flag = EnvConfigLoader.writeEnvConfigXml(environment);
		if (flag) {
			ToastUtil.show(this, "请求地址切换到" + environment.getCurrEnvironment() + "环境");
			handler.sendEmptyMessage(0);

		} else {
			ToastUtil.show(this, "请求地址切换失败！");
			dismissProgress();
		}
	}
}
